//uniform vec2 screen_size;
uniform float wave_amplitude;
uniform float wave_lenght;
uniform float wave_shift;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec4 pixel = Texel(texture, vec2(texture_coords.x + (cos((screen_coords.y/20)*wave_lenght + wave_shift)/50)*wave_amplitude, texture_coords.y));
    return vec4(pixel.r, pixel.g, pixel.b, pixel.a);
}
