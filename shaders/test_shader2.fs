//uniform vec2 screen_size;
uniform vec2 start_point;
uniform float wave_lenght;
uniform float wave_shift;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec2 resolution = vec2(1280, 720);
    //vec2 center = vec2(1280/2, 720/2);
    vec2 center = start_point;
    float distance_to_center = length(screen_coords-center);
    float wave_lenght = wave_lenght;
    float wave_shift = wave_shift;

    float cos_value = -distance_to_center/wave_lenght+wave_shift;
    float value = 0.0;

    //vec4 pixel = vec4(vec3(value),1.0);

    if ((cos_value > 3.14) || (cos_value < -3.14))
    {
      value = 0.0;
      //pixel = vec4(vec3(0.0),1.0);
    }
    else
    {
      value = (cos(cos_value)+1)/2;
    }

    vec4 pixel = Texel(texture, vec2(texture_coords.x, texture_coords.y));
    if (value != 0.0)
    {
      vec2 relative_center = normalize(screen_coords - center);

      //pixel = Texel(texture, vec2(cos(acos(texture_coords.x))*value + texture_coords.x, sin(asin(texture_coords.y))*value +  texture_coords.y));
      //pixel = Texel(texture, vec2( (cos(acos(texture_coords.x))/20*value) + texture_coords.x, sin( (asin(texture_coords.y))/20*value) +  texture_coords.y));
      float add_x = length(relative_center)/(resolution.x/24)*relative_center.x;
      float add_y = length(relative_center)/(resolution.y/24)*relative_center.y;
      pixel = Texel(texture, vec2( texture_coords.x + add_x*value, texture_coords.y + add_y*value));
    }


    //vec4 pixel = Texel(texture, vec2(texture_coords.x + cos(screen_coords.y/20)/100, texture_coords.y)); // This reads a color from our texture at the coordinates LOVE gave us (0-1, 0-1)
    //vec4 pixel = Texel(texture, vec2(texture_coords.x, texture_coords.y));
    //vec4 pixel_leftL = Texel(texture, vec2(texture_coords.x-0.002, texture_coords.y));
    //vec4 pixel_left = Texel(texture, vec2(texture_coords.x-0.001, texture_coords.y));
    //vec4 pixel_right = Texel(texture, vec2(texture_coords.x+0.001, texture_coords.y));
    //vec4 pixel_rightR = Texel(texture, vec2(texture_coords.x+0.002, texture_coords.y));
    //vec4 weight = vec4(0.5, 0.5, 0.5, 0.5);
    //vec4 pixel2 = mix(pixel_leftL, pixel_rightR, 0.5);
    //vec4 pixel3 = mix(pixel_left, pixel_right, 0.5);
    //vec4 pixel = mix(pixel2, pixel3, 0.75);
    return vec4(pixel.r, pixel.g, pixel.b, pixel.a); // This just returns a white color that's modulated by the brightest color channel at the given pixel in the texture. Nothing too complex, and not exactly the prettiest way to do B&W :P
}
