//uniform vec2 screen_size;
uniform vec2 start_point;
uniform float wave_lenght;
uniform float wave_shift;
uniform float intencity;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec2 resolution = vec2(1280, 720);
    //vec2 center = vec2(1280/2, 720/2);
    vec2 center = start_point;
    float distance_to_center = length(screen_coords-center);
    float wave_lenght = wave_lenght;
    float wave_shift = wave_shift;

    float cos_value = -distance_to_center/wave_lenght+wave_shift;
    float value = 0.0;

    //vec4 pixel = vec4(vec3(value),1.0);

    if ((cos_value > 3.14) || (cos_value < -3.14))
    {
      value = 0.0;
      //pixel = vec4(vec3(0.0),1.0);
    }
    else
    {
      value = (cos(cos_value)+1)/2;
    }

    vec4 pixel = Texel(texture, vec2(texture_coords.x, texture_coords.y));
    if (value != 0.0)
    {
      //vec3 (0.0,1.0,0.0,1.0)
      //pixel = vec4(pixel.r, pixel.g + value, pixel.b, pixel.a);
      vec2 relative_center = normalize(screen_coords - center);

      float add_x = (length(relative_center)/(resolution.x/24)*relative_center.x)*intencity;
      float add_y = (length(relative_center)/(resolution.y/24)*relative_center.y)*intencity;
      pixel = Texel(texture, vec2( texture_coords.x + add_x*value, texture_coords.y + add_y*value));

      //pixel = vec4(pixel.r  + (value/20)*intencity, pixel.g + (value/20)*intencity, pixel.b  + (value/20)*intencity, pixel.a);
    }

    return vec4(pixel.r, pixel.g, pixel.b, pixel.a); // This just returns a white color that's modulated by the brightest color channel at the given pixel in the texture. Nothing too complex, and not exactly the prettiest way to do B&W :P
}
