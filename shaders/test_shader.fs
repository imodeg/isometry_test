uniform vec2 screen_size;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec4 pixel = Texel(texture, vec2(texture_coords.x + cos(screen_coords.y/20)/50, texture_coords.y)); // This reads a color from our texture at the coordinates LOVE gave us (0-1, 0-1)
    //vec4 pixel = Texel(texture, vec2(texture_coords.x, texture_coords.y));
    //vec4 pixel_leftL = Texel(texture, vec2(texture_coords.x-0.002, texture_coords.y));
    //vec4 pixel_left = Texel(texture, vec2(texture_coords.x-0.001, texture_coords.y));
    //vec4 pixel_right = Texel(texture, vec2(texture_coords.x+0.001, texture_coords.y));
    //vec4 pixel_rightR = Texel(texture, vec2(texture_coords.x+0.002, texture_coords.y));
    //vec4 weight = vec4(0.5, 0.5, 0.5, 0.5);
    //vec4 pixel2 = mix(pixel_leftL, pixel_rightR, 0.5);
    //vec4 pixel3 = mix(pixel_left, pixel_right, 0.5);
    //vec4 pixel = mix(pixel2, pixel3, 0.75);
    return vec4(pixel.r, pixel.g, pixel.b, pixel.a); // This just returns a white color that's modulated by the brightest color channel at the given pixel in the texture. Nothing too complex, and not exactly the prettiest way to do B&W :P
}
