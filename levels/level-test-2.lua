return {
  version = "1.4",
  luaversion = "5.1",
  tiledversion = "1.4.2",
  orientation = "isometric",
  renderorder = "right-down",
  width = 16,
  height = 16,
  tilewidth = 100,
  tileheight = 50,
  nextlayerid = 7,
  nextobjectid = 5,
  properties = {},
  tilesets = {
    {
      name = "Blocks",
      firstgid = 1,
      filename = "Blocks.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 150,
        height = 200
      },
      properties = {},
      terrains = {},
      tilecount = 10,
      tiles = {
        {
          id = 0,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_01.png",
          width = 150,
          height = 125
        },
        {
          id = 1,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_02.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGress_01.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGressRock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Block",
          image = "../images/blocks/Block_h25_Rock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Block",
          image = "../images/Blocks/Block_h25_RockPic_01.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorPurple.png",
          width = 150,
          height = 125
        },
        {
          id = 7,
          type = "Block",
          properties = {
            ["ghost"] = true
          },
          image = "../images/blocks/Block_h25_ColorWhite.png",
          width = 150,
          height = 125
        },
        {
          id = 8,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorGray.png",
          width = 150,
          height = 125
        },
        {
          id = 9,
          image = "../images/blocks/Block_h25_ColorBlue.png",
          width = 150,
          height = 125
        }
      }
    },
    {
      name = "Decor",
      firstgid = 11,
      filename = "Decor.tsx",
      tilewidth = 150,
      tileheight = 250,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 10,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 36,
            ["hitbox_y"] = 36,
            ["hitbox_z"] = 25
          },
          image = "../images/decor/Decor_1x1_Rock01.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 42,
            ["hitbox_y"] = 42,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Rock02.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Stump01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress02.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_Tree01.png",
          width = 150,
          height = 200
        },
        {
          id = 7,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree02.png",
          width = 150,
          height = 225
        },
        {
          id = 8,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 9,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_blcok_gray.png",
          width = 150,
          height = 250
        }
      }
    },
    {
      name = "Decor_2x2",
      firstgid = 21,
      filename = "Decor_2x2.tsx",
      tilewidth = 250,
      tileheight = 200,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -75,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_Rock01.png",
          width = 250,
          height = 200
        }
      }
    },
    {
      name = "characters",
      firstgid = 22,
      filename = "characters.tsx",
      tilewidth = 150,
      tileheight = 238,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 14,
      tiles = {
        {
          id = 0,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "archibald",
            ["tween_scale_x_from"] = 0.97,
            ["tween_scale_x_time"] = 0.8,
            ["tween_scale_x_to"] = 1.03,
            ["tween_scale_y_from"] = 1.03,
            ["tween_scale_y_time"] = 0.8,
            ["tween_scale_y_to"] = 0.97
          },
          image = "../images/characters/character_archibald.png",
          width = 150,
          height = 238
        },
        {
          id = 1,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "barny"
          },
          image = "../images/characters/character_barny.png",
          width = 150,
          height = 238
        },
        {
          id = 2,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "boris"
          },
          image = "../images/characters/character_boris.png",
          width = 150,
          height = 238
        },
        {
          id = 3,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "doris"
          },
          image = "../images/characters/character_doris.png",
          width = 150,
          height = 238
        },
        {
          id = 4,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "begimot"
          },
          image = "../images/characters/character_father_begimot.png",
          width = 150,
          height = 238
        },
        {
          id = 5,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "isabella"
          },
          image = "../images/characters/character_isabella.png",
          width = 150,
          height = 238
        },
        {
          id = 6,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "jack"
          },
          image = "../images/characters/character_jack.png",
          width = 150,
          height = 238
        },
        {
          id = 7,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "molly"
          },
          image = "../images/characters/character_molly.png",
          width = 150,
          height = 238
        },
        {
          id = 8,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "patrick"
          },
          image = "../images/characters/character_patrick.png",
          width = 150,
          height = 238
        },
        {
          id = 9,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "sammy"
          },
          image = "../images/characters/character_sammy.png",
          width = 150,
          height = 238
        },
        {
          id = 10,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "shara"
          },
          image = "../images/characters/character_shara.png",
          width = 150,
          height = 238
        },
        {
          id = 11,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "stella"
          },
          image = "../images/characters/character_stella.png",
          width = 150,
          height = 238
        },
        {
          id = 12,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "stinky"
          },
          image = "../images/characters/character_stinky.png",
          width = 150,
          height = 238
        },
        {
          id = 13,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "thomas"
          },
          image = "../images/characters/character_thomas.png",
          width = 150,
          height = 238
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 1,
      name = "Tile Layer 1",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 2,
      name = "Tile Layer 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 28, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 27, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 16, 0, 0, 0, 0, 14, 0, 0, 0, 26, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 7, 8, 8, 7, 0, 0, 0, 0, 24, 0, 0, 0, 0,
        0, 0, 0, 7, 8, 8, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 7, 7, 7, 7, 0, 0, 0, 0, 23, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 30, 0, 31, 0, 32, 0, 33, 0, 34, 0, 22, 0, 0, 35, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      draworder = "topdown",
      id = 3,
      name = "Object Layer 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      objects = {
        {
          id = 3,
          name = "boris",
          type = "trigger",
          shape = "ellipse",
          x = 300,
          y = 300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 4,
          name = "archibald",
          type = "trigger",
          shape = "ellipse",
          x = 150,
          y = 300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 4,
      name = "Tile Layer 3",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -50,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 5,
      name = "Tile Layer 4",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -75,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 6,
      name = "Tile Layer 5",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -100,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    }
  }
}
