<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Blocks" tilewidth="150" tileheight="125" tilecount="38" columns="0">
 <tileoffset x="-25" y="25"/>
 <grid orientation="orthogonal" width="150" height="200"/>
 <tile id="0" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_Dirt_01.png"/>
 </tile>
 <tile id="1" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_Dirt_02.png"/>
 </tile>
 <tile id="2" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_DirtGress_01.png"/>
 </tile>
 <tile id="3" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_DirtGressRock_01.png"/>
 </tile>
 <tile id="4" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_Rock_01.png"/>
 </tile>
 <tile id="5" type="Block">
  <image width="150" height="125" source="../images/Blocks/Block_h25_RockPic_01.png"/>
 </tile>
 <tile id="6" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_ColorPurple.png"/>
 </tile>
 <tile id="7" type="Block">
  <properties>
   <property name="ghost" type="bool" value="true"/>
  </properties>
  <image width="150" height="125" source="../images/blocks/Block_h25_ColorWhite.png"/>
 </tile>
 <tile id="8" type="Block">
  <image width="150" height="125" source="../images/blocks/Block_h25_ColorGray.png"/>
 </tile>
 <tile id="9">
  <image width="150" height="125" source="../images/blocks/Block_h25_ColorBlue.png"/>
 </tile>
 <tile id="10">
  <image width="150" height="125" source="../images/blocks/Block_h25_water 1.png"/>
 </tile>
 <tile id="11">
  <image width="150" height="125" source="../images/blocks/Block_h25_water 2.png"/>
 </tile>
 <tile id="12">
  <image width="150" height="125" source="../images/blocks/Block_h25_water 3.png"/>
 </tile>
 <tile id="13">
  <image width="150" height="125" source="../images/blocks/Block_h25_water 4.png"/>
 </tile>
 <tile id="14">
  <image width="150" height="125" source="../images/blocks/Block_h25_water 5.png"/>
 </tile>
 <tile id="15">
  <image width="150" height="125" source="../images/blocks/Block_h25_water 6.png"/>
 </tile>
 <tile id="16">
  <image width="150" height="125" source="../images/blocks/grass_0.png"/>
 </tile>
 <tile id="17">
  <image width="150" height="125" source="../images/blocks/grass_1.png"/>
 </tile>
 <tile id="18">
  <image width="150" height="125" source="../images/blocks/playground_a.png"/>
 </tile>
 <tile id="19">
  <image width="150" height="125" source="../images/blocks/playground_a2.png"/>
 </tile>
 <tile id="20">
  <image width="150" height="125" source="../images/blocks/playground_a3.png"/>
 </tile>
 <tile id="21">
  <image width="150" height="125" source="../images/blocks/playground_b.png"/>
 </tile>
 <tile id="22">
  <image width="150" height="125" source="../images/blocks/playground_b2.png"/>
 </tile>
 <tile id="23">
  <image width="150" height="125" source="../images/blocks/playground_c.png"/>
 </tile>
 <tile id="24">
  <image width="150" height="125" source="../images/blocks/grass_2.png"/>
 </tile>
 <tile id="25">
  <image width="150" height="125" source="../images/blocks/grass_3.png"/>
 </tile>
 <tile id="26">
  <image width="150" height="125" source="../images/blocks/grass_4.png"/>
 </tile>
 <tile id="27">
  <image width="150" height="125" source="../images/blocks/light_sand.png"/>
 </tile>
 <tile id="28">
  <image width="150" height="125" source="../images/blocks/light_sand_grassy_b.png"/>
 </tile>
 <tile id="29">
  <image width="150" height="125" source="../images/blocks/light_sand_grassy_a.png"/>
 </tile>
 <tile id="30">
  <image width="150" height="125" source="../images/blocks/grass_dirt.png"/>
 </tile>
 <tile id="31">
  <image width="150" height="125" source="../images/blocks/grass_dirt_edge.png"/>
 </tile>
 <tile id="32">
  <image width="150" height="125" source="../images/blocks/grass_dirt_edge_flip.png"/>
 </tile>
 <tile id="33">
  <image width="150" height="125" source="../images/blocks/grass_dirt_01.png"/>
 </tile>
 <tile id="34">
  <image width="150" height="125" source="../images/blocks/grass_sand_edge.png"/>
 </tile>
 <tile id="35">
  <image width="150" height="125" source="../images/blocks/grass_dirt_edge_flip_03.png"/>
 </tile>
 <tile id="36">
  <image width="150" height="125" source="../images/blocks/grass_dirt_edge_flip_02.png"/>
 </tile>
 <tile id="37">
  <image width="150" height="125" source="../images/blocks/sand_dirt_edge.png"/>
 </tile>
</tileset>
