<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Decor_2x2" tilewidth="250" tileheight="469" tilecount="6" columns="0">
 <tileoffset x="-75" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="60"/>
   <property name="hitbox_y" type="int" value="60"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="250" height="200" source="../images/decor/Decor_2x2_Rock01.png"/>
 </tile>
 <tile id="1" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="60"/>
   <property name="hitbox_y" type="int" value="60"/>
   <property name="hitbox_z" type="int" value="200"/>
  </properties>
  <image width="250" height="469" source="../images/decor/Decor_2x2_skeleton_03.png"/>
 </tile>
 <tile id="2" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="100"/>
   <property name="hitbox_y" type="int" value="100"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="250" height="200" source="../images/decor/Decor_2x2_sandbox.png"/>
 </tile>
 <tile id="3" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="60"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="250" height="225" source="../images/decor/Decor_2x2_swing.png"/>
 </tile>
 <tile id="4" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="120"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="250" height="330" source="../images/decor/Decor_2x2_kids_ladder.png"/>
 </tile>
 <tile id="5" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="250" height="330" source="../images/decor/Decor_2x2_busket_ball.png"/>
 </tile>
</tileset>
