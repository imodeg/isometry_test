<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="decor_extra" tilewidth="350" tileheight="200" tilecount="3" columns="0">
 <tileoffset x="-125" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="60"/>
   <property name="hitbox_y" type="int" value="60"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="350" height="200" source="../images/decor/Decor_2x2_pumpkin_01.png"/>
 </tile>
 <tile id="1" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="60"/>
   <property name="hitbox_y" type="int" value="60"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="350" height="200" source="../images/decor/Decor_2x2_pumpkin_02.png"/>
 </tile>
 <tile id="2" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="60"/>
   <property name="hitbox_y" type="int" value="60"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="350" height="200" source="../images/decor/Decor_2x2_pumpkin_03.png"/>
 </tile>
</tileset>
