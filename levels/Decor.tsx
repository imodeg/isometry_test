<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Decor" tilewidth="150" tileheight="350" tilecount="33" columns="0">
 <tileoffset x="-25" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="38"/>
   <property name="hitbox_y" type="int" value="38"/>
   <property name="hitbox_z" type="int" value="100"/>
  </properties>
  <image width="150" height="175" source="../images/decor/Decor_1x1_Altar01.png"/>
 </tile>
 <tile id="1" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="36"/>
   <property name="hitbox_y" type="int" value="36"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="150" height="125" source="../images/decor/Decor_1x1_Rock01.png"/>
 </tile>
 <tile id="2" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="42"/>
   <property name="hitbox_y" type="int" value="42"/>
   <property name="hitbox_z" type="int" value="40"/>
  </properties>
  <image width="150" height="125" source="../images/decor/Decor_1x1_Rock02.png"/>
 </tile>
 <tile id="3" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="38"/>
   <property name="hitbox_y" type="int" value="38"/>
   <property name="hitbox_z" type="int" value="40"/>
  </properties>
  <image width="150" height="125" source="../images/decor/Decor_1x1_Stump01.png"/>
 </tile>
 <tile id="4" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="38"/>
   <property name="hitbox_y" type="int" value="38"/>
   <property name="hitbox_z" type="int" value="40"/>
  </properties>
  <image width="150" height="125" source="../images/decor/Decor_1x1_StumpGress01.png"/>
 </tile>
 <tile id="5" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="38"/>
   <property name="hitbox_y" type="int" value="38"/>
   <property name="hitbox_z" type="int" value="40"/>
  </properties>
  <image width="150" height="125" source="../images/decor/Decor_1x1_StumpGress02.png"/>
 </tile>
 <tile id="6" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="30"/>
   <property name="hitbox_y" type="int" value="30"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="200" source="../images/decor/Decor_1x1_Tree01.png"/>
 </tile>
 <tile id="7" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="30"/>
   <property name="hitbox_y" type="int" value="30"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_Tree02.png"/>
 </tile>
 <tile id="8" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="175" source="../images/decor/Decor_1x1_Altar01.png"/>
 </tile>
 <tile id="9" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="250" source="../images/decor/Decor_1x1_blcok_gray.png"/>
 </tile>
 <tile id="10" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="30"/>
   <property name="hitbox_y" type="int" value="30"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_Tree03.png"/>
 </tile>
 <tile id="11" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="30"/>
   <property name="hitbox_y" type="int" value="30"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="350" source="../images/decor/Decor_1x1_lamp_post.png"/>
 </tile>
 <tile id="12" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="40"/>
  </properties>
  <image width="150" height="125" source="../images/decor/Decor_1x1_bush.png"/>
 </tile>
 <tile id="13" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_0.png"/>
 </tile>
 <tile id="14" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="50"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_1.png"/>
 </tile>
 <tile id="15" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="70"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_2.png"/>
 </tile>
 <tile id="16" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_3.png"/>
 </tile>
 <tile id="17" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="70"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_4.png"/>
 </tile>
 <tile id="18" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_fance_02.png"/>
 </tile>
 <tile id="19" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_fance_01.png"/>
 </tile>
 <tile id="20" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_fun_02.png"/>
 </tile>
 <tile id="21" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_fun_01.png"/>
 </tile>
 <tile id="22" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_z1_05.png"/>
 </tile>
 <tile id="23" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_z1_04.png"/>
 </tile>
 <tile id="24" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_z1_03.png"/>
 </tile>
 <tile id="25" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_z1_02.png"/>
 </tile>
 <tile id="26" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_gravestone_z1_01.png"/>
 </tile>
 <tile id="27" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_skeleton_04.png"/>
 </tile>
 <tile id="28" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_skeleton_02.png"/>
 </tile>
 <tile id="29" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="125"/>
  </properties>
  <image width="150" height="225" source="../images/decor/Decor_1x1_skeleton_01.png"/>
 </tile>
 <tile id="30" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="30"/>
   <property name="hitbox_y" type="int" value="30"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="300" source="../images/decor/Decor_1x1_Tree06.png"/>
 </tile>
 <tile id="31" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="35"/>
   <property name="hitbox_y" type="int" value="35"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="250" source="../images/decor/Decor_1x1_Tree05.png"/>
 </tile>
 <tile id="32" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="35"/>
   <property name="hitbox_y" type="int" value="35"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="250" source="../images/decor/Decor_1x1_Tree04.png"/>
 </tile>
</tileset>
