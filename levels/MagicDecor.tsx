<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="MagicDecor" tilewidth="125" tileheight="125" tilecount="5" columns="0">
 <tileoffset x="-25" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="MagicDecor">
  <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="125" height="125" source="../images/items/item_candies.png"/>
 </tile>
 <tile id="1" type="MagicDecor">
   <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="125" height="125" source="../images/items/item_clock.png"/>
 </tile>
 <tile id="2" type="MagicDecor">
   <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="125" height="125" source="../images/items/item_cup.png"/>
 </tile>
 <tile id="3" type="MagicDecor">
   <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="125" height="125" source="../images/items/item_pumpkin_lighter.png"/>
 </tile>
 <tile id="4" type="MagicDecor">
   <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="125" height="125" source="../images/items/item_suit.png"/>
 </tile>
</tileset>
