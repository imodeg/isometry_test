return {
  version = "1.4",
  luaversion = "5.1",
  tiledversion = "1.4.2",
  orientation = "isometric",
  renderorder = "right-down",
  width = 16,
  height = 16,
  tilewidth = 100,
  tileheight = 50,
  nextlayerid = 10,
  nextobjectid = 9,
  properties = {
    ["background_image"] = "images/level_floor/room_back.png",
    ["background_offset_x"] = -408,
    ["background_offset_y"] = -290
  },
  tilesets = {
    {
      name = "Blocks",
      firstgid = 1,
      filename = "Blocks.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 150,
        height = 200
      },
      properties = {},
      terrains = {},
      tilecount = 38,
      tiles = {
        {
          id = 0,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_01.png",
          width = 150,
          height = 125
        },
        {
          id = 1,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_02.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGress_01.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGressRock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Block",
          image = "../images/blocks/Block_h25_Rock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Block",
          image = "../images/Blocks/Block_h25_RockPic_01.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorPurple.png",
          width = 150,
          height = 125
        },
        {
          id = 7,
          type = "Block",
          properties = {
            ["ghost"] = true
          },
          image = "../images/blocks/Block_h25_ColorWhite.png",
          width = 150,
          height = 125
        },
        {
          id = 8,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorGray.png",
          width = 150,
          height = 125
        },
        {
          id = 9,
          image = "../images/blocks/Block_h25_ColorBlue.png",
          width = 150,
          height = 125
        },
        {
          id = 10,
          image = "../images/blocks/Block_h25_water 1.png",
          width = 150,
          height = 125
        },
        {
          id = 11,
          image = "../images/blocks/Block_h25_water 2.png",
          width = 150,
          height = 125
        },
        {
          id = 12,
          image = "../images/blocks/Block_h25_water 3.png",
          width = 150,
          height = 125
        },
        {
          id = 13,
          image = "../images/blocks/Block_h25_water 4.png",
          width = 150,
          height = 125
        },
        {
          id = 14,
          image = "../images/blocks/Block_h25_water 5.png",
          width = 150,
          height = 125
        },
        {
          id = 15,
          image = "../images/blocks/Block_h25_water 6.png",
          width = 150,
          height = 125
        },
        {
          id = 16,
          image = "../images/blocks/grass_0.png",
          width = 150,
          height = 125
        },
        {
          id = 17,
          image = "../images/blocks/grass_1.png",
          width = 150,
          height = 125
        },
        {
          id = 18,
          image = "../images/blocks/playground_a.png",
          width = 150,
          height = 125
        },
        {
          id = 19,
          image = "../images/blocks/playground_a2.png",
          width = 150,
          height = 125
        },
        {
          id = 20,
          image = "../images/blocks/playground_a3.png",
          width = 150,
          height = 125
        },
        {
          id = 21,
          image = "../images/blocks/playground_b.png",
          width = 150,
          height = 125
        },
        {
          id = 22,
          image = "../images/blocks/playground_b2.png",
          width = 150,
          height = 125
        },
        {
          id = 23,
          image = "../images/blocks/playground_c.png",
          width = 150,
          height = 125
        },
        {
          id = 24,
          image = "../images/blocks/grass_2.png",
          width = 150,
          height = 125
        },
        {
          id = 25,
          image = "../images/blocks/grass_3.png",
          width = 150,
          height = 125
        },
        {
          id = 26,
          image = "../images/blocks/grass_4.png",
          width = 150,
          height = 125
        },
        {
          id = 27,
          image = "../images/blocks/light_sand.png",
          width = 150,
          height = 125
        },
        {
          id = 28,
          image = "../images/blocks/light_sand_grassy_b.png",
          width = 150,
          height = 125
        },
        {
          id = 29,
          image = "../images/blocks/light_sand_grassy_a.png",
          width = 150,
          height = 125
        },
        {
          id = 30,
          image = "../images/blocks/grass_dirt.png",
          width = 150,
          height = 125
        },
        {
          id = 31,
          image = "../images/blocks/grass_dirt_edge.png",
          width = 150,
          height = 125
        },
        {
          id = 32,
          image = "../images/blocks/grass_dirt_edge_flip.png",
          width = 150,
          height = 125
        },
        {
          id = 33,
          image = "../images/blocks/grass_dirt_01.png",
          width = 150,
          height = 125
        },
        {
          id = 34,
          image = "../images/blocks/grass_sand_edge.png",
          width = 150,
          height = 125
        },
        {
          id = 35,
          image = "../images/blocks/grass_dirt_edge_flip_03.png",
          width = 150,
          height = 125
        },
        {
          id = 36,
          image = "../images/blocks/grass_dirt_edge_flip_02.png",
          width = 150,
          height = 125
        },
        {
          id = 37,
          image = "../images/blocks/sand_dirt_edge.png",
          width = 150,
          height = 125
        }
      }
    },
    {
      name = "Decor",
      firstgid = 39,
      filename = "Decor.tsx",
      tilewidth = 150,
      tileheight = 350,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 33,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 36,
            ["hitbox_y"] = 36,
            ["hitbox_z"] = 25
          },
          image = "../images/decor/Decor_1x1_Rock01.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 42,
            ["hitbox_y"] = 42,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Rock02.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Stump01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress02.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_Tree01.png",
          width = 150,
          height = 200
        },
        {
          id = 7,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree02.png",
          width = 150,
          height = 225
        },
        {
          id = 8,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 9,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_blcok_gray.png",
          width = 150,
          height = 250
        },
        {
          id = 10,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree03.png",
          width = 150,
          height = 225
        },
        {
          id = 11,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_lamp_post.png",
          width = 150,
          height = 350
        },
        {
          id = 12,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_bush.png",
          width = 150,
          height = 125
        },
        {
          id = 13,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_0.png",
          width = 150,
          height = 225
        },
        {
          id = 14,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 50
          },
          image = "../images/decor/Decor_1x1_gravestone_1.png",
          width = 150,
          height = 225
        },
        {
          id = 15,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 70
          },
          image = "../images/decor/Decor_1x1_gravestone_2.png",
          width = 150,
          height = 225
        },
        {
          id = 16,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_3.png",
          width = 150,
          height = 225
        },
        {
          id = 17,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 70
          },
          image = "../images/decor/Decor_1x1_gravestone_4.png",
          width = 150,
          height = 225
        },
        {
          id = 18,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fance_02.png",
          width = 150,
          height = 225
        },
        {
          id = 19,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fance_01.png",
          width = 150,
          height = 225
        },
        {
          id = 20,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fun_02.png",
          width = 150,
          height = 225
        },
        {
          id = 21,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fun_01.png",
          width = 150,
          height = 225
        },
        {
          id = 22,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_05.png",
          width = 150,
          height = 225
        },
        {
          id = 23,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_04.png",
          width = 150,
          height = 225
        },
        {
          id = 24,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_03.png",
          width = 150,
          height = 225
        },
        {
          id = 25,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_02.png",
          width = 150,
          height = 225
        },
        {
          id = 26,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_01.png",
          width = 150,
          height = 225
        },
        {
          id = 27,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_skeleton_04.png",
          width = 150,
          height = 225
        },
        {
          id = 28,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_skeleton_02.png",
          width = 150,
          height = 225
        },
        {
          id = 29,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_skeleton_01.png",
          width = 150,
          height = 225
        },
        {
          id = 30,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree06.png",
          width = 150,
          height = 300
        },
        {
          id = 31,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 35,
            ["hitbox_y"] = 35,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree05.png",
          width = 150,
          height = 250
        },
        {
          id = 32,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 35,
            ["hitbox_y"] = 35,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree04.png",
          width = 150,
          height = 250
        }
      }
    },
    {
      name = "Decor_2x2",
      firstgid = 72,
      filename = "Decor_2x2.tsx",
      tilewidth = 250,
      tileheight = 469,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -75,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 6,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_Rock01.png",
          width = 250,
          height = 200
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 200
          },
          image = "../images/decor/Decor_2x2_skeleton_03.png",
          width = 250,
          height = 469
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 100,
            ["hitbox_y"] = 100,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_sandbox.png",
          width = 250,
          height = 200
        },
        {
          id = 3,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_swing.png",
          width = 250,
          height = 225
        },
        {
          id = 4,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 120,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_kids_ladder.png",
          width = 250,
          height = 330
        },
        {
          id = 5,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 25,
            ["hitbox_y"] = 25,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_busket_ball.png",
          width = 250,
          height = 330
        }
      }
    },
    {
      name = "Buttons",
      firstgid = 78,
      filename = "Buttons.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Button",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 0,
            ["theme"] = "Base"
          },
          image = "../images/objects/Button_Base_Off.png",
          width = 150,
          height = 125
        }
      }
    },
    {
      name = "Items_objects",
      firstgid = 79,
      filename = "Items_objects.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Item",
          properties = {
            ["hitbox_x"] = 25,
            ["hitbox_y"] = 25,
            ["hitbox_z"] = 25,
            ["name"] = "clock"
          },
          image = "../images/items/item_clock_object.png",
          width = 150,
          height = 125
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 1,
      name = "Tile Layer 1",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 2,
      name = "Tile Layer 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 8, 8, 8, 8, 8, 8, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 8, 79, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      draworder = "topdown",
      id = 7,
      name = "trigger",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      objects = {
        {
          id = 5,
          name = "homeOut",
          type = "",
          shape = "ellipse",
          x = 500,
          y = 450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "homeOut",
          type = "",
          shape = "ellipse",
          x = 500,
          y = 400,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "homeOut",
          type = "",
          shape = "ellipse",
          x = 500,
          y = 350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 8,
          name = "getClock",
          type = "",
          shape = "ellipse",
          x = 250,
          y = 550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
