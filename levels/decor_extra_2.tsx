<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="decor_extra_2" tilewidth="250" tileheight="200" tilecount="1" columns="0">
 <tileoffset x="-75" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Decor">
  <properties>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="150"/>
   <property name="hitbox_z" type="int" value="25"/>
  </properties>
  <image width="250" height="200" source="../images/decor/decor_table_extra.png"/>
 </tile>
</tileset>
