<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Items_objects" tilewidth="150" tileheight="125" tilecount="1" columns="0">
 <tileoffset x="-25" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Item">
  <properties>
   <property name="hitbox_x" type="int" value="25"/>
   <property name="hitbox_y" type="int" value="25"/>
   <property name="hitbox_z" type="int" value="25"/>
   <property name="name" value="clock"/>
  </properties>
  <image width="150" height="125" source="../images/items/item_clock_object.png"/>
 </tile>
</tileset>
