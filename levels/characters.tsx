<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="characters" tilewidth="200" tileheight="250" tilecount="23" columns="0">
 <tileoffset x="-25" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-5"/>
   <property name="cloudShiftY" type="int" value="-20"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="archibald"/>
   <property name="tween_scale_x_from" type="float" value="0.97"/>
   <property name="tween_scale_x_time" type="float" value="0.8"/>
   <property name="tween_scale_x_to" type="float" value="1.03"/>
   <property name="tween_scale_y_from" type="float" value="1.03"/>
   <property name="tween_scale_y_time" type="float" value="0.8"/>
   <property name="tween_scale_y_to" type="float" value="0.97"/>
  </properties>
  <image width="200" height="238" source="../images/characters/character_archibald.png"/>
 </tile>
 <tile id="1" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="14"/>
   <property name="cloudShiftY" type="int" value="30"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="barny"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.2"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.2"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="0.8"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_barny.png"/>
 </tile>
 <tile id="2" type="Character">
  <properties>
   <property name="cloudShiftY" type="int" value="-40"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="boris"/>
   <property name="tween_scale_x_from" type="float" value="0.97"/>
   <property name="tween_scale_x_time" type="float" value="1"/>
   <property name="tween_scale_x_to" type="float" value="1.03"/>
   <property name="tween_scale_y_from" type="float" value="1.03"/>
   <property name="tween_scale_y_time" type="float" value="1"/>
   <property name="tween_scale_y_to" type="float" value="0.97"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="3"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="200" height="250" source="../images/characters/character_boris.png"/>
 </tile>
 <tile id="3" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-20"/>
   <property name="cloudShiftY" type="int" value="60"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="doris"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_doris.png"/>
 </tile>
 <tile id="4" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-15"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="begimot"/>
   <property name="tween_shear_x_ease" value="easeInOutBack"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="0.8"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_father_begimot.png"/>
 </tile>
 <tile id="5" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="5"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="isabella"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_isabella.png"/>
 </tile>
 <tile id="6" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="5"/>
   <property name="cloudShiftY" type="int" value="10"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="jack"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.4"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.4"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="0.4"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_jack.png"/>
 </tile>
 <tile id="7" type="Character">
  <properties>
   <property name="cloudShiftY" type="int" value="45"/>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="molly"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.2"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.2"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="0.8"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_molly.png"/>
 </tile>
 <tile id="8" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-15"/>
   <property name="cloudShiftY" type="int" value="-25"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="patrick"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_patrick.png"/>
 </tile>
 <tile id="9" type="Character">
  <properties>
   <property name="cloudShiftY" type="int" value="50"/>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="sammy"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="1.2"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="2"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_sammy.png"/>
 </tile>
 <tile id="10" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-10"/>
   <property name="cloudShiftY" type="int" value="30"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="sara"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_shara.png"/>
 </tile>
 <tile id="11" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="15"/>
   <property name="cloudShiftY" type="int" value="15"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="stella"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="3"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="200" height="238" source="../images/characters/character_stella.png"/>
 </tile>
 <tile id="12" type="Character">
  <properties>
   <property name="cloudShiftY" type="int" value="45"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="stinky"/>
   <property name="tween_shear_x_ease" value="easeInOutBack"/>
   <property name="tween_shear_x_from" type="float" value="-0.04"/>
   <property name="tween_shear_x_time" type="float" value="0.8"/>
   <property name="tween_shear_x_to" type="float" value="0.04"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_stinky.png"/>
 </tile>
 <tile id="13" type="Character">
  <properties>
   <property name="cloudShiftY" type="int" value="-20"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="thomas"/>
   <property name="tween_scale_x_from" type="float" value="0.97"/>
   <property name="tween_scale_x_time" type="float" value="0.6"/>
   <property name="tween_scale_x_to" type="float" value="1.03"/>
   <property name="tween_scale_y_from" type="float" value="1.03"/>
   <property name="tween_scale_y_time" type="float" value="0.6"/>
   <property name="tween_scale_y_to" type="float" value="0.97"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="1.2"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_thomas.png"/>
 </tile>
 <tile id="14" type="Character">
  <properties>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="bushman"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_bushman.png"/>
 </tile>
 <tile id="15" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="4"/>
   <property name="cloudShiftY" type="int" value="30"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="arice"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_arice.png"/>
 </tile>
 <tile id="16" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-10"/>
   <property name="cloudShiftY" type="int" value="10"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="meredit"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="1"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="1"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="2"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_meredit.png"/>
 </tile>
 <tile id="17" type="Character">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="molly_sad"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_molly_sad.png"/>
 </tile>
 <tile id="18" type="Character">
  <properties>
   <property name="cloudShiftY" type="int" value="50"/>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="ostin"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_ostin.png"/>
 </tile>
 <tile id="19" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-5"/>
   <property name="cloudShiftY" type="int" value="60"/>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="pam"/>
   <property name="tween_scale_x_from" type="float" value="0.98"/>
   <property name="tween_scale_x_time" type="float" value="0.7"/>
   <property name="tween_scale_x_to" type="float" value="1.02"/>
   <property name="tween_scale_y_from" type="float" value="1.02"/>
   <property name="tween_scale_y_time" type="float" value="0.7"/>
   <property name="tween_scale_y_to" type="float" value="0.98"/>
   <property name="tween_shear_x_from" type="float" value="-0.025"/>
   <property name="tween_shear_x_time" type="float" value="1.4"/>
   <property name="tween_shear_x_to" type="float" value="0.025"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_pam.png"/>
 </tile>
 <tile id="20" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="0"/>
   <property name="cloudShiftY" type="int" value="45"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="robby"/>
   <property name="tween_shear_x_from" type="float" value="-0.05"/>
   <property name="tween_shear_x_time" type="float" value="0.8"/>
   <property name="tween_shear_x_to" type="float" value="0.05"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_robby.png"/>
 </tile>
 <tile id="21" type="Character">
  <properties>
   <property name="cloudShiftX" type="int" value="-25"/>
   <property name="cloudShiftY" type="int" value="60"/>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="150"/>
   <property name="name" value="strangekid"/>
   <property name="tween_shear_x_ease" value="inOutCirc"/>
   <property name="tween_shear_x_from" type="float" value="0"/>
   <property name="tween_shear_x_time" type="float" value="1.2"/>
   <property name="tween_shear_x_to" type="float" value="0.12"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_strangekid.png"/>
 </tile>
 <tile id="22" type="FinalSceneTrigger">
  <properties>
   <property name="hitbox_x" type="int" value="40"/>
   <property name="hitbox_y" type="int" value="40"/>
   <property name="hitbox_z" type="int" value="150"/>
  </properties>
  <image width="150" height="238" source="../images/characters/character_molly_sad.png"/>
 </tile>
</tileset>
