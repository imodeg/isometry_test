return {
  version = "1.4",
  luaversion = "5.1",
  tiledversion = "1.4.2",
  orientation = "isometric",
  renderorder = "right-down",
  width = 16,
  height = 16,
  tilewidth = 100,
  tileheight = 50,
  nextlayerid = 6,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "Blocks",
      firstgid = 1,
      filename = "Blocks.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 150,
        height = 200
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {
        {
          id = 0,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_01.png",
          width = 150,
          height = 125
        },
        {
          id = 1,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_02.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGress_01.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGressRock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Block",
          image = "../images/blocks/Block_h25_Rock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Block",
          image = "../images/Blocks/Block_h25_RockPic_01.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorPurple.png",
          width = 150,
          height = 125
        },
        {
          id = 7,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorWhite.png",
          width = 150,
          height = 125
        },
        {
          id = 8,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorGray.png",
          width = 150,
          height = 125
        }
      }
    },
    {
      name = "Decor",
      firstgid = 10,
      filename = "Decor.tsx",
      tilewidth = 150,
      tileheight = 225,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 36,
            ["hitbox_y"] = 36,
            ["hitbox_z"] = 25
          },
          image = "../images/decor/Decor_1x1_Rock01.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 42,
            ["hitbox_y"] = 42,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Rock02.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Stump01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress02.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_Tree01.png",
          width = 150,
          height = 200
        },
        {
          id = 7,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree02.png",
          width = 150,
          height = 225
        },
        {
          id = 8,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        }
      }
    },
    {
      name = "Decor_2x2",
      firstgid = 19,
      filename = "Decor_2x2.tsx",
      tilewidth = 250,
      tileheight = 200,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -75,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_Rock01.png",
          width = 250,
          height = 200
        }
      }
    },
    {
      name = "Buttons",
      firstgid = 20,
      filename = "Buttons.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Button",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 0,
            ["theme"] = "Base"
          },
          image = "images/objects/Button_Base_Off.png",
          width = 150,
          height = 125
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 1,
      name = "Tile Layer 1",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 7, 7, 7, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 7, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 7, 7, 7, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 7, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 7, 7, 7, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 2,
      name = "Tile Layer 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 19, 11, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 16, 0, 15, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 13, 16, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 20, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 20, 20, 20, 20, 20, 20, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 3,
      name = "Tile Layer 3",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -50,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 4,
      name = "Tile Layer 4",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -75,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 16,
      height = 16,
      id = 5,
      name = "Tile Layer 5",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -100,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    }
  }
}
