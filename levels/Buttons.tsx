<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Buttons" tilewidth="150" tileheight="125" tilecount="1" columns="0">
 <tileoffset x="-25" y="25"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Button">
  <properties>
   <property name="hitbox_x" type="int" value="50"/>
   <property name="hitbox_y" type="int" value="50"/>
   <property name="hitbox_z" type="int" value="0"/>
   <property name="theme" value="Base"/>
  </properties>
  <image width="150" height="125" source="../images/objects/Button_Base_Off.png"/>
 </tile>
</tileset>
