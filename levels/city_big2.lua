return {
  version = "1.4",
  luaversion = "5.1",
  tiledversion = "1.4.2",
  orientation = "isometric",
  renderorder = "right-down",
  width = 64,
  height = 128,
  tilewidth = 100,
  tileheight = 50,
  nextlayerid = 34,
  nextobjectid = 167,
  properties = {
    ["background_image"] = "images/level_floor/city_big_test.png",
    ["background_offset_x"] = -6400,
    ["background_offset_y"] = -125,
    ["big_img"] = true
  },
  tilesets = {
    {
      name = "Blocks",
      firstgid = 1,
      filename = "Blocks.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 150,
        height = 200
      },
      properties = {},
      terrains = {},
      tilecount = 38,
      tiles = {
        {
          id = 0,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_01.png",
          width = 150,
          height = 125
        },
        {
          id = 1,
          type = "Block",
          image = "../images/blocks/Block_h25_Dirt_02.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGress_01.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Block",
          image = "../images/blocks/Block_h25_DirtGressRock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Block",
          image = "../images/blocks/Block_h25_Rock_01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Block",
          image = "../images/Blocks/Block_h25_RockPic_01.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorPurple.png",
          width = 150,
          height = 125
        },
        {
          id = 7,
          type = "Block",
          properties = {
            ["ghost"] = true
          },
          image = "../images/blocks/Block_h25_ColorWhite.png",
          width = 150,
          height = 125
        },
        {
          id = 8,
          type = "Block",
          image = "../images/blocks/Block_h25_ColorGray.png",
          width = 150,
          height = 125
        },
        {
          id = 9,
          image = "../images/blocks/Block_h25_ColorBlue.png",
          width = 150,
          height = 125
        },
        {
          id = 10,
          image = "../images/blocks/Block_h25_water 1.png",
          width = 150,
          height = 125
        },
        {
          id = 11,
          image = "../images/blocks/Block_h25_water 2.png",
          width = 150,
          height = 125
        },
        {
          id = 12,
          image = "../images/blocks/Block_h25_water 3.png",
          width = 150,
          height = 125
        },
        {
          id = 13,
          image = "../images/blocks/Block_h25_water 4.png",
          width = 150,
          height = 125
        },
        {
          id = 14,
          image = "../images/blocks/Block_h25_water 5.png",
          width = 150,
          height = 125
        },
        {
          id = 15,
          image = "../images/blocks/Block_h25_water 6.png",
          width = 150,
          height = 125
        },
        {
          id = 16,
          image = "../images/blocks/grass_0.png",
          width = 150,
          height = 125
        },
        {
          id = 17,
          image = "../images/blocks/grass_1.png",
          width = 150,
          height = 125
        },
        {
          id = 18,
          image = "../images/blocks/playground_a.png",
          width = 150,
          height = 125
        },
        {
          id = 19,
          image = "../images/blocks/playground_a2.png",
          width = 150,
          height = 125
        },
        {
          id = 20,
          image = "../images/blocks/playground_a3.png",
          width = 150,
          height = 125
        },
        {
          id = 21,
          image = "../images/blocks/playground_b.png",
          width = 150,
          height = 125
        },
        {
          id = 22,
          image = "../images/blocks/playground_b2.png",
          width = 150,
          height = 125
        },
        {
          id = 23,
          image = "../images/blocks/playground_c.png",
          width = 150,
          height = 125
        },
        {
          id = 24,
          image = "../images/blocks/grass_2.png",
          width = 150,
          height = 125
        },
        {
          id = 25,
          image = "../images/blocks/grass_3.png",
          width = 150,
          height = 125
        },
        {
          id = 26,
          image = "../images/blocks/grass_4.png",
          width = 150,
          height = 125
        },
        {
          id = 27,
          image = "../images/blocks/light_sand.png",
          width = 150,
          height = 125
        },
        {
          id = 28,
          image = "../images/blocks/light_sand_grassy_b.png",
          width = 150,
          height = 125
        },
        {
          id = 29,
          image = "../images/blocks/light_sand_grassy_a.png",
          width = 150,
          height = 125
        },
        {
          id = 30,
          image = "../images/blocks/grass_dirt.png",
          width = 150,
          height = 125
        },
        {
          id = 31,
          image = "../images/blocks/grass_dirt_edge.png",
          width = 150,
          height = 125
        },
        {
          id = 32,
          image = "../images/blocks/grass_dirt_edge_flip.png",
          width = 150,
          height = 125
        },
        {
          id = 33,
          image = "../images/blocks/grass_dirt_01.png",
          width = 150,
          height = 125
        },
        {
          id = 34,
          image = "../images/blocks/grass_sand_edge.png",
          width = 150,
          height = 125
        },
        {
          id = 35,
          image = "../images/blocks/grass_dirt_edge_flip_03.png",
          width = 150,
          height = 125
        },
        {
          id = 36,
          image = "../images/blocks/grass_dirt_edge_flip_02.png",
          width = 150,
          height = 125
        },
        {
          id = 37,
          image = "../images/blocks/sand_dirt_edge.png",
          width = 150,
          height = 125
        }
      }
    },
    {
      name = "Decor",
      firstgid = 39,
      filename = "Decor.tsx",
      tilewidth = 150,
      tileheight = 350,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 33,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 36,
            ["hitbox_y"] = 36,
            ["hitbox_z"] = 25
          },
          image = "../images/decor/Decor_1x1_Rock01.png",
          width = 150,
          height = 125
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 42,
            ["hitbox_y"] = 42,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Rock02.png",
          width = 150,
          height = 125
        },
        {
          id = 3,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_Stump01.png",
          width = 150,
          height = 125
        },
        {
          id = 4,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress01.png",
          width = 150,
          height = 125
        },
        {
          id = 5,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 38,
            ["hitbox_y"] = 38,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_StumpGress02.png",
          width = 150,
          height = 125
        },
        {
          id = 6,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_Tree01.png",
          width = 150,
          height = 200
        },
        {
          id = 7,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree02.png",
          width = 150,
          height = 225
        },
        {
          id = 8,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Altar01.png",
          width = 150,
          height = 175
        },
        {
          id = 9,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_blcok_gray.png",
          width = 150,
          height = 250
        },
        {
          id = 10,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree03.png",
          width = 150,
          height = 225
        },
        {
          id = 11,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_lamp_post.png",
          width = 150,
          height = 350
        },
        {
          id = 12,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 40
          },
          image = "../images/decor/Decor_1x1_bush.png",
          width = 150,
          height = 125
        },
        {
          id = 13,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_0.png",
          width = 150,
          height = 225
        },
        {
          id = 14,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 50
          },
          image = "../images/decor/Decor_1x1_gravestone_1.png",
          width = 150,
          height = 225
        },
        {
          id = 15,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 70
          },
          image = "../images/decor/Decor_1x1_gravestone_2.png",
          width = 150,
          height = 225
        },
        {
          id = 16,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_3.png",
          width = 150,
          height = 225
        },
        {
          id = 17,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 70
          },
          image = "../images/decor/Decor_1x1_gravestone_4.png",
          width = 150,
          height = 225
        },
        {
          id = 18,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fance_02.png",
          width = 150,
          height = 225
        },
        {
          id = 19,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fance_01.png",
          width = 150,
          height = 225
        },
        {
          id = 20,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fun_02.png",
          width = 150,
          height = 225
        },
        {
          id = 21,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_fun_01.png",
          width = 150,
          height = 225
        },
        {
          id = 22,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_05.png",
          width = 150,
          height = 225
        },
        {
          id = 23,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_04.png",
          width = 150,
          height = 225
        },
        {
          id = 24,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_03.png",
          width = 150,
          height = 225
        },
        {
          id = 25,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_02.png",
          width = 150,
          height = 225
        },
        {
          id = 26,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_gravestone_z1_01.png",
          width = 150,
          height = 225
        },
        {
          id = 27,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_skeleton_04.png",
          width = 150,
          height = 225
        },
        {
          id = 28,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_skeleton_02.png",
          width = 150,
          height = 225
        },
        {
          id = 29,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 125
          },
          image = "../images/decor/Decor_1x1_skeleton_01.png",
          width = 150,
          height = 225
        },
        {
          id = 30,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 30,
            ["hitbox_y"] = 30,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree06.png",
          width = 150,
          height = 300
        },
        {
          id = 31,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 35,
            ["hitbox_y"] = 35,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree05.png",
          width = 150,
          height = 250
        },
        {
          id = 32,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 35,
            ["hitbox_y"] = 35,
            ["hitbox_z"] = 150
          },
          image = "../images/decor/Decor_1x1_Tree04.png",
          width = 150,
          height = 250
        }
      }
    },
    {
      name = "characters",
      firstgid = 72,
      filename = "characters.tsx",
      tilewidth = 200,
      tileheight = 250,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 23,
      tiles = {
        {
          id = 0,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -5,
            ["cloudShiftY"] = -20,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "archibald",
            ["tween_scale_x_from"] = 0.97,
            ["tween_scale_x_time"] = 0.8,
            ["tween_scale_x_to"] = 1.03,
            ["tween_scale_y_from"] = 1.03,
            ["tween_scale_y_time"] = 0.8,
            ["tween_scale_y_to"] = 0.97
          },
          image = "../images/characters/character_archibald.png",
          width = 200,
          height = 238
        },
        {
          id = 1,
          type = "Character",
          properties = {
            ["cloudShiftX"] = 14,
            ["cloudShiftY"] = 30,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "barny",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.2,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.2,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 0.8,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_barny.png",
          width = 150,
          height = 238
        },
        {
          id = 2,
          type = "Character",
          properties = {
            ["cloudShiftY"] = -40,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "boris",
            ["tween_scale_x_from"] = 0.97,
            ["tween_scale_x_time"] = 1,
            ["tween_scale_x_to"] = 1.03,
            ["tween_scale_y_from"] = 1.03,
            ["tween_scale_y_time"] = 1,
            ["tween_scale_y_to"] = 0.97,
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 3,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_boris.png",
          width = 200,
          height = 250
        },
        {
          id = 3,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -20,
            ["cloudShiftY"] = 60,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "doris",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_doris.png",
          width = 150,
          height = 238
        },
        {
          id = 4,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -15,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "begimot",
            ["tween_shear_x_ease"] = "easeInOutBack",
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 0.8,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_father_begimot.png",
          width = 150,
          height = 238
        },
        {
          id = 5,
          type = "Character",
          properties = {
            ["cloudShiftX"] = 5,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "isabella",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_isabella.png",
          width = 150,
          height = 238
        },
        {
          id = 6,
          type = "Character",
          properties = {
            ["cloudShiftX"] = 5,
            ["cloudShiftY"] = 10,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "jack",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.4,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.4,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 0.4,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_jack.png",
          width = 150,
          height = 238
        },
        {
          id = 7,
          type = "Character",
          properties = {
            ["cloudShiftY"] = 45,
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 150,
            ["name"] = "molly",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.2,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.2,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 0.8,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_molly.png",
          width = 150,
          height = 238
        },
        {
          id = 8,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -15,
            ["cloudShiftY"] = -25,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "patrick",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_patrick.png",
          width = 150,
          height = 238
        },
        {
          id = 9,
          type = "Character",
          properties = {
            ["cloudShiftY"] = 50,
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 150,
            ["name"] = "sammy",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 1.2,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 2,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_sammy.png",
          width = 150,
          height = 238
        },
        {
          id = 10,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -10,
            ["cloudShiftY"] = 30,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "sara",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_shara.png",
          width = 150,
          height = 238
        },
        {
          id = 11,
          type = "Character",
          properties = {
            ["cloudShiftX"] = 15,
            ["cloudShiftY"] = 15,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "stella",
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 3,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_stella.png",
          width = 200,
          height = 238
        },
        {
          id = 12,
          type = "Character",
          properties = {
            ["cloudShiftY"] = 45,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "stinky",
            ["tween_shear_x_ease"] = "easeInOutBack",
            ["tween_shear_x_from"] = -0.04,
            ["tween_shear_x_time"] = 0.8,
            ["tween_shear_x_to"] = 0.04
          },
          image = "../images/characters/character_stinky.png",
          width = 150,
          height = 238
        },
        {
          id = 13,
          type = "Character",
          properties = {
            ["cloudShiftY"] = -20,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "thomas",
            ["tween_scale_x_from"] = 0.97,
            ["tween_scale_x_time"] = 0.6,
            ["tween_scale_x_to"] = 1.03,
            ["tween_scale_y_from"] = 1.03,
            ["tween_scale_y_time"] = 0.6,
            ["tween_scale_y_to"] = 0.97,
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 1.2,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_thomas.png",
          width = 150,
          height = 238
        },
        {
          id = 14,
          type = "Character",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "bushman"
          },
          image = "../images/characters/character_bushman.png",
          width = 150,
          height = 238
        },
        {
          id = 15,
          type = "Character",
          properties = {
            ["cloudShiftX"] = 4,
            ["cloudShiftY"] = 30,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "arice",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_arice.png",
          width = 150,
          height = 238
        },
        {
          id = 16,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -10,
            ["cloudShiftY"] = 10,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "meredit",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 1,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 1,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 2,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_meredit.png",
          width = 150,
          height = 238
        },
        {
          id = 17,
          type = "Character",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 150,
            ["name"] = "molly_sad"
          },
          image = "../images/characters/character_molly_sad.png",
          width = 150,
          height = 238
        },
        {
          id = 18,
          type = "Character",
          properties = {
            ["cloudShiftY"] = 50,
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 150,
            ["name"] = "ostin",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_ostin.png",
          width = 150,
          height = 238
        },
        {
          id = 19,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -5,
            ["cloudShiftY"] = 60,
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 150,
            ["name"] = "pam",
            ["tween_scale_x_from"] = 0.98,
            ["tween_scale_x_time"] = 0.7,
            ["tween_scale_x_to"] = 1.02,
            ["tween_scale_y_from"] = 1.02,
            ["tween_scale_y_time"] = 0.7,
            ["tween_scale_y_to"] = 0.98,
            ["tween_shear_x_from"] = -0.025,
            ["tween_shear_x_time"] = 1.4,
            ["tween_shear_x_to"] = 0.025
          },
          image = "../images/characters/character_pam.png",
          width = 150,
          height = 238
        },
        {
          id = 20,
          type = "Character",
          properties = {
            ["cloudShiftX"] = 0,
            ["cloudShiftY"] = 45,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "robby",
            ["tween_shear_x_from"] = -0.05,
            ["tween_shear_x_time"] = 0.8,
            ["tween_shear_x_to"] = 0.05
          },
          image = "../images/characters/character_robby.png",
          width = 150,
          height = 238
        },
        {
          id = 21,
          type = "Character",
          properties = {
            ["cloudShiftX"] = -25,
            ["cloudShiftY"] = 60,
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 150,
            ["name"] = "strangekid",
            ["tween_shear_x_ease"] = "inOutCirc",
            ["tween_shear_x_from"] = 0,
            ["tween_shear_x_time"] = 1.2,
            ["tween_shear_x_to"] = 0.12
          },
          image = "../images/characters/character_strangekid.png",
          width = 150,
          height = 238
        },
        {
          id = 22,
          type = "FinalSceneTrigger",
          properties = {
            ["hitbox_x"] = 40,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 150
          },
          image = "../images/characters/character_molly_sad.png",
          width = 150,
          height = 238
        }
      }
    },
    {
      name = "Buttons",
      firstgid = 95,
      filename = "Buttons.tsx",
      tilewidth = 150,
      tileheight = 125,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -25,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Button",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 50,
            ["hitbox_z"] = 0,
            ["theme"] = "Base"
          },
          image = "../images/objects/Button_Base_Off.png",
          width = 150,
          height = 125
        }
      }
    },
    {
      name = "decor_extra",
      firstgid = 96,
      filename = "decor_extra.tsx",
      tilewidth = 350,
      tileheight = 200,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -125,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 3,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_pumpkin_01.png",
          width = 350,
          height = 200
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_pumpkin_02.png",
          width = 350,
          height = 200
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_pumpkin_03.png",
          width = 350,
          height = 200
        }
      }
    },
    {
      name = "Decor_2x2",
      firstgid = 99,
      filename = "Decor_2x2.tsx",
      tilewidth = 250,
      tileheight = 469,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -75,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 6,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_Rock01.png",
          width = 250,
          height = 200
        },
        {
          id = 1,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 60,
            ["hitbox_z"] = 200
          },
          image = "../images/decor/Decor_2x2_skeleton_03.png",
          width = 250,
          height = 469
        },
        {
          id = 2,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 100,
            ["hitbox_y"] = 100,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_sandbox.png",
          width = 250,
          height = 200
        },
        {
          id = 3,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 60,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_swing.png",
          width = 250,
          height = 225
        },
        {
          id = 4,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 120,
            ["hitbox_y"] = 40,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_kids_ladder.png",
          width = 250,
          height = 330
        },
        {
          id = 5,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 25,
            ["hitbox_y"] = 25,
            ["hitbox_z"] = 100
          },
          image = "../images/decor/Decor_2x2_busket_ball.png",
          width = 250,
          height = 330
        }
      }
    },
    {
      name = "decor_extra_2",
      firstgid = 105,
      filename = "decor_extra_2.tsx",
      tilewidth = 250,
      tileheight = 200,
      spacing = 0,
      margin = 0,
      columns = 0,
      objectalignment = "unspecified",
      tileoffset = {
        x = -75,
        y = 25
      },
      grid = {
        orientation = "orthogonal",
        width = 1,
        height = 1
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {
        {
          id = 0,
          type = "Decor",
          properties = {
            ["hitbox_x"] = 50,
            ["hitbox_y"] = 150,
            ["hitbox_z"] = 25
          },
          image = "../images/decor/decor_table_extra.png",
          width = 250,
          height = 200
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 64,
      height = 128,
      id = 15,
      name = "floor",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 34, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 26, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 27, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 27, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 32, 34, 34, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 36, 34, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 36, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 17, 17, 17, 31, 17, 17, 17, 17, 17, 17, 17, 17, 27, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 26, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 26, 17, 17, 25, 25, 25, 25, 25,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 33, 33, 33, 33, 33, 33, 33, 33, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 17, 17, 25, 25, 25, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 36, 36, 36, 36, 36, 36, 36, 36, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 25, 25, 25, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 31, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 18, 25, 17, 25, 25, 25, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 23, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 18, 17, 25, 25, 25, 25, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 22, 23, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 18, 25, 25, 25, 25, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 18, 25, 25, 25, 25, 26, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 22, 22, 22, 22, 22, 22, 22, 22, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 18, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 19, 19, 19, 19, 19, 19, 19, 19, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 19, 19, 19, 19, 19, 19, 19, 19, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 19, 19, 19, 19, 19, 19, 19, 19, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25,
        25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 19, 19, 19, 19, 19, 19, 19, 19, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25,
        25, 25, 17, 17, 17, 32, 34, 37, 17, 27, 25, 17, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 21, 19, 19, 19, 19, 19, 19, 21, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25,
        25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 25, 25, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25,
        25, 25, 17, 17, 17, 32, 34, 37, 17, 25, 25, 25, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 25, 25, 35, 28,
        25, 25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 25, 25, 17, 25, 25, 26, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 35, 16,
        25, 25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 35, 16,
        25, 25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 25, 25, 35, 28, 16,
        25, 25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 31, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 25, 25, 25, 17, 25, 35, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 25, 25, 25, 25, 25, 35, 16, 16,
        25, 25, 25, 25, 17, 32, 34, 37, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 35, 16, 16,
        25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 25, 25, 35, 28, 16, 16,
        25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 17, 35, 16, 16, 16,
        25, 25, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 25, 17, 35, 16, 16, 16,
        25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 35, 28, 16, 16, 16,
        25, 25, 17, 17, 32, 34, 37, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 35, 28, 16, 16, 16, 16,
        25, 25, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 35, 16, 16, 16, 12, 16,
        25, 25, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 35, 16, 16, 16, 16, 16,
        25, 25, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 33, 33, 33, 33, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 27, 17, 35, 28, 16, 16, 16, 16, 16,
        25, 25, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 25, 25, 25, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 25, 26, 17, 17, 17, 17, 35, 16, 16, 16, 14, 16, 16,
        25, 25, 17, 32, 34, 34, 37, 17, 17, 17, 25, 25, 17, 25, 25, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 36, 36, 36, 34, 34, 33, 33, 33, 33, 17, 17, 17, 17, 17, 17, 35, 28, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 32, 34, 34, 37, 17, 17, 17, 25, 17, 25, 25, 25, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 17, 25, 25, 17, 17, 25, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 36, 36, 36, 36, 36, 36, 36, 17, 17, 17, 17, 35, 16, 13, 16, 16, 16, 16, 16,
        25, 25, 31, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 25, 17, 17, 32, 34, 34, 33, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 16, 16, 16, 16, 12, 16, 16,
        25, 25, 17, 25, 17, 27, 17, 36, 36, 34, 37, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 25, 25, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 25, 25, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 16, 12, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 25, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 25, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 17, 17, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 25, 25, 17, 17, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 25, 25, 25, 17, 17, 17, 25, 25, 17, 17, 17, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 25, 25, 25, 17, 25, 25, 25, 25, 25, 25, 17, 17, 17, 17, 35, 16, 14, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 17, 17, 17, 17, 26, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 26, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 25, 17, 17, 25, 25, 25, 25, 25, 17, 25, 25, 25, 25, 25, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 27, 17, 17, 26, 17, 17, 17, 17, 25, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 25, 25, 25, 25, 17, 17, 17, 17, 25, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 25, 17, 35, 16, 16, 16, 16, 12, 16, 16,
        25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 26, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 17, 35, 16, 12, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 17, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 26, 25, 17, 35, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 25, 17, 17, 17, 25, 25, 33, 33, 33, 34, 34, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 35, 29, 28, 16, 16, 16, 16, 14, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 25, 17, 17, 17, 25, 17, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 35, 30, 28, 16, 16, 13, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 25, 17, 17, 25, 25, 25, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 26, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 25, 17, 35, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 17, 17, 17, 25, 25, 25, 36, 36, 36, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 35, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 26, 17, 17, 17, 25, 17, 17, 25, 25, 25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 25, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 33, 33, 35, 28, 16, 16, 16, 16, 12, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 38, 28, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 27, 17, 25, 17, 17, 25, 17, 17, 25, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 38, 28, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 38, 28, 28, 28, 16, 13, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 27, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 33, 33, 33, 33, 33, 33, 33, 17, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 15, 16, 13, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 12, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 32, 34, 37, 17, 17, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 32, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 36, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 26, 17, 17, 36, 36, 36, 36, 36, 36, 36, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 34, 34, 38, 28, 28, 16, 16, 11, 16, 16, 14, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 17, 17, 9, 9, 9, 9, 9, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 33, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 18, 25, 18, 18, 17, 17, 17, 9, 9, 9, 9, 9, 36, 36, 36, 36, 36, 36, 36, 34, 34, 34, 36, 34, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 33, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 33, 33, 33, 33, 33, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 32, 34, 38, 28, 28, 16, 13, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 33, 33, 33, 33, 17, 17, 17, 17, 17, 17, 17, 18, 17, 17, 17, 18, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 36, 36, 36, 36, 34, 34, 34, 34, 34, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 36, 36, 36, 36, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 9, 9, 9, 9, 9, 17, 17, 26, 17, 17, 17, 17, 32, 34, 37, 17, 32, 34, 38, 28, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 32, 34, 38, 28, 28, 16, 16, 11, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 31, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 32, 33, 34, 34, 37, 17, 17, 35, 29, 29, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 25, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 32, 34, 34, 34, 37, 17, 17, 35, 29, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 25, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 31, 32, 34, 34, 36, 17, 17, 26, 35, 29, 30, 28, 16, 16, 16, 16, 13, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 32, 34, 37, 17, 17, 17, 18, 35, 29, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 33, 33, 33, 33, 33, 34, 34, 37, 17, 17, 17, 18, 17, 35, 30, 28, 16, 12, 15, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 25, 17, 17, 17, 32, 34, 34, 37, 17, 17, 17, 17, 17, 25, 25, 25, 25, 18, 26, 18, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 33, 33, 33, 33, 33, 32, 34, 34, 34, 33, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 25, 17, 17, 17, 9, 9, 9, 9, 9, 36, 36, 36, 36, 36, 36, 36, 17, 17, 17, 17, 18, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 32, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 37, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 17, 17, 35, 29, 29, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 9, 9, 9, 9, 9, 17, 17, 27, 17, 18, 17, 17, 17, 18, 17, 17, 17, 35, 29, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 36, 36, 36, 36, 36, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 29, 30, 28, 16, 12, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 32, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 29, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 18, 17, 17, 17, 35, 29, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 29, 30, 28, 16, 16, 16, 14, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 31, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 27, 17, 9, 9, 9, 9, 9, 17, 31, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 27, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 18, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 26, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 26, 17, 17, 17, 35, 29, 28, 16, 13, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 27, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 12, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 25, 25, 17, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 30, 28, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 17, 17, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 35, 29, 30, 16, 16, 16, 16, 16, 16, 16,
        17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 25, 25, 25, 25, 25, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 9, 9, 9, 9, 9, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 35, 30, 30, 16, 16, 16, 11, 16, 16, 16,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 9, 9, 9, 9, 9, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 29, 16, 16, 12, 16, 16, 16, 16,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 9, 9, 9, 9, 9, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 29, 16, 16, 16, 16, 16, 16, 16,
        25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 9, 9, 9, 9, 9, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 29, 16, 16, 16, 16, 16, 16, 16
      }
    },
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 64,
      height = 128,
      id = 7,
      name = "Tile Layer 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 46, 46, 46, 0, 46, 0, 0, 71, 0, 0, 0, 0, 0, 46, 46, 49, 71, 46, 46, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 45, 46, 46, 46, 0, 0, 46, 46, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0,
        0, 46, 46, 0, 0, 0, 46, 46, 49, 46, 46, 46, 49, 46, 71, 46, 0, 0, 0, 0, 0, 46, 46, 49, 71, 46, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 46, 0, 0, 0, 46, 46, 46, 0, 46, 71, 46, 46, 46, 0, 46, 46, 0, 0, 0, 46, 46,
        0, 46, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 46, 46, 46, 0, 0, 70, 46, 0, 46, 0,
        46, 46, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 44, 0, 0, 0, 46, 46, 0, 0, 0, 46, 46, 46, 70, 45, 46, 46, 46, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 93, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 45, 0, 0, 46, 49, 0, 71, 45, 70, 46, 46, 46, 0, 46, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 46, 71, 46, 0, 46, 45, 46, 46, 70, 0, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 46, 46, 71, 46, 0, 46, 0, 0, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 44, 0, 0, 46, 46, 0, 46, 0, 0, 0, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 46, 71, 46, 46, 70, 46, 46,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 46, 0, 0, 46,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 45, 46, 0, 46,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 0, 45, 0, 0,
        46, 46, 46, 70, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 46, 0, 0,
        46, 46, 49, 0, 46, 0, 0, 0, 0, 46, 46, 0, 0, 46, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 0, 46, 0,
        46, 0, 46, 45, 46, 70, 46, 0, 49, 0, 46, 46, 0, 46, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 71, 0, 45, 0,
        46, 0, 46, 46, 0, 46, 46, 0, 0, 46, 46, 0, 49, 46, 0, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0,
        46, 0, 0, 46, 45, 0, 0, 0, 0, 46, 70, 46, 70, 46, 70, 46, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 46, 0,
        46, 49, 46, 46, 46, 0, 0, 0, 0, 0, 0, 46, 46, 49, 46, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 46, 0,
        70, 46, 46, 46, 49, 0, 0, 0, 0, 46, 46, 46, 46, 46, 46, 46, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 46, 45, 0,
        46, 49, 45, 70, 46, 46, 0, 0, 46, 46, 46, 46, 46, 70, 46, 46, 49, 46, 0, 46, 8, 0, 0, 8, 51, 51, 51, 45, 51, 51, 45, 51, 51, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 0,
        46, 46, 46, 46, 70, 46, 0, 70, 0, 46, 0, 46, 46, 0, 0, 46, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 51, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0,
        46, 46, 46, 46, 46, 70, 46, 45, 46, 46, 45, 46, 46, 46, 0, 46, 46, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 46, 0,
        46, 46, 46, 8, 0, 46, 46, 46, 46, 70, 0, 0, 46, 46, 46, 0, 46, 46, 49, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 46, 0,
        46, 46, 0, 8, 0, 0, 0, 0, 0, 46, 46, 46, 46, 46, 46, 0, 46, 0, 0, 45, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 104, 0, 0, 103, 0, 0, 50, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 71, 0,
        45, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 46, 0, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0,
        46, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 46, 46, 46, 0, 46, 46, 51, 0, 8, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 46, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 46, 46, 46, 46, 0, 0, 46, 0, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 71, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 46, 45, 46, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0,
        45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 46, 49, 70, 0, 0, 51, 0, 63, 0, 0, 0, 0, 0, 0, 0, 66, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 91, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 45, 0,
        0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 70, 46, 46, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 46, 0,
        46, 44, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 46, 46, 46, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 102, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 0,
        70, 46, 46, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 49, 46, 49, 0, 0, 51, 51, 45, 51, 51, 51, 51, 45, 51, 51, 45, 51, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0,
        46, 45, 40, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 46, 0, 0,
        46, 45, 45, 8, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 46, 46, 46, 0, 0, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0,
        46, 8, 45, 0, 8, 0, 0, 0, 8, 8, 0, 0, 0, 0, 46, 70, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 0, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0,
        46, 45, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 46, 46, 46, 0, 0, 0, 52, 0, 0, 54, 0, 0, 55, 0, 0, 56, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0,
        69, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0,
        46, 46, 0, 0, 82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 92, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 71, 0, 0, 0,
        46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 56, 0, 0, 55, 0, 0, 52, 0, 0, 54, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 8,
        70, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 45, 0, 0, 0, 8,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 8,
        46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 8, 0,
        49, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 54, 0, 0, 52, 0, 0, 53, 0, 0, 52, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 71, 0, 0, 0, 8, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 0, 0, 65, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 8, 0,
        46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 8, 0,
        46, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 71, 46, 0, 0, 0, 8, 0, 0,
        69, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 45, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 46, 0, 49, 0, 0, 8, 0, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 46, 0, 0, 0, 0, 8, 0, 0,
        0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 70, 70, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 49, 46, 0, 0, 8, 0, 0, 0,
        46, 45, 0, 0, 0, 0, 0, 0, 0, 0, 46, 69, 46, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 50, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 46, 49, 0, 8, 0, 0, 0, 0,
        46, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 46, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 46, 49, 0, 8, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
        70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 46, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0,
        45, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 0, 51, 0, 0, 0, 8, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 0, 46, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 78, 0, 0, 0, 0, 0, 51, 0, 68, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        70, 46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 51, 51, 45, 51, 51, 51, 51, 51, 45, 51, 51, 51, 0, 45, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 69, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 9, 8, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 45, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8, 0, 51, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 49, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        70, 0, 0, 0, 45, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 49, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 49, 0, 49, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 45, 46, 0, 0, 70, 46, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 49, 0, 49, 0, 0, 0, 45, 45, 0, 0, 0, 71, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 0, 46, 49, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 46, 0, 0, 46, 0, 0, 0, 0, 0, 0, 46, 0, 8, 0, 0, 0, 0, 0, 0,
        69, 46, 45, 46, 70, 46, 46, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 45, 0, 0, 70, 0, 0, 0, 0, 45, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 49, 46, 46, 46, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 46, 46, 45, 70, 46, 0, 46, 0, 46, 0, 46, 46, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 46, 46, 46, 0, 0, 8, 0, 0, 0, 0, 87, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 46, 0, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 71, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 70, 46, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 49, 46, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        70, 46, 46, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 51, 45, 45, 45, 51, 0, 0, 45, 45, 45, 45, 49, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 69, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 71, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 49, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 51, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 45, 0, 0, 0, 46, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 51, 51, 51, 0, 8, 8, 8, 8, 8, 0, 0, 51, 0, 0, 0, 0, 45, 0, 0, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 45, 0, 0, 51, 0, 0, 0, 0, 61, 0, 59, 0, 0, 0, 51, 0, 0, 0, 0, 86, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 8, 8, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 70, 46, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 0, 51, 51, 46, 51, 45, 51, 51, 51, 51, 51, 46, 51, 0, 0, 0, 0, 0, 0, 0, 0, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 0, 45, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 74, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 49, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        70, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 46, 45, 0, 0, 97, 0, 0, 96, 0, 0, 97, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 85, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 45, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 70, 46, 0, 0, 96, 0, 0, 98, 0, 0, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        70, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 98, 0, 0, 97, 0, 0, 98, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 45, 51, 45, 51, 51, 45, 51, 45, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 49, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 46, 49, 0, 69, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 0, 0, 0, 51, 45, 45, 45, 51, 0, 0, 51, 45, 45, 45, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 70, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 45, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 45, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 45, 69, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 70, 0, 69, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 69, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 69, 46, 49, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 8, 8, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 45, 70, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 45, 0, 44, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 69, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        0, 46, 0, 49, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 8, 8, 0, 51, 67, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 0, 46, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 77, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        0, 49, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 100, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 49, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 51, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 60, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 69, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 45, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        45, 46, 0, 0, 8, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
        69, 45, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 46, 46, 46, 0, 45, 49, 46, 0, 0, 46, 46, 46, 46, 0, 0, 0, 0, 0, 46, 46, 46, 46, 46, 46, 69, 46, 46, 46, 46, 49, 0, 0, 0, 0, 0, 46, 0, 46, 0, 0, 46, 46, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 71, 46, 0, 0, 45, 46, 49, 49, 46, 0, 0, 46, 71, 46, 46, 0, 49, 71, 46, 46, 46, 46, 45, 0, 0, 46, 46, 46, 46, 46, 46, 46, 46, 46, 71, 46, 0, 0, 0, 46, 49, 69, 46, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 8, 0, 0, 0, 0, 0, 0,
        46, 46, 46, 46, 71, 46, 46, 46, 0, 49, 46, 49, 49, 46, 46, 0, 0, 46, 46, 46, 46, 0, 45, 46, 71, 46, 46, 45, 46, 46, 46, 46, 45, 0, 0, 46, 46, 46, 46, 0, 0, 0, 0, 0, 0, 0, 46, 46, 45, 46, 0, 0, 46, 69, 0, 46, 0, 8, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      draworder = "topdown",
      id = 10,
      name = "triggers",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = -25,
      properties = {},
      objects = {
        {
          id = 1,
          name = "home",
          type = "",
          shape = "ellipse",
          x = 1750,
          y = 5300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 4,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 4200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 5,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 3450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 6,
          name = "church",
          type = "",
          shape = "ellipse",
          x = 1550,
          y = 2800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1650,
          y = 2600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 8,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 800,
          y = 4700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 9,
          name = "home",
          type = "",
          shape = "ellipse",
          x = 1750,
          y = 5250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "home",
          type = "",
          shape = "ellipse",
          x = 1750,
          y = 5200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 12,
          name = "home",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 5350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 13,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 850,
          y = 4700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 14,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 850,
          y = 4650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 15,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 750,
          y = 4650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 16,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 750,
          y = 4700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 17,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 4200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 18,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2100,
          y = 4150,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 19,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 4150,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 20,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2100,
          y = 4200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 21,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 600,
          y = 3350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 22,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 3400,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 23,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 3350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 24,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 600,
          y = 3450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 25,
          name = "church",
          type = "",
          shape = "ellipse",
          x = 1550,
          y = 2850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 26,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 2550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 27,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 2600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 28,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1600,
          y = 2600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 29,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1600,
          y = 2550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 30,
          name = "home",
          type = "",
          shape = "ellipse",
          x = 1750,
          y = 5350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 31,
          name = "thomas",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 4350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 32,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2400,
          y = 1350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 33,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 150,
          y = 1900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 34,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2050,
          y = 650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 35,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1500,
          y = 4000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 37,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 700,
          y = 5650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 39,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 40,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 41,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 4100,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 42,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2800,
          y = 4700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 43,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 44,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1900,
          y = 2850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 45,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1100,
          y = 3850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 46,
          name = "patrick",
          type = "",
          shape = "ellipse",
          x = 1650,
          y = 1050,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 47,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 600,
          y = 250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 35,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1350,
          y = 1850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 38,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 750,
          y = 5650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 39,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 750,
          y = 5600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 40,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 750,
          y = 5550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 41,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 700,
          y = 5550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 42,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 5550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 43,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 5600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 44,
          name = "isabella",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 5650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 55,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 850,
          y = 4600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 56,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 800,
          y = 4600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 57,
          name = "archibald",
          type = "",
          shape = "ellipse",
          x = 750,
          y = 4600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 58,
          name = "thomas",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 4350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 59,
          name = "thomas",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 4300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 60,
          name = "thomas",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 4250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 61,
          name = "thomas",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 4250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 62,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 4100,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 63,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1150,
          y = 4100,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 64,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1150,
          y = 4050,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 65,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1150,
          y = 4000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 66,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 4000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 67,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 4050,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 68,
          name = "ostin",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 4000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 70,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 3850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 71,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 3800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 72,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1150,
          y = 3750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 73,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1200,
          y = 3750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 74,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1150,
          y = 3850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 75,
          name = "doris",
          type = "",
          shape = "ellipse",
          x = 1100,
          y = 3800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 76,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1450,
          y = 4000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 77,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1450,
          y = 3950,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 78,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1550,
          y = 4000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 79,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1550,
          y = 3950,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 80,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1550,
          y = 3900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 81,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1500,
          y = 3900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 82,
          name = "barny",
          type = "",
          shape = "ellipse",
          x = 1450,
          y = 3900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 83,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2100,
          y = 4100,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 84,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 4100,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 85,
          name = "boris",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 4100,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 86,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 550,
          y = 3450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 87,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 550,
          y = 3400,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 88,
          name = "arice",
          type = "",
          shape = "ellipse",
          x = 550,
          y = 3350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 89,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 250,
          y = 1850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 90,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 200,
          y = 1800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 91,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 250,
          y = 1900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 92,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 250,
          y = 1800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 93,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 200,
          y = 1900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 94,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 150,
          y = 1850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 95,
          name = "sara",
          type = "",
          shape = "ellipse",
          x = 150,
          y = 1800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 96,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 2500,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 97,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1650,
          y = 2500,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 98,
          name = "begimot",
          type = "",
          shape = "ellipse",
          x = 1600,
          y = 2500,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 99,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1350,
          y = 1800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 100,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1300,
          y = 1800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 101,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 1850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 102,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 1800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 103,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1250,
          y = 1900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 104,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1300,
          y = 1900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 105,
          name = "robby",
          type = "",
          shape = "ellipse",
          x = 1350,
          y = 1900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 106,
          name = "patrick",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 1050,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 107,
          name = "patrick",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 1000,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 108,
          name = "patrick",
          type = "",
          shape = "ellipse",
          x = 1700,
          y = 950,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 109,
          name = "patrick",
          type = "",
          shape = "ellipse",
          x = 1650,
          y = 950,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 110,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 111,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 600,
          y = 150,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 112,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 113,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 650,
          y = 150,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 114,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 550,
          y = 250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 115,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 550,
          y = 200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 116,
          name = "strangekid",
          type = "",
          shape = "ellipse",
          x = 550,
          y = 150,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 117,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 118,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 119,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2300,
          y = 1300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 120,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2300,
          y = 1250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 121,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 122,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2300,
          y = 1200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 123,
          name = "stinky",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1200,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 124,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2450,
          y = 1350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 125,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2450,
          y = 1300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 126,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2400,
          y = 1250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 127,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2450,
          y = 1250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 128,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2350,
          y = 1250,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 129,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2350,
          y = 1300,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 130,
          name = "molly",
          type = "",
          shape = "ellipse",
          x = 2350,
          y = 1350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 131,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 132,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1400,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 133,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 134,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 135,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 1350,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 136,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 1400,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 137,
          name = "pam",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 1450,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 138,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 1750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 139,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 1700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 140,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2150,
          y = 1650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 141,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 142,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2200,
          y = 1650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 143,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 144,
          name = "sammy",
          type = "",
          shape = "ellipse",
          x = 2250,
          y = 1750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 145,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2100,
          y = 650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 146,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2100,
          y = 600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 147,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2100,
          y = 550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 148,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2050,
          y = 550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 149,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2000,
          y = 650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 150,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2000,
          y = 600,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 151,
          name = "stella",
          type = "",
          shape = "ellipse",
          x = 2000,
          y = 550,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 152,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1900,
          y = 2800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 153,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1850,
          y = 2800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 154,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1800,
          y = 2800,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 155,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1850,
          y = 2900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 156,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1800,
          y = 2900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 157,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1800,
          y = 2850,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 158,
          name = "jack",
          type = "",
          shape = "ellipse",
          x = 1900,
          y = 2900,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 159,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2800,
          y = 4750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 160,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2750,
          y = 4750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 161,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2700,
          y = 4750,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 162,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2700,
          y = 4700,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 163,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2700,
          y = 4650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 164,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2750,
          y = 4650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        },
        {
          id = 165,
          name = "meredit",
          type = "",
          shape = "ellipse",
          x = 2800,
          y = 4650,
          width = 50,
          height = 50,
          rotation = 0,
          visible = true,
          properties = {
            ["isCharacter"] = true
          }
        }
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_church_01.png",
      id = 11,
      name = "church",
      visible = true,
      opacity = 1,
      offsetx = 4100,
      offsety = 1150,
      properties = {
        ["hitbox_x"] = 500,
        ["hitbox_y"] = 450,
        ["hitbox_z"] = 500,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_village_01.png",
      id = 13,
      name = "village_01",
      visible = true,
      opacity = 1,
      offsetx = 4450,
      offsety = 0,
      properties = {
        ["hitbox_x"] = 350,
        ["hitbox_y"] = 400,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_vilage_03.png",
      id = 16,
      name = "village_03",
      visible = true,
      opacity = 0.59,
      offsetx = 4182.97,
      offsety = 2108.12,
      properties = {
        ["hitbox_x"] = 350,
        ["hitbox_y"] = 400,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_vilage_02.png",
      id = 20,
      name = "village_02",
      visible = true,
      opacity = 0.44,
      offsetx = 1073.55,
      offsety = 2825.88,
      properties = {
        ["hitbox_x"] = 400,
        ["hitbox_y"] = 400,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_red_02.png",
      id = 18,
      name = "house_red_2",
      visible = true,
      opacity = 0.59,
      offsetx = 1333.47,
      offsety = 2197.79,
      properties = {
        ["hitbox_x"] = 260,
        ["hitbox_y"] = 290,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_fantasy_02.png",
      id = 17,
      name = "fantasy_02",
      visible = true,
      opacity = 0.59,
      offsetx = 1884.47,
      offsety = 2502.12,
      properties = {
        ["hitbox_x"] = 260,
        ["hitbox_y"] = 325,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_fantasy_01.png",
      id = 21,
      name = "fantasy_01",
      visible = true,
      opacity = 0.59,
      offsetx = 5596.23,
      offsety = -586.697,
      properties = {
        ["hitbox_x"] = 300,
        ["hitbox_y"] = 325,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_red_01.png",
      id = 32,
      name = "fantasy_red",
      visible = true,
      opacity = 0.59,
      offsetx = 5665.73,
      offsety = 283.803,
      properties = {
        ["hitbox_x"] = 250,
        ["hitbox_y"] = 300,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_random_01.png",
      id = 19,
      name = "random_house_01",
      visible = true,
      opacity = 0.59,
      offsetx = 2457.47,
      offsety = 1225.29,
      properties = {
        ["hitbox_x"] = 240,
        ["hitbox_y"] = 260,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_black_01.png",
      id = 31,
      name = "random_house_02",
      visible = true,
      opacity = 0.59,
      offsetx = 5630.97,
      offsety = 1631.29,
      properties = {
        ["hitbox_x"] = 240,
        ["hitbox_y"] = 260,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_shop.png",
      id = 14,
      name = "shop_01",
      visible = true,
      opacity = 0.34,
      offsetx = 7110.33,
      offsety = 123,
      properties = {
        ["hitbox_x"] = 800,
        ["hitbox_y"] = 500,
        ["hitbox_z"] = 150,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_1.png",
      id = 22,
      name = "tree_01",
      visible = true,
      opacity = 0.34,
      offsetx = 6644.39,
      offsety = -279.538,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_1.png",
      id = 25,
      name = "tree03",
      visible = true,
      opacity = 0.34,
      offsetx = 5245.97,
      offsety = 215.545,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_1.png",
      id = 26,
      name = "tree04",
      visible = true,
      opacity = 0.34,
      offsetx = 5740.89,
      offsety = 12.2955,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_1.png",
      id = 27,
      name = "tree05",
      visible = true,
      opacity = 0.34,
      offsetx = 3693.31,
      offsety = 140.545,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_2.png",
      id = 23,
      name = "tree_02",
      visible = true,
      opacity = 0.34,
      offsetx = 6300.73,
      offsety = -446.205,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_2.png",
      id = 28,
      name = "tree_06",
      visible = true,
      opacity = 0.34,
      offsetx = 4003.06,
      offsety = -4.53788,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_2.png",
      id = 24,
      name = "tree07",
      visible = true,
      opacity = 0.34,
      offsetx = 7554.06,
      offsety = 419.462,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/tree_big_2.png",
      id = 33,
      name = "Copy of tree07",
      visible = true,
      opacity = 0.34,
      offsetx = 3549.31,
      offsety = 1273.21,
      properties = {
        ["hitbox_x"] = 40,
        ["hitbox_y"] = 40,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    },
    {
      type = "imagelayer",
      image = "../images/houses/house_decorated_01.png",
      id = 29,
      name = "house_corner",
      visible = true,
      opacity = 0.34,
      offsetx = 47.2088,
      offsety = 2174.37,
      properties = {
        ["hitbox_x"] = 400,
        ["hitbox_y"] = 400,
        ["hitbox_z"] = 300,
        ["type"] = "house"
      }
    }
  }
}
