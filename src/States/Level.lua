local Camera = require("Core.Camera")
local Map = require("Map.Map")
local Player = require("Entities.Player")
local Tiled = require("Utils.Tiled")
local World = require("Core.World")
local Interval = require("Core.Interval")
local Sprite = require("Core.Sprite")
local Cat = require("Entities.Cat")
local InventoryManager = require("Dialog.InventoryManager")
local TimeManager = require("Dialog.TimeManager")
local EffectManager = require("Dialog.EffectManager")
local DialogManager = require("Dialog.DialogManager_New")

local cityData = Tiled:new(require("city_big2"))
local churchData = Tiled:new(require("church"))
local churchEndData = Tiled:new(require("church_end"))
local churchEndSceneData = Tiled:new(require("church_end_scene"))
local homeData = Tiled:new(require("home"))
local testData = Tiled:new(require("level-test-2"))

local CICLE_TIME = 120 --in seconds 120
local CICLE_END_TIME = 6 --end game state (before new loop)
local SLOTS_DISTANSE = 90
local SLOTS_YPOS = 110
local SLOTS_XPOS = 880

local MAPS = {
  home = {
    data = homeData,
    position = vec3d(238, 295, 26)
  },
  city = {
    data = cityData,
    position = vec3d(1830, 5270, 26)
  },
  cityOutChurch = {
    data = cityData,
    position = vec3d(1576, 2830, 26)
  },
  church = {
    data = churchData,
    position = vec3d(295, 388, 26)
  },
  churchEnd = {
    data = churchEndData,
    position = vec3d(295, 388, 26)
  },
  churchEndScene = {
    data = churchEndSceneData,
    position = vec3d(295, 388, 26)
  },
  test = {
    data = testData,
    position = vec3d(200, 200, 26)
  }
}

local START_MAP = "home"
local FIRST_LOAD_MAP = "city"

return function (Game)
  local Level = Game:addState("Level")

  function Level:enteredState()
    self.loops = 0

    self.inventoryManager = InventoryManager:new(self)
    self.timeManager = TimeManager:new(self)
    self.effectManager = EffectManager:new(self)
    self.dialogManager = DialogManager:new(self)

    self.timer = Interval:new(CICLE_TIME)
    self.endTimer = Interval:new(CICLE_END_TIME)
    self.endTimer.onEndSignal:add( function() self:startNewLoop() end )
    self.endTimerPre = Interval:new(CICLE_END_TIME-0.5) --for white flash
    self.endTimerPre.onEndSignal:add( function() self.effectManager:startEffect("whiteFadeOut", {duration = 0.5}) end )

    self:startNewLoop()
    self.camera = Camera:new()

    self.canvasGame = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight(), { msaa  = 16})
    self.canvasFinalPass = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight(), { msaa  = 16})
  end

  function Level:exitedState()
  end

  function Level:doUpdate(dt)
    if DEBUG_ENABLED then
      if self.controls:isKeyboardDown("1", 0) then
        self:gotoMap("test")
      end

      if self.controls:isKeyboardDown("2", 0) then
        self:gotoMap("home")
      end

      if self.controls:isKeyboardDown("3", 0) then
        self:gotoMap("city")
      end

      if self.controls:isKeyboardDown("4", 0) then
        self:gotoMap("church")
      end

      if self.controls:isKeyboardDown("5", 0) then
        self:gotoMap("cityOutChurch")
      end
    end

    if self.controls:isKeyboardDown("z", 0) then
      print("tmp")
      --self.timeManager:startTimeUpEffect()
      --self.effectManager:startShaderEffect_Game("circleRipple")
      --self.timeManager.timeUp_Tween_scale1:start()
    end

    self.world:update(dt)

    local playerPosition = self.player:getOrtPosition()
    self.camera:setFocus(playerPosition.x, playerPosition.y)

    if not self.dialogManager.isStarted then
      self.timer:update(dt)
    end
    self.endTimer:update(dt)
    self.endTimerPre:update(dt)

    if not self.timer.isStarted and self.store.data.inventory.clock then
      self.timer:start()
    end

    self.inventoryManager:update(dt)
    self.timeManager:update(dt)
    self.effectManager:update(dt)
    self.dialogManager:update(dt)
  end

  function Level:doDraw()
    love.graphics.setCanvas(self.canvasGame) --draw game
      love.graphics.clear(0,0,0,1)
      self.camera:push()

      if self.map.props.background_image then
        if self.map.props.big_img then

          self.floorSprites[1]:draw(vec2d(
            self.map.props.background_offset_x,
            self.map.props.background_offset_y
          ))

          self.floorSprites[2]:draw(vec2d(
            self.map.props.background_offset_x + self.floorSprites[1].image:getWidth(),
            self.map.props.background_offset_y
          ))

          self.floorSprites[3]:draw(vec2d(
            self.map.props.background_offset_x + self.floorSprites[1].image:getWidth(),
            self.map.props.background_offset_y + self.floorSprites[1].image:getHeight()
          ))

          self.floorSprites[4]:draw(vec2d(
            self.map.props.background_offset_x,
            self.map.props.background_offset_y + self.floorSprites[1].image:getHeight()
          ))

        else
          self.floorSprite:draw(vec2d(
            self.map.props.background_offset_x,
            self.map.props.background_offset_y
          ))
        end

      end

      self.world:draw()
      self.camera:pop()
    love.graphics.setCanvas()

    love.graphics.setCanvas(self.canvasFinalPass)
      self.effectManager:setShader_Game()
      love.graphics.draw(self.canvasGame)
      self.effectManager:endShader()

      self.inventoryManager:draw()
      self.timeManager:draw()
      self.dialogManager:draw()
      --self.effectManager:draw()
    love.graphics.setCanvas()

    self.effectManager:setShader_All()
      love.graphics.draw(self.canvasFinalPass)
    self.effectManager:endShader()
    self.effectManager:draw()

    if self.isDebug then
      love.graphics.print( getTimeText(self.timer.time), FONT_DEBUG, love.graphics.getWidth() - 60, 20)

      love.graphics.print("px=" .. self.player.position.x, 12, love.graphics.getHeight() - 32)
      love.graphics.print("py=" .. self.player.position.y, 12, love.graphics.getHeight() - 12)

      local itemY = 0
      for item, flag in pairs(self.store.data.inventory) do
        local value = flag and "[X]" or "[ ]"
        love.graphics.print(item .. " " .. value, 12, 32 + 12 * itemY)
        itemY = itemY + 1
      end
    end
  end

  function Level:startNewLoop()
    self.loops = self.loops + 1

    if FIRST_LOAD_MAP ~= START_MAP then
      self:gotoMap(FIRST_LOAD_MAP)
      FIRST_LOAD_MAP = START_MAP
    end

    self:gotoMap(START_MAP)
    --self:gotoMap("churchEndScene")
    --self:gotoMap("church")

    self.timer = Interval:new(CICLE_TIME)
    --self.timer.onEndSignal:add( function() self:startNewLoop() end )
    self.timer.onEndSignal:addOnce(
      function()
        self.store:reset()
        self.endTimer:start()
        self.endTimerPre:start()
        self.effectManager:startEffect("fadeCircle")
        self.effectManager:startShaderEffect_All("screenWave")
      end )

    self.timeManager.isGetted = false
    self.store:reset()

    self.dialogManager:start("day")

    if self.loops == 1 then
      self.effectManager:startEffect("blackFadeIn")
    else
      self.effectManager:startEffect("whiteFadeIn")
    end
  end

  function Level:gotoMap(mapName)
    local map = MAPS[mapName]

    self.world = World:new(self, {
      grid = {
        dimension = map.data:getGridDimension()
      }
    })

    self.map = Map:new(self, map.data)

    if self.map.props.big_img then
      --print(string.sub(self.map.props.background_image, 0, string.len(self.map.props.background_image)-4))
      local startImgName = string.sub(self.map.props.background_image, 0, string.len(self.map.props.background_image)-4)
      self.floorSprites = {}
      self.floorSprites[1] = Sprite:new({image = startImgName .. "_1.png"  })
      self.floorSprites[2] = Sprite:new({image = startImgName .. "_2.png"  })
      self.floorSprites[3] = Sprite:new({image = startImgName .. "_3.png"  })
      self.floorSprites[4] = Sprite:new({image = startImgName .. "_4.png"  })
    else
      if self.map.props.background_image then
        self.floorSprite = Sprite:new({
          --image = "images/level_floor/city_big_test.png"
          image = self.map.props.background_image
        })
      end
    end

    self.player = Player:new(self, {
      position = map.position
    })

    self.cat = Cat:new(self, {
      position = vec3d(map.position.x - 100, map.position.y, map.position.z)
    })
  end
end
