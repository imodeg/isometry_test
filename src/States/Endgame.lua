local Camera = require("Core.Camera")
local Map = require("Map.Map")
local Player = require("Entities.Player")
local Tiled = require("Utils.Tiled")
local World = require("Core.World")
local Interval = require("Core.Interval")

local Sprite = require("Core.Sprite")

return function (Game)
  local Endgame = Game:addState("Endgame")

  function Endgame:enteredState()
    self.mainEndgameBack = Sprite:new({image = "images/menu/end_game.png"})

  end

  function Endgame:exitedState()

  end

  function Endgame:doUpdate(dt)
    if self.controls:isKeyboardDown("space", 0) or self.controls:isKeyboardDown("enter", 0) then
      self:gotoState("Menu")
    end
  end

  function Endgame:doDraw()
    self.mainEndgameBack:draw(vec2d(0,0))

    --love.graphics.print( "Press \"SPACEBAR\" to start", FONT_CLOCK, 630, 650)
  end

end
