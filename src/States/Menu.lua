local Camera = require("Core.Camera")
local Map = require("Map.Map")
local Player = require("Entities.Player")
local Tiled = require("Utils.Tiled")
local World = require("Core.World")
local Interval = require("Core.Interval")

local Sprite = require("Core.Sprite")

return function (Game)
  local Menu = Game:addState("Menu")

  function Menu:enteredState()
    self.mainMenuBack = Sprite:new({image = "images/menu/main_menu.png"})

    self.langButtonStartPosX = 680
    self.langButtonPosY = 460
    self.langButtonWidth = 170
    self.langButtonHeight = 80
    self.langButtonsGap = 20

    self.mouseControls:add(1, self.langButtonStartPosX, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight, function() LANG = "en" end)
    self.mouseControls:add(1, self.langButtonStartPosX + self.langButtonWidth + self.langButtonsGap, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight, function() LANG = "ru" end)
    self.mouseControls:add(1, self.langButtonStartPosX + self.langButtonWidth*2 + self.langButtonsGap*2, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight, function() LANG = "jp" end)

    self.mouseControls:add(1, 688, 565, 50, 50, function() self:prevLanguage() end)
    self.mouseControls:add(1, 1170, 565, 50, 50, function() self:nextLanguage() end)
  end

  function Menu:exitedState()
    self.mouseControls:ramoveAllEvents()
  end

  function Menu:doUpdate(dt)
    if self.controls:isKeyboardDown("left", 0) then
      self:prevLanguage()
    end

    if self.controls:isKeyboardDown("right", 0) then
      self:nextLanguage()
    end

    if self.controls:isKeyboardDown("space", 0) or self.controls:isKeyboardDown("enter", 0) then
      self:gotoState("Loading")
    end
  end

  function Menu:nextLanguage()
    if LANG == "en" then
      LANG = "ru"
    elseif LANG == "ru" then
      LANG = "jp"
    elseif LANG == "jp" then
      LANG = "en"
    end
  end

  function Menu:prevLanguage()
    if LANG == "en" then
      LANG = "jp"
    elseif LANG == "ru" then
      LANG = "en"
    elseif LANG == "jp" then
      LANG = "ru"
    end
  end

  function Menu:doDraw()
    self.mainMenuBack:draw(vec2d(0,0))

    pushColor()
    love.graphics.setColor(1, 1, 1, 0.2)
    love.graphics.rectangle( "fill", self.langButtonStartPosX, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight)
    love.graphics.rectangle( "fill", self.langButtonStartPosX + self.langButtonWidth + self.langButtonsGap, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight)
    love.graphics.rectangle( "fill", self.langButtonStartPosX + self.langButtonWidth*2 + self.langButtonsGap*2, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight)

    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setLineWidth( 3.0 )
    if LANG == "en" then
      love.graphics.printf( "ENGLISH", FONT_CLOCK, 650, 570, 605, "center")
      love.graphics.rectangle( "line", self.langButtonStartPosX, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight)
    elseif LANG == "ru" then
      love.graphics.printf( "РУССКИЙ", FONT_CLOCK, 650, 570, 605, "center")
      love.graphics.rectangle( "line", self.langButtonStartPosX + self.langButtonWidth + self.langButtonsGap, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight)
    elseif LANG == "jp" then
      love.graphics.printf( "日本語", FONT_JPN_BIG, 650, 570, 605, "center")
      love.graphics.rectangle( "line", self.langButtonStartPosX + self.langButtonWidth*2 + self.langButtonsGap*2, self.langButtonPosY, self.langButtonWidth, self.langButtonHeight)
    end
    love.graphics.setLineWidth(1)
    popColor()

    love.graphics.printf( "<", FONT_JPN_BIG, 700, 570, 555, "left")
    love.graphics.printf( ">", FONT_JPN_BIG, 700, 570, 505, "right")

    love.graphics.printf( "ENG", FONT_CLOCK, self.langButtonStartPosX, self.langButtonPosY+20, self.langButtonWidth, "center")
    love.graphics.printf( "PУС", FONT_CLOCK, self.langButtonStartPosX + self.langButtonWidth + self.langButtonsGap, self.langButtonPosY+20, self.langButtonWidth, "center")
    love.graphics.printf( "日本語", FONT_JPN_BIG, self.langButtonStartPosX + self.langButtonWidth*2 + self.langButtonsGap*2, self.langButtonPosY+20, self.langButtonWidth, "center")

    love.graphics.print( "Press \"SPACEBAR\" to start", FONT_CLOCK, 630, 650)
  end

end
