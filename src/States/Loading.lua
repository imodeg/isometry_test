local Camera = require("Core.Camera")
local Map = require("Map.Map")
local Player = require("Entities.Player")
local Tiled = require("Utils.Tiled")
local World = require("Core.World")
local Interval = require("Core.Interval")

local Sprite = require("Core.Sprite")

return function (Game)
  local Loading = Game:addState("Loading")

  function Loading:enteredState()
    self.timerToLoad = Interval:new(0.5)
  end

  function Loading:exitedState()

  end

  function Loading:doUpdate(dt)
    self.timerToLoad:update(dt)
    if self.timerToLoad.time <= 0 then
      self:gotoState("Level")
    end
  end

  function Loading:doDraw()
    love.graphics.print( "Loading", FONT_CLOCK, 540, 340)
  end

end
