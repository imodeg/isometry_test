local Entity = require("Entities.Entity")

local EntityStatic = Entity:subclass("EntityStatic")

function EntityStatic:initialize(game, def)
  Entity.initialize(self, game, def)
end

return EntityStatic
