local Entity = require("Entities.Entity")

local EntityDynamic = Entity:subclass("EntityDynamic")

function EntityDynamic:initialize(game, def)
  Entity.initialize(self, game, def)

  self.nextPosition = def.position

  self.mass = 1
  self.velocity = vec3d(0, 0, 0)
  self.acceleration = vec3d(0, 0, 0)

end

function EntityDynamic:move(vec)
  local collisions, length = self.world:move(self, vec)

  self.worldAABB = self:getWorldAABB(self.position)

  return collisions, length
end

function EntityDynamic:applyAcceleration(dt)
  self.velocity = vec3dSum(self.velocity, vec3dMultScalar(self.acceleration, dt))
end

function EntityDynamic:applyVelocity(dt)
  self.nextPosition = vec3dSum(self.nextPosition, vec3dMultScalar(self.velocity, dt))
end

function EntityDynamic:applyForce(forceVec)
  self.velocity = vec3dSum(self.velocity, vec3dDivideScalar(forceVec, self.mass))
end

return EntityDynamic
