local EntityDynamic = require("Entities.EntityDynamic")
local Sprite = require("Core.Sprite")
local Stateful = require("stateful")
local Clip = require("Core.Clip")
local Cloud = require("Dialog.Cloud")


local Player = EntityDynamic:subclass("Player")
Player:include(Stateful)

function Player:initialize(game, def)
  EntityDynamic.initialize(self, game, {
    position = def.position,
    hitbox = vec3d(25, 25, 125)
  })

  self.sprite = Sprite:new({
    image = "images/Player1.png",
    offset = vec2d(-64, -190)
  })

  self.clip =  Clip:new({
      file = "images/player_spritesheet.png",
      w = 140,
      h = 207,
      animations = {
        idle_front = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ,13, 14 ,15, 16, true, .08},
        run_front = {17, 18, 19, 20, 21, 22, 23, 24, true, .08},
        idle_back = {25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, true, .08},
        run_back = {41, 42, 43, 44, 45, 46, 47, 48, true, .08}
      },
      offset = {
        x = -70,
        y = -190,
      }
    })

  self.clip:play("idle_front")

  self.dirrection = vec3d(0, 0, 0)
  self.dirClip = 1

  self.controls = game.controls
  self.speed = 350
  self.gravity = 0
  self.isCollide = false

  self.velocity = vec3d(0, 0, 0)
  self.acceleration = vec3d(0, 0, -self.gravity)
  self:gotoState('Stand')

  self.cloud = Cloud:new(game)
end

function Player:update(dt)
  self.nextPosition = self.position
  self:applyAcceleration(dt)
  self:applyVelocity(dt)
  self:inputUpdate(dt)

  local collisions, length = self:move(self.nextPosition)

  self:doUpdate(dt, collisions)
  self.clip:update(dt)

  self.isCollide = length > 0
  self.position = self.nextPosition
end

function Player:inputUpdate(dt)

end

function Player:doUpdate(dt, data)
  self.cloud:update(dt)
end

function Player:draw()
  local OrtCoordinates = self:getOrtPosition()
  --self.sprite:draw(OrtCoordinates)
  self.clip:draw(OrtCoordinates.x, OrtCoordinates.y, self.dirClip)
  EntityDynamic.draw(self)

  self.cloud:draw(OrtCoordinates.x+30, OrtCoordinates.y-140)
end

local PlayerState_Stand = Player:addState("Stand")
local PlayerState_Walk = Player:addState("Walk")
local PlayerState_Dialog = Player:addState("Dialog")

function PlayerState_Stand:enteredState()
  if (self.dirrection.x == -1 and self.dirrection.y == -1) or
     (self.dirrection.x == 0 and self.dirrection.y == -1) or
     (self.dirrection.x == 1 and self.dirrection.y == -1) then
    self.clip:play("idle_back")
  else
    self.clip:play("idle_front")
  end
end

function PlayerState_Stand:inputUpdate(dt)
  local direction = vec3d(0, 0, 0)

  if self.controls:isKeyboardDown('up') or self.controls:isKeyboardDown('w') then
    self:gotoState('Walk')
    direction.y = -1
  end

  if self.controls:isKeyboardDown('down') or self.controls:isKeyboardDown('s') then
    self:gotoState('Walk')
    direction.y = 1
  end

  if self.controls:isKeyboardDown('left') or self.controls:isKeyboardDown('a') then
    self:gotoState('Walk')
    direction.x = -1
    self.dirClip = -1
  end

  if self.controls:isKeyboardDown('right') or self.controls:isKeyboardDown('d') then
    self:gotoState('Walk')
    direction.x = 1
    self.dirClip = 1
  end

  if direction.x ~= 0 or direction.y ~= 0 then
    self.dirrection = direction
  end

  local deltaPosition = vec3dMultScalar(
    vec3dNormalize(vec3dRotate45(direction)),
    self.speed * dt
  )

  self.nextPosition = vec3dSum(self.nextPosition, deltaPosition)
end

function PlayerState_Stand:doUpdate(dt, collisions)
  for _, collision in pairs(collisions) do
    if collision == 'z+' then
      self.velocity = vec3d(self.velocity.x, self.velocity.y, 0)
    end
  end
  self.cloud:update(dt)
end

function PlayerState_Walk:enteredState()
  if (self.dirrection.x == -1 and self.dirrection.y == -1) or
     (self.dirrection.x == 0 and self.dirrection.y == -1) or
     (self.dirrection.x == 1 and self.dirrection.y == -1) then
    self.clip:play("run_back")
  else
    self.clip:play("run_front")
  end
end

function PlayerState_Walk:inputUpdate(dt)
  local direction = vec3d(0, 0, 0)

  if self.controls:isKeyboardDown('up') or self.controls:isKeyboardDown('w') then
    --self:gotoState('Walk')
    direction.y = -1
  end

  if self.controls:isKeyboardDown('down') or self.controls:isKeyboardDown('s') then
    --self:gotoState('Walk')
    direction.y = 1
  end

  if self.controls:isKeyboardDown('left') or self.controls:isKeyboardDown('a') then
    --self:gotoState('Walk')
    direction.x = -1
    self.dirClip = -1
  end

  if self.controls:isKeyboardDown('right') or self.controls:isKeyboardDown('d') then
    --self:gotoState('Walk')
    direction.x = 1
    self.dirClip = 1
  end

  if direction.x ~= 0 or direction.y ~= 0 then
    self.dirrection = direction
    if (self.dirrection.x == -1 and self.dirrection.y == -1) or
       (self.dirrection.x == 0 and self.dirrection.y == -1) or
       (self.dirrection.x == 1 and self.dirrection.y == -1) then
      self.clip:play("run_back")
    else
      self.clip:play("run_front")
    end
  end

  if not (self.controls:isKeyboardDown('up') or self.controls:isKeyboardDown('down') or
         self.controls:isKeyboardDown('left') or self.controls:isKeyboardDown('right') or
         self.controls:isKeyboardDown('w') or self.controls:isKeyboardDown('s') or
         self.controls:isKeyboardDown('a') or self.controls:isKeyboardDown('d')) then
    self:gotoState('Stand')
  end

  local deltaPosition = vec3dMultScalar(
    vec3dNormalize(vec3dRotate45(direction)),
    self.speed * dt
  )

  self.nextPosition = vec3dSum(self.nextPosition, deltaPosition)
end

function PlayerState_Walk:doUpdate(dt, collisions)
  for _, collision in pairs(collisions) do
    if collision == 'z+' then
      self.velocity = vec3d(self.velocity.x, self.velocity.y, 0)
    end
  end
  self.cloud:update(dt)
end

function PlayerState_Dialog:enteredState()
  if (self.dirrection.x == -1 and self.dirrection.y == -1) or
     (self.dirrection.x == 0 and self.dirrection.y == -1) or
     (self.dirrection.x == 1 and self.dirrection.y == -1) then
    self.clip:play("idle_back")
  else
    self.clip:play("idle_front")
  end
end

function PlayerState_Dialog:doUpdate()
end

function PlayerState_Dialog:inputUpdate()
end

return Player
