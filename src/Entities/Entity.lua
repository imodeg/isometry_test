local Entity = class("Entity")

function Entity:initialize(game, def)
  self.game = game
  self.world = game.world

  self.position = def.position
  self.hitbox = def.hitbox
  self.hitboxHalf = vec3dDivideScalar(self.hitbox, 2)

  self.debugColor = {1, 1, 1}

  self.aabb = {
    min = vec3d(-self.hitboxHalf.x, -self.hitboxHalf.y, 0),
    max = vec3d(self.hitboxHalf.x, self.hitboxHalf.y, self.hitbox.z)
  }
  self.worldAABB = self:getWorldAABB(self.position)

  -- Depth sorting helpers
  self.depth = 0
  self.entitiesBehind = {}
  self.depthSortVisited = false

  self.world:add(self)
end

function Entity:destroy()
  self.world:remove(self)
end

function Entity:getOrtPosition()
  return iso2ort(self.position)
end

function Entity:getWorldAABB(position)
  return {
    min = vec3dSum(self.aabb.min, position),
    max = vec3dSum(self.aabb.max, position)
  }
end

function Entity:isBehind(entity)
  return self.worldAABB.min.x < entity.worldAABB.max.x and
    self.worldAABB.min.y < entity.worldAABB.max.y and
    self.worldAABB.min.z < entity.worldAABB.max.z
end

function Entity:collide()
end

function Entity:update(dt)
  -- @todo correct position according to physics
end

function Entity:draw()
  if not self.game.isDebug then
    return
  end

  pushColor()
  love.graphics.setColor(self.debugColor[1], self.debugColor[2], self.debugColor[3], 1)

  local topSquare = self:_getOrtSquare(self.hitbox.z)
  local bottomSquare = self:_getOrtSquare(0)

  self:_drawSquare(topSquare)
  self:_drawSquare(bottomSquare)

  for i = 1, 4 do
    love.graphics.line(
      topSquare[i].x, topSquare[i].y,
      bottomSquare[i].x, bottomSquare[i].y
    )
  end

  local ortPos = self:getOrtPosition()
  love.graphics.circle(
    "fill",
    math.floor(ortPos.x),
    math.floor(ortPos.y),
    2
  )
  popColor()
end

function Entity:_drawSquare(square)
  love.graphics.line(
    square[1].x, square[1].y,
    square[2].x, square[2].y,
    square[3].x, square[3].y,
    square[4].x, square[4].y,
    square[1].x, square[1].y
  )
end

function Entity:_getOrtSquare(z)
  local offsets = {
    vec3d(-self.hitboxHalf.x, -self.hitboxHalf.y, z),
    vec3d(self.hitboxHalf.x,  -self.hitboxHalf.y, z),
    vec3d(self.hitboxHalf.x,  self.hitboxHalf.y, z),
    vec3d(-self.hitboxHalf.x, self.hitboxHalf.y, z)
  }

  local points = {}
  for _, offset in ipairs(offsets) do
    table.insert(points, iso2ort(vec3dSum(self.position, offset)))
  end

  return points
end

return Entity
