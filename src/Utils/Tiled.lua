-- local Tile = require("Map.Tile")

local Tiled = class("Tiled")

local JUMP_HEIGHT = 4

function Tiled:initialize(data)
  self.data = data
end

function Tiled:getGridDimension()
  return vec3d(self.data.width, self.data.height, #self.data.layers + JUMP_HEIGHT)
end

function Tiled:getTileDefs()
  local defs = {}

  for _, tileset in pairs(self.data.tilesets) do
    for _, def in pairs(tileset.tiles) do
      defs[tileset.firstgid + def.id] = {
        type = def.type,
        hitbox = self:getTileHitbox(def),
        props = def.properties,
        sprite = {
          image = fixImagePath(def.image),
          offset = vec2d(
            -def.width / 2,
            self.data.tileheight - def.height
          ),
        }
      }
    end
  end
  return defs
end

function Tiled:getTileHitbox(def)
  local props = def.properties

  if props ~= nil then
    return vec3d(props.hitbox_x, props.hitbox_y, props.hitbox_z)
  end

  return nil
end

function Tiled:getTilesData()
  local tilesData = {}

  for z, layer in pairs(self.data.layers) do
    if layer.type == "tilelayer" and layer.name ~= "floor" then
      for x = 1, layer.width do
        for y = 1, layer.height do
          local index = y * x + (y - 1) * (layer.width - x)
          local tileDefId = layer.data[index]

          if tileDefId > 0 then
            table.insert(tilesData, {
              tileDefId = tileDefId,
              gridPosition = vec3d(x, y, z)
            })
          end
        end
      end
    end
  end


  return tilesData
end

function Tiled:getObjectsData()
  local objects = {}

  for _, layer in pairs(self.data.layers) do
    if layer.type == "objectgroup" then
      for _, obj in pairs(layer.objects) do
        local w = obj.width
        local h = obj.height

        table.insert(objects, {
          name = obj.name,
          position = vec3d(obj.x, obj.y, -layer.offsety),
          hitbox = vec3d(w, h, 1),
          defs = obj.properties
        })
      end
    end
  end

  return objects
end

function Tiled:getImagesData()
  local images = {}

  for _, layer in pairs(self.data.layers) do
    local props = layer.properties
    if layer.type == "imagelayer" and props.type == "house" then
      local x = layer.offsetx - 5650
      local y = layer.offsety + 870
      local ov = ort2iso(vec2d(x, y))

      table.insert(images, {
        sprite = {
          image = fixImagePath(layer.image),
          offset = vec2d(-750, -870)
        },
        hitbox = vec3d(props.hitbox_x, props.hitbox_y, props.hitbox_z),
        position = vec3d(ov.x, ov.y, 25)
      })
    end
  end

  return images
end

function fixImagePath(path)
  return string.sub(path, 3)
end

return Tiled
