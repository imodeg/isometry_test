function pointInsideRectangle(point_x, point_y, rect_start_x, rect_start_y, rect_size_x, rect_size_y)
  if (point_x > rect_start_x) and (point_x < (rect_start_x+rect_size_x))
  and (point_y > rect_start_y) and (point_y < (rect_start_y+rect_size_y)) then
    return true
  end
  return false
end
