function math.round(x)
  return math.floor(x + .5)
end

function getTimeText(seconds)
  local result_min = math.floor(seconds/60)
  local result_sec = math.floor(seconds - result_min*60)
  local result_text = result_min .. ":" .. result_sec
  return result_text
end
