function table.extend(destination, source)
  for k,v in pairs(source) do
    destination[k] = v
  end
  return destination
end