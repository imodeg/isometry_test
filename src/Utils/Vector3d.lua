function vec3d(x, y, z)
  return { x = x, y = y or 0, z = z or 0 }
end

function vec3dCopy(v)
  return vec3d(v.x, v.y, v.z)
end

function vec3dSum(v1, v2)
  return vec3d(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z)
end

function vec3dSub(v1, v2)
  return vec3d(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z)
end

function vec3dMult(v1, v2)
  return vec3d(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z)
end

function vec3dDivide(v1, v2)
  return vec3d(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z)
end

function vec3dMultScalar(v, scalar)
  return vec3d(v.x * scalar, v.y * scalar, v.z * scalar)
end

function vec3dDivideScalar(v, scalar)
  return vec3d(v.x / scalar, v.y / scalar, v.z / scalar)
end

function vec3dLength(v)
  return math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z)
end

function vec3dNormalize(v)
  local length = vec3dLength(v)

  if length > 0 then
    return vec3dDivideScalar(v, length)
  else
    return v
  end
end

local cos45 = math.cos(-math.pi / 4)
local sin45 = math.sin(-math.pi / 4)
function vec3dRotate45(v)
  return vec3d(
    v.x * cos45 - v.y * sin45,
    v.x * sin45 + v.y * cos45,
    0
  )
end

function isVec3dEquals(v1, v2)
  return v1.x == v2.x and v1.y == v2.y and v1.z == v2.z
end

function vec3dInvert(v)
  return vec3d(-v.x, -v.y, -v.z)
end
