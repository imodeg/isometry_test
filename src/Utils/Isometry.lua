function iso2ort(vec)
  return {
    x = vec.x - vec.y,
    y = (vec.x + vec.y) / 2 - vec.z,
  }
end

function ort2iso(vec)
  return {
    x = (2 * vec.y + vec.x) / 2,
    y = (2 * vec.y - vec.x) / 2
  }
end
