local colorStack = {}

function pushColor()
  table.insert(colorStack, {love.graphics.getColor()})
end

function popColor()
  local c = colorStack[#colorStack]
  love.graphics.setColor(c[1], c[2], c[3], c[4])
  table.remove(colorStack, #colorStack)
end
