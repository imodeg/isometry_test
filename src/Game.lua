local Stateful = require("stateful")
local Game = class("Game")
local Store = require("Store")
local Controls = require("Core.Controls")
local MouseControls = require("Core.MouseControls")

Game:include(Stateful)

local Tiled = require("Utils.Tiled")

function Game:initialize()
  self.isDebug = DEBUG_ENABLED
  self.loops = 0
  self.store = Store:new()
  self.controls = Controls:new()
  self.mouseControls = MouseControls:new()
end

function Game:keyboardPressed(key)
  self.controls:keyboardPressed(key)
end

function Game:keyboardReleased(key)
  self.controls:keyboardReleased(key)
end

function Game:mousePressed( x, y, button, istouch, presses )
  self.mouseControls:mousepressed(x, y, button, istouch, presses)
end

function Game:mouseReleased( x, y, button, istouch, presses )
  self.mouseControls:mousereleased(x, y, button, istouch, presses)
end

function Game:update(dt)
  if DEBUG_ENABLED then
    if self.controls:isKeyboardDown("`", 0) or self.controls:isKeyboardDown("0", 0) then
      self.isDebug = not self.isDebug
    end

    if self.controls:isKeyboardDown("l", 0) then
      if LANG == "ru" then
        LANG = "en"
      else
        LANG = "ru"
      end
    end
  end

  self:doUpdate(dt)
  self.controls:update(dt)
end

function Game:draw()
  self:doDraw()

  if DEBUG_ENABLED then
    love.graphics.setFont(FONT_DEBUG)
    love.graphics.print("FPS = " .. love.timer.getFPS(), 12, 12)
    self.mouseControls:draw()
  end
end

function Game:doUpdate(dt) end
function Game:doDraw() end

require("States.Level")(Game)
require("States.Menu")(Game)
require("States.Loading")(Game)
require("States.Endgame")(Game)

return Game
