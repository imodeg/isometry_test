---
-- @classmod Core.Camera
local Camera = class("Camera")

function Camera:initialize()
  self.x = 0
  self.y = 0

  self.w = love.graphics.getWidth()
  self.h = love.graphics.getHeight()

  self.maxX = self.w
  self.maxY = self.h

  self.bounds = {
    min = nil,
    max = nil,
  }

  self.scale = {x = 1, y = 1}
end

function Camera:push()
  love.graphics.push()
  love.graphics.scale(self.scale.x, self.scale.y)
  love.graphics.translate(-self.x, -self.y)
end

function Camera:pop()
  love.graphics.pop()
end

function Camera:setBounds(x, y, w, h)
  self.bounds.min = {x = x, y = y}
  self.bounds.max = {x = x + w, y = y + h}

  for _, c in ipairs({'x', 'y'}) do
    if self.bounds.min[c] and self.bounds.max[c] and self.bounds.min[c] > self.bounds.max[c] then
      self.bounds.max[c] = self.bounds.min[c]
      self[c] = self.bounds.min[c]
    end
  end

  self:_positionToBounds()
end

function Camera:getFrameBounds()
  return self.x, self.y, self.w, self.h
end

function Camera:isInFrameBounds(entity)
  local w = entity.w or 0
  local h = entity.h or 0

  local bx, by, bw, bh = self:getFrameBounds()

  return entity.x + w > bx - 32
    and entity.y + h > by - 32
    and entity.x < bx + bw + 32
    and entity.y < by + bh + 32
end

function Camera:move(dx, dy)
  self.x = self.x + (dx or 0)
  self.y = self.y + (dy or 0)

  self:_positionToBounds()
end

function Camera:setPosition(x, y)
  self.x = x or self.x
  self.y = y or self.y

  self:_positionToBounds()
end

function Camera:setFocus(x, y)
  self.x = x - self.w / 2 / self.scale.x
  self.y = y - self.h / 2 / self.scale.y

  self:_positionToBounds()
end

function Camera:setScale(sx, sy)
  self.scale.x = sx or self.scale.x
  self.scale.y = sy or sx or self.scale.y
end

function Camera:_positionToBounds()
  self.x = math.floor(self.x)
  self.y = math.floor(self.y)

  if self.bounds.min then
    for _, c in ipairs({'x', 'y'}) do
      if self.bounds.min[c] and self[c] < self.bounds.min[c] then
        self[c] = self.bounds.min[c]
      end
    end
  end

  if self.bounds.max then
    for _, c in ipairs({'x', 'y'}) do
      local side = self[c == 'x' and 'w' or 'h'] / self.scale[c]

      if self.bounds.max[c] and self[c] > self.bounds.max[c] - side then
        self[c] = self.bounds.max[c] - side
      end
    end
  end

  self.maxX = self.x + self.w / self.scale.x
  self.maxY = self.y + self.h / self.scale.y
end

return Camera
