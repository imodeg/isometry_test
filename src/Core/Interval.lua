local Signal = require("Core.Event.Signal")
---
-- @classmod Core.Interval
local Interval = class("Interval")

---
-- @number length Interval lenght in seconds (positive)
function Interval:initialize(length, isRepeat)
  self.time = 0
  self:setLength(length)
  self.isStarted = false
  self.isRepeat = false or isRepeat

  self.onEndSignal = Signal:new()
end

---
-- Set interval length
function Interval:setLength(length)
  assert(
    type(length) == "number" and length >= 0,
    "Interval length must be a positive number"
  )

  self.length = length
end

---
-- Start interval
function Interval:start()
  self.time = self.length
  self.isStarted = true
end

---
-- Is interval stopped
function Interval:isStopped()
  return self.time <= 0
end

---
-- Stop interval
function Interval:stop()
  self.time = 0
  self.isStarted = false
end

---
-- Get interval progress
-- @treturn number between 0 and 1
function Interval:progress()
  return (self.length - self.time) / self.length
end

---
-- @number dt
function Interval:update(dt)
  if self.time > 0 then
    self.time = self.time - dt;
    if self.time < 0 then
      if self.isRepeat then
        self:start()
        self.onEndSignal:execute()
      else
        self.time = 0
        self.isStarted = false
        self.onEndSignal:execute()
      end
    end
  end
end

return Interval
