local Grid = class("Grid")

local GRID_SIZE = vec3d(50, 50, 25)
local DRAW_RADIUS = 16
local UPDATE_RADIUS = 16

function Grid:initialize(game, def)
  self.game = game

  self.dimension = def.dimension

  self.cells = {}
  for x = 1, self.dimension.x do
    self.cells[x] = {}
    for y = 1, self.dimension.y do
      self.cells[x][y] = {}
      for z = 1, self.dimension.z do
        self.cells[x][y][z] = {}
      end
    end
  end
end

function Grid:add(entity)
  local gridPosition = self:toGridPosition(entity.position)
  self:addToCell(entity, gridPosition)
end

function Grid:move(entity, nextPosition)
  local gridPosition = self:toGridPosition(entity.position)
  local nextGridPosition = self:toGridPosition(nextPosition)

  if not isVec3dEquals(gridPosition, nextGridPosition) then
    self:removeFromCell(entity, gridPosition)
    self:addToCell(entity, nextGridPosition)
  end
end

function Grid:addToCell(entity, position)
  local cell = self:getCell(position)
  if cell ~= nil then
    table.insert(cell, entity)
  end
end

function Grid:removeFromCell(entity, position)
  local cell = self:getCell(position)
  if cell ~= nil then
    for i = #cell, 1, -1 do
      if cell[i] == entity then
        table.remove(cell, i)
      end
    end
  end
end

function Grid:getCell(position)
  if self:inBounds(position) then
    return self.cells[position.x][position.y][position.z]
  end

  return nil
end

function Grid:toGridPosition(position)
  local gridPosition = vec3dDivide(position, GRID_SIZE)
  return vec3d(
    math.round(gridPosition.x) + 1,
    math.round(gridPosition.y) + 1,
    math.round(gridPosition.z) + 1
  )
end

function Grid:fromGridPosition(position)
  return vec3dMult(
    vec3dSub(position, vec3d(1, 1, 1)),
    GRID_SIZE
  )
end

function Grid:findByType(type)
  local findedEntities = {}

  center = center or vec2d(0, 0)
  radius = radius or math.max(self.dimension.x, self.dimension.y)

  local entities = {}
  local minX = math.max(1, center.x - radius)
  local minY = math.max(1, center.y - radius)
  local maxX = math.min(self.dimension.x, center.x + radius)
  local maxY = math.min(self.dimension.y, center.y + radius)

  for x = minX, maxX do
    for y = minY, maxY do
      for z = 1, self.dimension.z do
        local cell = self.cells[x][y][z]
        for _, entity in ipairs(cell) do
          if entity ~= nil and entity.type == type then
            table.insert(findedEntities, entity)
          end
        end
      end
    end
  end

  return findedEntities
end

function Grid:findByTypeAndName(type,name)
  --local findedEntities = {}

  center = center or vec2d(0, 0)
  radius = radius or math.max(self.dimension.x, self.dimension.y)

  local entities = {}
  local minX = math.max(1, center.x - radius)
  local minY = math.max(1, center.y - radius)
  local maxX = math.min(self.dimension.x, center.x + radius)
  local maxY = math.min(self.dimension.y, center.y + radius)

  for x = minX, maxX do
    for y = minY, maxY do
      for z = 1, self.dimension.z do
        local cell = self.cells[x][y][z]
        for _, entity in ipairs(cell) do
          if entity ~= nil and entity.name == name and entity.type == type then
            return entity
          end
        end
      end
    end
  end

  --return findedEntities
end

function Grid:findByName(name)
  --local findedEntities = {}

  center = center or vec2d(0, 0)
  radius = radius or math.max(self.dimension.x, self.dimension.y)

  local entities = {}
  local minX = math.max(1, center.x - radius)
  local minY = math.max(1, center.y - radius)
  local maxX = math.min(self.dimension.x, center.x + radius)
  local maxY = math.min(self.dimension.y, center.y + radius)

  for x = minX, maxX do
    for y = minY, maxY do
      for z = 1, self.dimension.z do
        local cell = self.cells[x][y][z]
        for _, entity in ipairs(cell) do
          if entity ~= nil and entity.name == name then
            return entity
          end
        end
      end
    end
  end

  --return findedEntities
end

function Grid:remove(entity)
end

function Grid:inBounds(v)
  return v.x > 0
    and v.y > 0
    and v.z > 0
    and v.x <= self.dimension.x
    and v.y <= self.dimension.y
    and v.z <= self.dimension.z
end

function Grid:update(dt)
  local entities = self:flattenEntities(
    self:toGridPosition(self.game.player.position),
    UPDATE_RADIUS
  )

  for i = 1, #entities do
    entities[i]:update(dt)
  end
end

function Grid:draw()
  local maxDimension = math.max(self.dimension.x + 1, self.dimension.y + 1)

  local entities = self:flattenEntities(
    self:toGridPosition(self.game.player.position),
    DRAW_RADIUS
  )
  local sortedEntities = self:sortEntities(entities)

  for _, entity in ipairs(sortedEntities) do
    entity:draw()

    if self.game.isDebug then
      pushColor()
      local ortPos = entity:getOrtPosition()
      love.graphics.setColor(1, 0, 0, 1)
      local gp = self:toGridPosition(entity.position)
      love.graphics.print(tostring(entity.depth), ortPos.x - 12, ortPos.y - 20)
      popColor()
    end
  end
end

function Grid:flattenEntities(center, radius)
  center = center or vec2d(0, 0)
  radius = radius or math.max(self.dimension.x, self.dimension.y)

  local entities = {}
  local minX = math.max(1, center.x - radius)
  local minY = math.max(1, center.y - radius)
  local maxX = math.min(self.dimension.x, center.x + radius)
  local maxY = math.min(self.dimension.y, center.y + radius)

  for x = minX, maxX do
    for y = minY, maxY do
      for z = 1, self.dimension.z do
        local cell = self.cells[x][y][z]
        for _, entity in ipairs(cell) do
          if entity ~= nil then
            table.insert(entities, entity)
          end
        end
      end
    end
  end

  return entities
end

function Grid:sortEntities(entities)
  for i = 1, #entities do
    local a = entities[i]
    local behindIndex = 1

    for j = 1, #entities do
      if i ~= j then
        b = entities[j]

        if b:isBehind(a) then
          a.entitiesBehind[behindIndex] = b
          behindIndex = behindIndex + 1
        end
      end
    end

    a.depthSortVisited = false
  end

  local result = {}
  local depth = 0
  for i = 1, #entities do
    depth = self:visitEntity(entities[i], depth)
    result[entities[i].depth] = entities[i]
  end

  return result
end

function Grid:visitEntity(entity, currentDepth)
  if entity.depthSortVisited then
    return currentDepth
  end

  entity.depthSortVisited = true

  local depth = currentDepth

  for i = 1, #entity.entitiesBehind do
    if entity.entitiesBehind[i] then
      depth = self:visitEntity(entity.entitiesBehind[i], depth)
      entity.entitiesBehind[i] = nil
    end
  end

  entity.depth = depth + 1

  return entity.depth
end

return Grid
