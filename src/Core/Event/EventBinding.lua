local EventBinding = class('EventBinding')

function EventBinding:initialize(eventController, type, isOnce, callback)
  self.eventController = eventController
  self.type = type
  self.isOnce = isOnce
  self.callback = callback
  self.destroy = false
end

function EventBinding:remove()
  self.eventController:remove(self)
end

return EventBinding
