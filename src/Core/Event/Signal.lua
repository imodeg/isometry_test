local EventBinding = require("Core.Event.EventBinding")

local Signal = class("Signal")

function Signal:initialize()
  self.callbacks = {}
  self.callbacksOnce = {}
end

function Signal:execute(...)
  for _, eventBinding in ipairs(self.callbacks) do
    eventBinding.callback(...)
  end

  for _, eventBinding in ipairs(self.callbacksOnce) do
    eventBinding.callback(...)
  end
  self.callbacksOnce = {}
end

function Signal:add(callback)
  local newEventBinding = EventBinding:new(self, "Signal", false, callback)
  table.insert(self.callbacks, newEventBinding)
end

function Signal:addOnce(callback)
  local newEventBinding = EventBinding:new(self, "Signal", true, callback)
  table.insert(self.callbacksOnce, newEventBinding)
end

function Signal:remove(eventBinding)
  if(eventBinding.isOnce) then
    for i, inEventBinding in ipairs(self.callbacks) do
      if (inEventBinding == eventBinding) then
        table.remove(self.callbacks, i)
        break
      end
    end
  else
    for i, inEventBinding in ipairs(self.callbacksOnce) do
      if (inEventBinding == eventBinding) then
        table.remove(self.callbacksOnce, i)
        break
      end
    end
  end
end

function Signal:ramoveAll()
  self.callbacks = {}
  self.callbacksOnce = {}
end

return Signal
