---
-- @classmod Core.Controls

local Controls = class("Controls")

function Controls:initialize()
  self.keyboard = {keys = {}}
end

---
-- @string key
-- @number ticks
-- @treturn boolean
function Controls:isKeyboardDown(key, ticks)
  local value = self.keyboard.keys[key]
  return nil ~= value and 0 <= value and (nil == ticks or value <= ticks)
end

---
-- @number dt
function Controls:update(dt)
  for k, t in pairs(self.keyboard.keys) do
    if 0 <= t then
      self.keyboard.keys[k] = t + 1
    end
  end
end

---
-- Register keyboard key press
-- @string key
function Controls:keyboardPressed(key)
  if self.keyboard.keys[key] == nil or self.keyboard.keys[key] == -1 then
    self.keyboard.keys[key] = 0
  end
end

---
-- Register keyboard key release
-- @string key
function Controls:keyboardReleased(key)
  self.keyboard.keys[key] = -1
end

return Controls
