local Signal = require("Core.Event.Signal")
---
-- @classmod Core.Tween
local Tween = class("Tween")

---
-- Example (will linearly change x from 0 to 100 in exactly 500ms):
--
--    tween = Tween:new(0, 100, .5, "linear")
--    x = tween:update(dt)
--
-- @number from
-- @number to
-- @number duration In seconds
-- @number tween Tween function name from Tween.static.tweens
function Tween:initialize(from, to, duration, tween, loop, stoped)
  assert(
    type(Tween.static.tweens[tween]) == "function",
    "tween must be key of Tween.static.tweens"
  )

  self.from = from
  self.current = from
  self.to = to
  self.tween = tween
  self.duration = duration
  self.time = 0
  self.isStopped = stoped or false

  self.isLoop = loop or false
  self.dirForward = true --calculate tween time forward

  self.onEndSignal = Signal:new()
end

function Tween:stop()
  self.isStopped = true
end

function Tween:start()
  self.time = 0
  self.isStopped = false
end
---
-- @number dt
-- @treturn number Current value
function Tween:update(dt)
  if self.isStopped then
    return self.current
  end

  self.current = Tween.static.tweens[self.tween](
    self.time,
    self.from,
    self.to - self.from,
    self.duration
  )

  if(self.dirForward == true) then
    self.time = self.time + dt
  else
    self.time = self.time - dt
  end

  if self.time >= self.duration then
    if (self.isLoop == true) then
      self.dirForward = false
    else
      self.isStopped = true
    end
    self.onEndSignal:execute()
  end

  if self.time <= 0 then
    self.dirForward = true
  end

  return self.current
end

local function calculatePAS(p,a,c,d)
  p, a = p or d * 0.3, a or 0
  if a < math.abs(c) then return p, c, p / 4 end -- p, a, s
  return p, a, p / (2 * math.pi) * math.asin(c/a) -- p,a,s
end

Tween.static.tweens = {
  linear = function (t, b, c, d)
    return c * t / d + b
  end,
  easeInSine = function (t, b, c, d)
      return -c * math.cos(t / d * (math.pi / 2)) + c + b
  end,
  easeOutSine = function (t, b, c, d)
      return c * math.sin(t / d * (math.pi / 2)) + b
  end,
  easeInOutSine = function (t, b, c, d)
      return -c / 2 * (math.cos(math.pi * t / d) - 1) + b
  end,
  easeInQuad = function (t, b, c, d)
      t = t / d
      return c * t * t + b
  end,
  easeOutQuad = function (t, b, c, d)
      t = t / d
      return -c * t * (t - 2) + b
  end,
  easeInQuint = function (t, b, c, d)
      return c * math.pow(t / d, 5) + b
  end,
  easeOutQuint = function (t, b, c, d)
      return c * (math.pow(t / d - 1, 5) + 1) + b
  end,
  easeInOutCubic = function(t, b, c, d)
    t = t / d * 2
    if t < 1 then return c / 2 * t * t * t + b end
    t = t - 2
    return c / 2 * (t * t * t + 2) + b
  end,
  easeInCubic = function(t, b, c, d)
    return c * math.pow(t / d, 5) + b
  end,
  easeOutCubic = function(t, b, c, d)
    return c * (math.pow(t / d - 1, 5) + 1) + b
  end,
  inOutCirc = function (t, b, c, d)
    t = t / d * 2
    if t < 1 then return -c / 2 * (math.sqrt(1 - t * t) - 1) + b end
    t = t - 2
    return c / 2 * (math.sqrt(1 - t * t) + 1) + b
  end,
  easeInOutBack = function(t, b, c, d, s)
    s = (s or 1.70158) * 1.525
    t = t / d * 2
    if t < 1 then return c / 2 * (t * t * ((s + 1) * t - s)) + b end
    t = t - 2
    return c / 2 * (t * t * ((s + 1) * t + s) + 2) + b
  end,
  easeInBack = function(t, b, c, d, s)
    s = s or 1.70158
    t = t / d
    return c * t * t * ((s + 1) * t - s) + b
  end,
  easeOutBack = function(t, b, c, d, s)
    s = s or 1.70158
    t = t / d - 1
    return c * (t * t * ((s + 1) * t + s) + 1) + b
  end,
  easeInElastic = function(t, b, c, d, a, p)
    local s
    if t == 0 then return b end
    t = t / d
    if t == 1  then return b + c end
    p,a,s = calculatePAS(p,a,c,d)
    t = t - 1
    return -(a * math.pow(2, 10 * t) * math.sin((t * d - s) * (2 * math.pi) / p)) + b
  end,
  easeInElastic_small = function(t, b, c, d, a, p)
    local s
    if t == 0 then return b end
    t = t / d
    if t == 1  then return b + c end
    p,a,s = calculatePAS(0.6,a,c,d)
    t = t - 1
    return -(a * math.pow(2, 10 * t) * math.sin((t * d - s) * (2 * math.pi) / p)) + b
  end,
  easeOutElastic = function(t, b, c, d, a, p)
    local s
    if t == 0 then return b end
    t = t / d
    if t == 1 then return b + c end
    p,a,s = calculatePAS(p,2,c,d)
    return a * math.pow(2, -10 * t) * math.sin((t * d - s) * (2 * math.pi) / p) + c + b
  end,
  easeOutElastic_small = function(t, b, c, d, a, p)
    local s
    if t == 0 then return b end
    t = t / d
    if t == 1 then return b + c end
    p,a,s = calculatePAS(0.6,a,c,d)
    return a * math.pow(2, -10 * t) * math.sin((t * d - s) * (2 * math.pi) / p) + c + b
  end,
  easeOutBounce = function(t, b, c, d, s)
    t = t / d
    if t < 1 / 2.75 then return c * (7.5625 * t * t) + b end
    if t < 2 / 2.75 then
      t = t - (1.5 / 2.75)
      return c * (7.5625 * t * t + 0.75) + b
    elseif t < 2.5 / 2.75 then
      t = t - (2.25 / 2.75)
      return c * (7.5625 * t * t + 0.9375) + b
    end
    t = t - (2.625 / 2.75)
    return c * (7.5625 * t * t + 0.984375) + b
  end

}

return Tween
