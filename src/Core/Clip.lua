---
-- Animation clip
-- @classmod Core.Clip
local Clip = class("Clip")
local Interval = require("Core.Interval")

Clip.static.cachedImageData = {}

---
-- Cache and retrieve image and imageData
-- @treturn {{image,imageData},...}
Clip.static.getCachedImageData = function (path)
  if nil ~= Clip.static.cachedImageData[path] then
    return Clip.static.cachedImageData[path][1], Clip.static.cachedImageData[path][2]
  end

  image = love.graphics.newImage(path)
  imageData = love.image.newImageData(path)
  Clip.static.cachedImageData[path] = {image, imageData}

  return image, imageData
end

---
-- @table Core.ClipDef
-- @tparam string file Spritesheet file name in "graphics" folder
-- @int[opt=16] w Frame width
-- @int[opt=16] h Frame height
-- @tparam {x, y} offset Drawing offset
-- @tparam {Animation,...} animations Animations definitions,
-- key is an animation name.
-- There are must be at least one definition.

---
-- `
--     {
--         int frame,
--         int anotherFrame,
--         ...,
--         bool isLoop | string nextAnimation,
--         number length
--     }
-- `
--
-- Example: `{1, 2, 3, true, .1}`
-- - looped animation from frames 1, 2, 3, with .1s (100ms) berween frames
--
-- @table Animation

---
-- @tparam Core.ClipDef def
function Clip:initialize(def)
  self.image, self.imageData = Clip.static.getCachedImageData(def.file)

  self.w = def.w or 16
  self.h = def.h or 16

  self.currentFrame = 1
  self.isStopped = false
  self.interval = Interval:new(0)

  def.offset = def.offset or {}
  self.offset = {x = def.offset.x or 0, y = def.offset.y or 0}

  self:setAnimations(def.animations)
end

---
-- @tparam {Animation,...} defs
function Clip:setAnimations(defs)
  self.animations = {}

  local imgW, imgH = self.imageData:getDimensions()
  local framesPerRow = imgW / self.w

  local firstAnimationName = nil
  local quads = {}
  local animations = {}

  for name, def in pairs(defs) do
    local animation = {
      frames = {},
      length = 0, -- 0 is not animated
      isLoop = false,
      nextAnimation = nil,
    }

    for i = 1, #def do
      local frame = def[i]
      local ftype = type(frame)

      if 'number' == ftype then
        -- last number in definition is interval between frames
        if i == #def and 1 < #def then
          animation.length = frame
        else
          if nil == quads[frame] then
            -- create quad for 'frame'
            local row = math.floor((frame - 1) / framesPerRow)
            local col = (frame - 1) - (framesPerRow * row)

            quads[frame] = love.graphics.newQuad(
              col * self.w,
              row * self.h,
              self.w,
              self.h,
              imgW,
              imgH
            )
          end

          table.insert(animation.frames, quads[frame])
        end
      elseif 'boolean' == ftype then
        animation.isLoop = frame
      elseif 'string' == ftype then
        animation.nextAnimation = frame
      end
    end

    self.animations[name] = animation
    firstAnimationName = firstAnimationName or name
  end

  self:play(firstAnimationName)
end

---
-- @int[opt=0] x
-- @int[opt=0] y
-- @int[opt=1] scaleX
-- @int[opt=1] scaleY
function Clip:draw(x, y, scaleX, scaleY)
  x = x or 0
  y = y or 0
  scaleX = scaleX or 1
  scaleY = scaleY or 1

  local offsetX = self.w * math.min(scaleX / math.abs(scaleX), 0)
  local offsetY = self.h * math.min(scaleY / math.abs(scaleY), 0)

  local animation = self.animations[self.currentAnimation]

  love.graphics.draw(
    self.image,
    animation.frames[self.currentFrame],
    x - offsetX + self.offset.x,
    y - offsetY + self.offset.y,
    nil,
    scaleX,
    scaleY
  )
end

---
-- Unsafe method to draw exact animation.
--
-- @string animation
-- @see Clip:draw
function Clip:drawAnimation(animation, ...)
  self.currentAnimation = animation
  self:draw(...)
end

---
-- Change animation
-- @string name
function Clip:play(name)
  if name ~= self.currentAnimation then
      self.currentAnimation = name
      self.currentFrame = 1
      self.interval:setLength(self.animations[name].length)
      self.interval:start()
      self.isStopped = false
  end
end

---
-- Start animation
function Clip:start()
  self:stop()
  self.isStopped = false
end

---
-- Stop animation
function Clip:stop()
  self.currentFrame = 1
  self.currentTime = 0
  self.isStopped = true
end

---
-- @number dt
function Clip:update(dt)
  if self.isStopped then
    return
  end

  if self.interval:isStopped() then
    self:_nextFrame()
    self.interval:start()
  end

  self.interval:update(dt)
end

function Clip:_nextFrame()
  local animation = self.animations[self.currentAnimation]
  local nextFrame = self.currentFrame + 1

  if nextFrame > #animation.frames then
    if animation.isLoop then
      self.currentFrame = 1
    elseif animation.nextAnimation ~= nil then
      self:play(animation.nextAnimation)
    else
      self.isStopped = true
    end
  else
    self.currentFrame = nextFrame
  end
end

return Clip
