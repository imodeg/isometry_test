local Grid = require("Core.Grid")

local World = class("World")

function World:initialize(game, def)
  self.game = game

  self.grid = Grid:new(game, def.grid)
  self.entities = {}
end

function World:add(entity)
  self.grid:add(entity)
  self.entities[entity] = true
end

function World:remove(entity)
  self.grid:remove(entity)
  self.entities[entity] = nil
end

function World:move(targetEntity, nextPosition)
  local collisions = {}
  local length = 0

  local entities = self.grid:flattenEntities(self.grid:toGridPosition(nextPosition), 2);

  for _, entity in pairs(entities) do
    if entity ~= targetEntity then
      local collision = self:isCollide(targetEntity, entity, nextPosition)

      if collision ~= nil then

        if entity.type ~= "Trigger" and targetEntity.type ~= "Trigger" then
          self:pullOutEntity(targetEntity, entity, nextPosition, collision)
        end

        table.insert(collisions, collision)
        length = length + 1

        targetEntity:collide(entity)
        entity:collide(targetEntity)
      end
    end
  end

  self.grid:move(targetEntity, nextPosition)

  return collisions, length
end

function World:isCollide(targetEntity, entity, nextPosition)
  local prevTargetAABB = targetEntity:getWorldAABB(targetEntity.position)
  local targetAABB = targetEntity:getWorldAABB(nextPosition)
  local aabb = entity:getWorldAABB(entity.position)

  local targetMin, targetMax = targetAABB.min, targetAABB.max
  local prevTargetMin, prevTargetMax = prevTargetAABB.min, prevTargetAABB.max
  local min, max = aabb.min, aabb.max

  if targetMin.x <= max.x and targetMax.x >= min.x and
    targetMin.y <= max.y and targetMax.y >= min.y and
    targetMin.z <= max.z and targetMax.z >= min.z then

    if prevTargetMin.z >= max.z then
      return "z+"
    end

    if prevTargetMin.x > max.x and
      targetMin.y < max.y and
      targetMax.y > min.y
    then
      return "x+"
    end

    if prevTargetMax.x < min.x and
      targetMin.y < max.y and
      targetMax.y > min.y then
      return "x-"
    end

    if prevTargetMin.y > max.y and
      targetMin.x < max.x and
      targetMax.x > min.x then
      return "y+"
    end

    if prevTargetMax.y < min.y and
      targetMin.x < max.x and
      targetMax.x > min.x then
      return "y-"
    end
  end

  return nil
end

function World:pullOutEntity(targetEntity, entity, nextPosition, direction)
  if direction == 'x+' then
    nextPosition.x = entity.position.x + targetEntity.hitboxHalf.x + entity.hitboxHalf.x + 1
  end

  if direction == 'x-' then
    nextPosition.x = entity.position.x - targetEntity.hitboxHalf.x - entity.hitboxHalf.x - 1
  end

  if direction == 'y+' then
    nextPosition.y = entity.position.y + targetEntity.hitboxHalf.y + entity.hitboxHalf.y + 1
  end

  if direction == 'y-' then
    nextPosition.y = entity.position.y - targetEntity.hitboxHalf.y - entity.hitboxHalf.y - 1
  end

  if direction == 'z+' then
    nextPosition.z = entity.position.z + entity.hitbox.z
  end
end

function World:update(dt)
  self.grid:update(dt)
end

function World:draw()
  self.grid:draw()
end

return World
