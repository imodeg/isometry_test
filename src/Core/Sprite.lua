local Sprite = class("Sprite")

Sprite.static.cachedImageData = {}

---
-- Cache and retrieve image and imageData
-- @treturn {{image,imageData},...}
Sprite.static.getCachedImageData = function (filePath)
  if nil ~= Sprite.static.cachedImageData[filePath] then
    return Sprite.static.cachedImageData[filePath][1], Sprite.static.cachedImageData[filePath][2]
  end

  image = love.graphics.newImage(filePath)
  imageData = love.image.newImageData(filePath)
  Sprite.static.cachedImageData[filePath] = {image, imageData}

  return image, imageData
end

function Sprite:initialize(def)
  self.image, self.imageData = Sprite.static.getCachedImageData(def.image)
  self.offset = def.offset or vec2d(0, 0)

  local imgW, imgH = self.imageData:getDimensions()
  self.quad = love.graphics.newQuad(
    0,
    0,
    imgW,
    imgH,
    imgW,
    imgH
  )
end

function Sprite:draw(position, offset_x, offset_y, rotate, scale_x, scale_y, shear_x, shear_y, alpha)
  pushColor()
  love.graphics.setColor(1, 1, 1, alpha or 1)
  love.graphics.draw(
    self.image,
    self.quad,
    math.floor(position.x),
    math.floor(position.y),
    rotate or nil,
    scale_x or 1,
    scale_y or 1,
    -self.offset.x + (offset_x or 0),
    -self.offset.y + (offset_y or 0),
    shear_x or 0,
    shear_y or 0
  )
  popColor()

  -- @todo remove
  -- local imgW, imgH = self.imageData:getDimensions()
  -- love.graphics.rectangle(
  --   "line",
  --   position.x + self.offset.x,
  --   position.y + self.offset.y,
  --   imgW,
  --   imgH
  -- )
end

return Sprite
