local EventBinding = require("Core.Event.EventBinding")

local MouseControls = class("MouseControls")

function MouseControls:initialize()
  self.events = {}
end

function MouseControls:mousepressed(x, y, button, istouch, presses)
  for _, event in ipairs(self.events) do
    if (event.button == button) and (pointInsideRectangle(x, y, event.area.start_x, event.area.start_y, event.area.size_x, event.area.size_y)) then
      event.callback(x, y)
    end
  end
end

function MouseControls:mousereleased(x, y, button, istouch, presses)
  --TODO: mouse release event
end

function MouseControls:add(button, area_start_x, area_start_y, area_size_x, area_size_y, callback)
  local newEvent = EventBinding:new(self, "mouseEvent", false, callback)
  newEvent.button = button
  local area = {}
  area.start_x = area_start_x
  area.start_y = area_start_y
  area.size_x = area_size_x
  area.size_y = area_size_y

  newEvent.area = area

  table.insert(self.events, newEvent)
  return newEvent
end

--debug press areas draw
function MouseControls:draw()
  for _, event in ipairs(self.events) do
     pushColor()
     love.graphics.setColor(1, 1, 1, 0.2)
     love.graphics.rectangle( "fill", event.area.start_x, event.area.start_y, event.area.size_x, event.area.size_y )
     love.graphics.setColor(1, 1, 1, 1)
     love.graphics.rectangle( "line", event.area.start_x, event.area.start_y, event.area.size_x, event.area.size_y )
     popColor()
  end
end

function MouseControls:ramoveEvent(event)
  --TODO: remove single event
end

function MouseControls:ramoveAllEvents()
  self.events = {}
end

return MouseControls
