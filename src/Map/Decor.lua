local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")

local Decor = EntityStatic:subclass("Decor")

function Decor:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.sprite = Sprite:new(def.sprite)
  self.type = "Decor"

end

function Decor:draw()
  local ortPos = self:getOrtPosition();

  self.sprite:draw(ortPos)

  EntityStatic.draw(self)
end

return Decor
