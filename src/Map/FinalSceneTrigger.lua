local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")
local Interval = require("Core.Interval")
local Tween = require("Core.Tween")

local FinalSceneTrigger = EntityStatic:subclass("FinalSceneTrigger")

function FinalSceneTrigger:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.sprite = Sprite:new(def.sprite)
  self.type = "FinalSceneTrigger"

  self.mollyAlphaTween = Tween:new(0, 1.0, 1.0, "easeOutCubic", false, true)
  self.mollyAlphaTween.onEndSignal:add(
    function()
      self.game.dialogManager:start("mollyEnd")
    end )

  self.mollyScaleTween = Tween:new(2.0, 1.0, 1.0, "easeOutCubic", false, true)

  self.mollySpawnInterval_1 = Interval:new(2.0)
  self.mollySpawnInterval_1.onEndSignal:add(
    function()
      self.particlesEffect:start()
      self.mollySpawnInterval_2:start()
    end )
  self.mollySpawnInterval_1:start()

  self.mollySpawnInterval_2 = Interval:new(4.0)
  self.mollySpawnInterval_2.onEndSignal:add(
    function()
      self.mollyAlphaTween:start()
      self.mollyScaleTween:start()
      self.particlesEffect:stop()

      local decores = self.game.world.grid:findByType("MagicDecor")

      for _, entity in ipairs(decores) do
        entity:startDrop()
      end
    end )

  self.particleImage, self.particleImageData = Sprite.static.getCachedImageData("images/effects/star.png")
  self.particlesEffect = love.graphics.newParticleSystem(self.particleImage, 72)
  self.particlesEffect:setColors(1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0.5, 1, 1, 1, 0.0)
  self.particlesEffect:setDirection(-1.5707963705063)
  self.particlesEffect:setEmissionArea("borderellipse", 250.0, 250.0, 0, false)
  self.particlesEffect:setEmissionRate(63.447830200195)
  self.particlesEffect:setEmitterLifetime(-1)
  self.particlesEffect:setInsertMode("top")
  self.particlesEffect:setLinearAcceleration(0, 0, 0, 0)
  self.particlesEffect:setLinearDamping(0, 0)
  self.particlesEffect:setOffset(50, 50)
  self.particlesEffect:setParticleLifetime(1.4, 1.4)
  self.particlesEffect:setRadialAcceleration(-7.8609485626221, -252.42379760742)
  self.particlesEffect:setRelativeRotation(false)
  self.particlesEffect:setRotation(0, 0)
  self.particlesEffect:setSizes(0.062874101102352)
  self.particlesEffect:setSizeVariation(0)
  self.particlesEffect:setSpeed(0.17468774318695, 0.17468774318695)
  self.particlesEffect:setSpin(0, 0)
  self.particlesEffect:setSpinVariation(0)
  self.particlesEffect:setSpread(0)
  self.particlesEffect:setTangentialAcceleration(0, 0.097048744559288)
  self.particlesEffect:stop()
end

function FinalSceneTrigger:update(dt)
  self.mollyAlphaTween:update(dt)
  self.mollyScaleTween:update(dt)
  self.mollySpawnInterval_1:update(dt)
  self.mollySpawnInterval_2:update(dt)

  self.particlesEffect:update(dt)
end

function FinalSceneTrigger:draw()
  local ortPos = self:getOrtPosition();

  love.graphics.draw(self.particlesEffect, ortPos.x, ortPos.y-40)
  self.sprite:draw(vec2d(ortPos.x-5,ortPos.y+15), nil, nil, nil, self.mollyScaleTween.current, self.mollyScaleTween.current, nil, nil, self.mollyAlphaTween.current)

  EntityStatic.draw(self)
end

return FinalSceneTrigger
