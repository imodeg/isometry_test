local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")

local Button = EntityStatic:subclass("Button")

function Button:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.type = "Button"

  local theme = def.props.theme

  self.spriteOn = Sprite:new({
    image = "images/objects/Button_" .. theme .. "_On.png",
    offset = def.sprite.offset
  })

  self.spriteOff = Sprite:new({
    image = "images/objects/Button_" .. theme .. "_Off.png",
    offset = def.sprite.offset
  })
end

function Button:collide(entity)
  EntityStatic.collide(self, entity)
  self.isOn = true
end

function Button:update(dt)
  self.isOn = false
end

function Button:draw()
  local ortPos = self:getOrtPosition();

  if self.isOn then
    self.spriteOn:draw(ortPos)
  else
    self.spriteOff:draw(ortPos)
  end

  EntityStatic.draw(self)
end

return Button
