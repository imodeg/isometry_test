local Button = require("Map.Button")
local Decor = require("Map.Decor")
local MagicDecor = require("Map.MagicDecor")
local FinalSceneTrigger = require("Map.FinalSceneTrigger")
local Grid = require("Core.Grid")
local Tile = require("Map.Tile")
local Trigger = require("Map.Trigger")
local Image = require("Map.Image")
local Character = require("Map.Character")
local Item = require("Map.Item")

local TYPE_MAP = {
  Block = Tile,
  Decor = Decor,
  MagicDecor = MagicDecor,
  Button = Button,
  Character = Character,
  Item = Item,
  FinalSceneTrigger = FinalSceneTrigger
}

local Map = class("Map")

function Map:initialize(game, tiled)
  self.game = game
  self.grid = game.world.grid

  self.props = tiled.data.properties
  local tilesData = tiled:getTilesData()
  local defs = tiled:getTileDefs()
  for _, tileData in ipairs(tilesData) do
    local def = defs[tileData.tileDefId]
    local TileClass = TYPE_MAP[def.type]

    local sprite = nil

    if not def.props or not def.props.ghost then
      sprite = def.sprite
    end

    if TileClass ~= nil then
      TileClass:new(self.game, {
        position = self.grid:fromGridPosition(tileData.gridPosition),
        hitbox = def.hitbox,
        sprite = sprite,
        props = def.props
      })
    end
  end

  local objects = tiled:getObjectsData()
  for _, objData in ipairs(objects) do
    Trigger:new(self.game, objData)
  end

  local images = tiled:getImagesData()
  for _, imgData in ipairs(images) do
    Image:new(self.game, imgData)
  end
end

return Map
