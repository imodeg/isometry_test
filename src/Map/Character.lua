local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")
local Cloud = require("Dialog.Cloud")

local Character = EntityStatic:subclass("Character")

function Character:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.sprite = Sprite:new(def.sprite)
  self.type = "Character"
  self.name = def.props.name or "unknown"
  --print(self.name)
  self.animationTween_rotate = Tween:new(def.props.tween_rotate_from or 0, def.props.tween_rotate_to or 0, def.props.tween_rotate_time or 1, def.props.tween_rotate_ease or "easeInOutCubic", true)
  self.animationTween_scale_x = Tween:new(def.props.tween_scale_x_from or 1, def.props.tween_scale_x_to or 1, def.props.tween_scale_x_time or 1, def.props.tween_scale_x_ease or "easeInOutCubic", true)
  self.animationTween_scale_y = Tween:new(def.props.tween_scale_y_from or 1, def.props.tween_scale_y_to or 1, def.props.tween_scale_y_time or 1, def.props.tween_scale_y_ease or "easeInOutCubic", true)
  self.animationTween_shear_x = Tween:new(def.props.tween_shear_x_from or 0, def.props.tween_shear_x_to or 0, def.props.tween_shear_x_time or 1, def.props.tween_shear_x_ease or "easeInOutCubic", true)
  self.animationTween_shear_y = Tween:new(def.props.tween_shear_y_from or 0, def.props.tween_shear_y_to or 0, def.props.tween_shear_y_time or 1, def.props.tween_shear_y_ease or "easeInOutCubic", true)

  self.cloud = Cloud:new(game)
  self.cloudShiftX = def.props.cloudShiftX or 0
  self.cloudShiftY = def.props.cloudShiftY or 0
end

function Character:update(dt)
  self.animationTween_rotate:update(dt)
  self.animationTween_scale_x:update(dt)
  self.animationTween_scale_y:update(dt)
  self.animationTween_shear_x:update(dt)
  self.animationTween_shear_y:update(dt)
  --print(self.animationTween.current)
  self.cloud:update(dt)
end

function Character:draw()
  local ortPos = self:getOrtPosition();

  self.sprite:draw(ortPos, 0, 0, self.animationTween_rotate.current,
                                    self.animationTween_scale_x.current,
                                    self.animationTween_scale_y.current,
                                    self.animationTween_shear_x.current,
                                    self.animationTween_shear_y.current)

  --75, 190

  EntityStatic.draw(self)

  self.cloud:draw(ortPos.x+30+self.cloudShiftX, ortPos.y-140+self.cloudShiftY)
end

return Character
