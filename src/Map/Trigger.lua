local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")

local Trigger = EntityStatic:subclass("Trigger")

function Trigger:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.debugColor = {0, 1, 0}
  self.type = "Trigger"
  self.name = def.name
  self.isCollide = false

  --if def.isCharacter then print("y") end
  self.isCharacter = def.defs.isCharacter or false

  if self.isCharacter then
    self.characterEntity = self.game.world.grid:findByTypeAndName("Character", self.name)
  end

  --self.spriteCloud = Sprite:new({
  --  image = "images/gui/cloud.png",
  --  offset = vec2d(4,36)
  --})

  self.isCollidedNow = false
end

function Trigger:collide(entity)
  EntityStatic.collide(self, entity)
  self.debugColor = {1, 0, 0}
  self.isCollide = true
end

function Trigger:update(dt)
  if self.isCollide then
    if self.game.controls:isKeyboardDown("space", 0) and not self.game.dialogManager.isStarted then
      self.game.dialogManager:start(self.name)
    end

    if self.isCharacter then
      self.characterEntity.cloud:open()
    else
      self.game.player.cloud:open()
    end
    self.isCollidedNow = true
  else
    if self.isCollidedNow then
      self.isCollidedNow = false
      --print("whaat")
      if self.isCharacter then
        self.characterEntity.cloud:close()
      else
        self.game.player.cloud:close()
      end
      --self.game.player.cloud:close()
    end
  end

  self.debugColor = {0, 1, 0}
  self.isCollide = false
end

function Trigger:draw()
  EntityStatic.draw(self)

  --if self.game.player and self.isCollide then
  --  local playerPos = iso2ort(self.game.player.position)
  --  self.spriteCloud:draw(playerPos, 20, 90)
  --end
end

return Trigger
