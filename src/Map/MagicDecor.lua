local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")

local MagicDecor = EntityStatic:subclass("MagicDecor")

function MagicDecor:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.sprite = Sprite:new(def.sprite)
  self.type = "MagicDecor"

  --self.fixOrtPos = self:getOrtPosition();

  self.fixOrtPos = self:getOrtPosition();
  self.ortPos = self:getOrtPosition();
  self.baseYpos = self.ortPos.y - 50
  self.time = love.math.random(0,40)/10

  self.spriteShadow = Sprite:new({image = "images/ShadowCircle.png"})
  self.shadowScale = 1.0


  self.particleImage, self.particleImageData = Sprite.static.getCachedImageData("images/effects/star.png")
  self.particlesEffect = love.graphics.newParticleSystem(self.particleImage, 32)
  self.particlesEffect:setColors(1, 1, 1, 0, 1, 1, 1, 0.3560605943203, 1, 1, 1, 0.15151515603065, 1, 1, 1, 0)
  self.particlesEffect:setDirection(-1.5707963705063)
  self.particlesEffect:setEmissionArea("ellipse", 29.67657661438, 29.67657661438, 0, false)
  self.particlesEffect:setEmissionRate(20)
  self.particlesEffect:setEmitterLifetime(-1)
  self.particlesEffect:setInsertMode("top")
  self.particlesEffect:setLinearAcceleration(0.43671935796738, 30.32773399353, 3.930474281311, 318.36840820313)
  self.particlesEffect:setLinearDamping(0.0017468774458393, 0.00019409749074839)
  self.particlesEffect:setOffset(50, 50)
  self.particlesEffect:setParticleLifetime(0.24844479560852, 1.0697101354599)
  self.particlesEffect:setRadialAcceleration(0, 0)
  self.particlesEffect:setRelativeRotation(false)
  self.particlesEffect:setRotation(0, 0)
  self.particlesEffect:setSizes(0.0025149639695883, 0.07886927574873)
  self.particlesEffect:setSizeVariation(0.97507786750793)
  self.particlesEffect:setSpeed(0.019409749656916, 1.5721896886826)
  self.particlesEffect:setSpin(3.3605935573578, 0.00048782021622173)
  self.particlesEffect:setSpinVariation(0)
  self.particlesEffect:setSpread(6.2831854820251)
  self.particlesEffect:setTangentialAcceleration(0.097048744559288, 0.097048744559288)
  --self.particlesEffect:stop()

  self.dropped = false


  self.dropTweenY = Tween:new(self.fixOrtPos.y - 10, self.fixOrtPos.y, 1.0, "easeOutBounce", false, true)
  self.dropTweenShadow = Tween:new(0.5, 1.0, 1.0, "easeOutBounce", false, true)
end

function MagicDecor:startDrop()
  self.dropped = true

  self.dropTweenY.from = self.baseYpos + math.cos(self.time*2)*30
  self.dropTweenY.current = self.baseYpos + math.cos(self.time*2)*30
  self.dropTweenY.to = self.fixOrtPos.y


  self.dropTweenShadow.from = 0.2 + ((math.cos(self.time*2)/2)+1)*0.7
  self.dropTweenShadow.current = 0.2 + ((math.cos(self.time*2)/2)+1)*0.7
  self.dropTweenShadow.to = 1.5

  self.dropTweenY:start()
  self.dropTweenShadow:start()

end

function MagicDecor:update(dt)
  self.time = self.time + dt
  self.ortPos.y = self.baseYpos + math.cos(self.time*2)*30
  self.shadowScale = 0.2 + ((math.cos(self.time*2)/2)+1)*0.7

  self.particlesEffect:moveTo( 0, math.cos(self.time*2)*30 )
  self.particlesEffect:update(dt)

  self.dropTweenY:update(dt)
  self.dropTweenShadow:update(dt)
end

function MagicDecor:draw()
  --local ortPos = self:getOrtPosition();

  if self.dropped then

    --self.spriteShadow:draw(self.fixOrtPos, 30, 15, 0, self.shadowScale, self.shadowScale, nil, nil, 0.5)
    self.spriteShadow:draw(self.fixOrtPos, 30, 10, 0, self.dropTweenShadow.current, self.dropTweenShadow.current, nil, nil, 0.5)
    self.sprite:draw(vec2d(self.fixOrtPos.x, self.dropTweenY.current))

  else

    self.spriteShadow:draw(self.fixOrtPos, 30, 10, 0, self.shadowScale, self.shadowScale, nil, nil, 0.5)
    love.graphics.draw(self.particlesEffect, self.fixOrtPos.x, self.fixOrtPos.y-50)
    self.sprite:draw(self.ortPos)

  end


  EntityStatic.draw(self)
end

return MagicDecor
