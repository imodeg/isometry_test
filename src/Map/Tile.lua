local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")

local Tile = EntityStatic:subclass("Tile")

function Tile:initialize(game, def)
  local hitbox = vec3d(50, 50, 25)

  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = hitbox
  })

  if def.sprite then
    self.sprite = Sprite:new(def.sprite)
  end

  self.type = "Block"

  self.shadows = nil
end

function Tile:draw()
  local ortPos = self:getOrtPosition();

  if self.sprite then
    self.sprite:draw(ortPos)
  end

  EntityStatic.draw(self)
end

return Tile
