local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")

local Image = EntityStatic:subclass("Image")

function Image:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.debugColor = {0, 0, 1}

  self.sprite = Sprite:new(def.sprite)

  self.type = "Trigger"
end

function Image:collide(entity)
  EntityStatic.collide(self, entity)
  self.debugColor = {1, 0, 0}
  self.isCollide = true
end

function Image:update()
  self.debugColor = {0, 1, 0}
  self.isCollide = false
end

function Image:draw()
  local ortPos = self:getOrtPosition();

  self.sprite:draw(ortPos)

  EntityStatic.draw(self)
end

return Image
