local EntityStatic = require("Entities.EntityStatic")
local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")

local Item = EntityStatic:subclass("Item")

function Item:initialize(game, def)
  EntityStatic.initialize(self, game, {
    position = def.position,
    hitbox = def.hitbox
  })

  self.sprite = Sprite:new(def.sprite)
  self.type = "Item"
  self.name = def.props.name or "unknown"
end

function Item:update(dt)
  if self.game.store.data.inventory[self.name] then
    local gridPosition = self.game.world.grid:toGridPosition(self.position)
    self.game.world.grid:removeFromCell(self, gridPosition)
  end
  --print(self.animationTween.current)
end

function Item:draw()
  local ortPos = self:getOrtPosition();
  self.sprite:draw(ortPos, 0, 0)
  --75, 190
  EntityStatic.draw(self)
end

return Item
