local Store = class("GameState")

function Store:initialize()
  self:reset()
end

function Store:reset()
  self.data = {
    inventory = {
      pumpkin = false,
      pie = false,
      lantern = false,
      lambswool = false,
      key = false,
      clock = false,
      costume = false,
      candle = false,
      candies = false
    },

    chars = {
      archibald = {
        name = {
          ru = "Арчибальд",
          en = "Archibald"
        },
        visits = 0,
        pumpkinsGiven = 0,
        state = function (data, char)
          if char.pumpkinsGiven > 1 then
            return "goaway"
          end

          if data.chars.boris.visits == 0 then
            return "greet"
          end

          if data.inventory.pumpkin then
            return "alreadyHas"
          end

          if char.pumpkinsGiven == 0 then
            return "give"
          end

          if data.inventory.costume then
            return "secondPumpkin"
          end

          return "needCostume"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Этой осенью у меня выросли просто огромные тыквы!",
                  "Хочешь посмотреть на мои тыквы?"
                },
                {
                  "Огромные тыквы!"
                }
              },
              en = {
                {
                  "Sucha a huge pumpkins have grown this autumn!",
                  "Would you like to see my pumpkins?"

                },
                {
                  "Huge pumpkins!"
                }
              }
            }
          },
          give = {
            messages = {
              ru = {
                {
                  "Ты пришла посмотреть на мои тыквы?",
                  "Я могу дать тебе одну!"
                }
              },
              en = {
                {
                  "You came to see my pumpkins?",
                  "I could give you one!"
                }
              }
            },
            leave = function (data, char)
              data.inventory.pumpkin = true
              char.pumpkinsGiven = char.pumpkinsGiven + 1
            end
          },
          alreadyHas = {
            messages = {
              ru = {
                {
                  "Ха-ха, мои тыквы больше чем твоя голова!",
                  "Тебе не унести две за раз! Ты слишком худая."
                }
              },
              en = {
                {
                  "Ha-ha, my pumpkins are bigger than your head!",
                  "I don't think you can manage to carry two at a time!"
                }
              }
            }
          },
          needCostume = {
            messages = {
              ru = {
                {
                  "Я всегда хотел себе более праздничный костюм!"
                },
                {
                  "У тебя, случайно, нет запасного костюма?",
                  "Я могу дать тебе за него еще одну тыкву!"
                }
              },
              en = {
                {
                  "I've always wanted a more festive costume for myself!"
                },
                {
                  "Do you by any chance have a spare costume?",
                  "I could give you another pumpkin for it!"
                }
              }
            }
          },
          secondPumpkin = {
            messages = {
              ru = {
                {
                  "Я всегда хотел себе более праздничный костюм!"
                },
                {
                  "О, какой прекрасный костюм!",
                  "Жаль, он мне не по размеру."
                },
                {
                  "Но ты очень добра ко мне! Держи!"
                }
              },
              en = {
                {
                   "I've always wanted a more festive costume for myself!"
                },
                {
                  "Oh, such a beautiful costume!",
                  "It's a shame it's not my size."
                },
                {
                  "But you are very kind! Here!"
                }
              }
            },
            leave = function (data, char)
              data.inventory.pumpkin = true
              char.pumpkinsGiven = char.pumpkinsGiven + 1
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Я отдал тебе самые лучшие тыквы, чего ты еще хочешь?"
                }
              },
              en = {
                {
                  "I gave you my best pumpkins, what else do you want?"
                }
              }
            }
          }
        }
      },
      boris = {
        name = {
          ru = "Борис",
          en = "Boris"
        },
        visits = 0,
        piesGiven = 0,
        state = function (data, char)
          if char.piesGiven > 0 then
            return "goaway"
          end

          if data.inventory.pumpkin then
            return "cook"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Привет, ты хочешь что-то заказать?",
                  "Я могу испечь для тебя все что угодно!"
                },
                {
                  "...Если ты принесешь мне продукты."
                }
              },
              en = {
                {
                  "Hi, you want to order something?",
                  "I can bake for you anything you want!"
                },
                {
                "...If you give me ingredients."
                }
              }
            }
          },
          cook = {
            messages = {
              ru = {
                {
                  "Что у тебя там? Тыква?"
                },
                {
                  "Из этого выйдет отличный тыквенный пирог!"
                },
                {
                  "Давай ее сюда!"
                }
              },
              en = {
                {
                  "What do you have? A pumpkin?"
                },
                {
                  "That will be a perfect pumpkin pie!"
                },
                {
                  "Ok, give it to me!"
                }
              }
            },
            leave = function (data, char)
              data.inventory.pumpkin = false
              data.inventory.pie = true
              char.piesGiven = 1
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Ты хочешь еще один пирог? А куда ты дела первый?"
                },
                {
                  "Извини, я делаю только один пирог в день.",
                  "Приходи завтра."
                }
              },
              en = {
                {
                  "Do you want another pie? What did you do with the first?"
                },
                {
                  "Sorry, I make only one pie a day.",
                  "Come tomorrow."
                }
              }
            }
          }
        }
      },
      begimot = {
        name = {
          ru = "отец Бегемот",
          en = "Father Behemoth"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.key then
            return "goaway"
          end

          if data.inventory.pie then
            return "giveKey"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Чего тебе?!",
                  "Храм сегодня закрыт!"
                },
                {
                  "А-ну иди отсюда!"
                }
              },
              en = {
                {
                  "What do you want?!",
                  "The church is closed today!"
                },
                {
                  "Get out of here!"
                }
              }
            }
          },
          giveKey = {
            messages = {
              ru = {
                {
                  "Ты принесла пирог?",
                  "Не знаю, чего ты добиваешься, но я люблю пироги."
                },
                {
                  "Хорошо, держи ключ, только никому не рассказывай."
                }
              },
              en = {
                {
                 "You bring the pie? I don't know what you're up to,",
                 "...but I like pies."
                },
                {
                  "Fine, here is the key, but don't tell anyone."
                }
              }
            },
            leave = function (data)
              data.inventory.key = true
              data.inventory.pie = false
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "У тебя есть еще пирог?"
                },
                {
                  "Нет?! Ну тогда иди куда шла!!"
                }
              },
              en = {
                {
                  "Do you have another pie?"
                },
                {
                  "No?? Then go away!!"
                }
              }
            }
          }
        }
      },
      thomas = {
        name = {
          ru = "Томас",
          en = "Thomas"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.lambswool then
            return "goaway"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Кому Ламбсвул! Горячий Ламбсвул!", -- lambswool
                  "Подходи ближе, я угощу тебя!"
                },
                {
                  "Что значит, обычный пунш?!",
                  "Я варил его с самого утра!"
                }
              },
              en = {
                {
                 "Who wants some Lambswool! Hot Lambswool!",
                  "Come closer, I'll treat you!"
                },
                {
                  "What do you mean 'regular punch'?!",
                  "I have been making it since morning!"
                }
              }
            },
            leave = function (data)
              data.inventory.lambswool = true
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Кажется, ты обозвала мой Ламбсвул обычным пуншем?"
                }
              },
              en = {
                {
                  "It's you who called my Lambswool 'a regular punch'?"
                }
              }
            }
          }
        }
      },
      molly = {
        name = {
          ru = "Молли",
          en = "Molly"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.candies then
            return "goaway"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Обожаю Хэллоуин!",
                  "Это лучший праздник на свете!"
                },
                {
                  "Хочу, чтобы Хэллоуин был каждый день!",
                  "Хочешь, конфету? У меня их много!"
                }
              },
              en = {
                {
                  "I just love Halloween!",
                  "It is the best holiday ever!"
                },
                {
                  "I wish it could be Halloween every day!",
                  "Want some candy? I have a lot!"
                }
              }
            },
            leave = function (data)
              data.inventory.candies = true
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Урааа! Хэллоуин!!!"
                },
                {
                  "Обожаю Хэллоуин!",
                  "Это лучший праздник на свете!"
                }
              },
              en = {
                {
                  "Yaaaay! Halloween!!!"
                },
                {
                  "I love Halloween!",
                  "It is the best holiday ever!"
                }
              }
            }
          }
        }
      },
      sara = { -- @todo проверить наличие loops в game (game.loops)
        name = {
          ru = "старушка Сара",
          en = "old Sara"
        },
        visits = 0,
        state = function (data, char, game)
          if game.loops == 1 then
            return "firstDay"
          end

          return "greet"
        end,
        states = {
          firstDay = {
            messages = {
              ru = {
                {
                  "..."
                }
              },
              en = {
                {
                  "..."
                }
              }
            }
          },
          greet = {
            messages = {
              ru = {
                {
                  "Здравствуй, девочка. Я ждала тебя."
                },
                {
                  "Ты ведь уже заметила, что происходит нечто странное?",
                  "Кто-то наложил заклятие на это место!"
                },
                {
                  "В наших местах часто рождаются ведьмы.",
                  "Вот, например, я, или ты"
                },
                {
                  "Но раз это сделали не ты и не я, то есть кто-то еще.",
                  "Должно быть, это очень сильный маг!"
                },
                {
                  "Но любое проклятие можно разрушить!",
                },
                {
                  "Есть два варианта:"
                },
                {
                  "найти того, кто наложил проклятие",
                  "и заставить его отменить его"
                },
                {
                  "или совершить магический ритуал и вернуть все на круги своя"
                },
                {
                  "Ритуал описан в Большой Энциклопедии Магии.",
                  "А где она - я уж и не помню..."
                }
              },
              en = {
                {
                  "Hello, girl. I've been waiting for you."
                },
                {
                  "You did notice already, that something strange is happening?",
                  "Someone put a curse on this place!"
                },
                {
                  "Witches are often born in our area.",
                  "Just like me and you"
                },
                {
                  "But since it was not you or me who did it,",
                  "then there is someone else.",
                },
                {
                  "It must be a really powerful magician!"
                },
                {
                  "But every curse can be broken!"
                },
                {
                  "There are two options:"
                },
                {
                  "find the one who put a curse",
                  "and make them reverse it"
                },
                {
                  "or perform a magic ritual",
                  "and put the world back the way it was"
                },
                {
                  "Ritual is described in Big Encyclopedia of Magic.",
                  "But I can't remember where it is..."
                }
              }
            }
          }
        }
      },
      stella = {
        name = {
          ru = "Стелла",
          en = "Stella"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.costume then
            return "goaway"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Фу...",
                  "Ради всех святых, что на тебе надето?!"
                },
                {
                  "Возьми это и не позорься."
                },
                {
                  "Можешь не благодарить."
                }
              },
              en = {
                {
                  "Ew...",
                  "For the love of God, what are you wearing?!"
                },
                {
                  "Here, thake this and don't embarrass yourself."
                },
                {
                  "You're welcome."
                }
              }
            },
            leave = function (data)
              data.inventory.costume = true
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Ты все еще в этом платье?",
                  "Тогда отойти от меня подальше."
                }
              },
              en = {
                {
                  "Are you still in this dress?",
                  "Stay away from me then."
                }
              }
            }
          }
        }
      },
      barny = {
        name = {
          ru = "Барни",
          en = "Barney"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Привет! Отлично выглядишь!",
                  "Мы вечером пойдем выпрашивать конфеты, пойдешь с нами?"
                },
                {
                  "Бывай."
                }
              },
              en = {
                {
                  "Hi! You look great!",
                  "At night we'll go for trick-or-treating."
                },
                {
                  "Will you come with us?"
                },
                {
                  "Bye bye."
                }
              }
            }
          }
        }
      },
      arice = {
        name = {
          ru = "Эйрис",
          en = "Arice"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.lantern then
            return "goaway"
          end

          if data.inventory.pumpkin and data.inventory.candle then
            return "giveLantern"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Ты успела вырезать фонарь из тыквы?",
                  "Джек не простит тебя!"
                },
                {
                  "Я умею вырезать фонари.”",
                  "Но для этого нужны тыква и свечи."
                }
              },
              en = {
                {

                  "Have you already carved a jack-o'-lantern?",
                  "Jack won't forgive you!"
                },
                {
                  "I can carve lanterns.",
                  "But I need a pumpkin and a candle."
                }
              }
            }
          },
          giveLantern = {
            messages = {
              ru = {
                {
                  "Ага! Значит, ты не умеешь вырезать фонари?"
                },
                {
                  "Минуточку…"
                },
                {
                  "Держи, я спасла твой праздник!"
                }
              },
              en = {
                {
                  "Ah! sou you don't know how to make jack-o'-lanterns?"
                },
                {
                  "Just a minute"
                },
                {
                  "Here you go, I saved your holiday!"
                }
              }
            },
            leave = function (data)
              data.inventory.lantern = true
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Вот бы у меня была такая голова!"
                }
              },
              en = {
                {
                  "I wish I have a head like this!"
                }
              }
            }
          }
        }
      },
      isabella = {
        name = {
          ru = "Изабелла",
          en = "Isabella"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Ты уже разговаривала с отцом Бегемотом?",
                },
                {
                  "Он сегодня ведет себя особенно скверно.",
                  "Опять встал не с той ноги! Ха-ха!"
                }
              },
              en = {
                {
                  "Have you already talked to Father Behemoth?",
                },
                {
                  "He behaves particularly unpleasant.",
                  "He must have got out of the wrong side of bed as usual! Hah!"
                }
              }
            }
          }
        }
      },
      robby = {
        name = {
          ru = "Робби",
          en = "Robby"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Ох уж этот отец Бегемот! Он терпеть не может праздники!",
                  "Ругался так громко, что всех местных призраков распугал!"
                },
                {
                  "Он рад только Тыквенным пирогам!",
                  "Если бы он сам умел их печь, вообще не выходил бы на улицу."
                },
                {
                  "А тут такая красота..."
                }
              },
              en = {
                {
                  "Oh, this Father Behemoth! He hates the holidays!",
                  "He swore so loudly that he scared away all the local ghosts!"
                },
                {
                  "He's only happy about Pumpkin Pies!"
                },
                {
                  "If he knew how to bake them himself,",
                  "he would never go outside."
                },
                {
                  "But here around so beautiful..."
                }
              }
            }
          }
        }
      },
      sammy = {
        name = {
          ru = "Сэмми",
          en = "Sammy"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Сладость или гадость!!!"
                }
              },
              en = {
                {
                  "Trick-or-treat!!!"
                }
              }
            }
          }
        }
      },
      pam = {
        name = {
          ru = "Пэм",
          en = "Pam"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Кошелек или жизнь?"
                }
              },
              en = {
                {
                  "Your money or your life?"
                }
              }
            }
          }
        }
      },
      ostin = {
        name = {
          ru = "Остин",
          en = "Ostin"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Эх..."
                },
                {
                  "Я недостаточно пугающий"
                }
              },
              en = {
                {
                  "Eh..."
                },
                {
                  "I'm not scary enough"
                }
              }
            }
          }
        }
      },
      meredit = {
        name = {
          ru = "Мередит",
          en = "Meredith"
        },
        visits = 0,
        candlesGiven = 0,
        state = function (data, char)
          if char.candlesGiven == 0 then
            return "greet"
          end

          return "goaway"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Жаль, что храм сегодня закрыт."
                },
                {
                  "Хотя, говорят, что в Хэллоуин туда лучше не соваться.",
                },
                {
                  "Все равно, возьмите свечу,",
                  "может быть вам удастся сегодня поставить ее во имя святых!"
                }
              },
              en = {
                {
                  "It is a pity that the temple is closed today."
                },
                {
                  "Although, everybody say that on Halloween",
                  "it is better not to meddle there."
                },
                {
                  "Anyway, take a candle,"
                },
                {
                  "Maybe you will be able to put it",
                  "in the name of the saints today!"
                }
              }
            },
            leave = function (data, char)
              char.candlesGiven = 1
              data.inventory.candle = true
            end
          },
          goaway = {
            messages = {
              ru = {
                {
                  "Сегодня я могла бы поговорить с духами!"
                },
                {
                  "Но храм сегодня не работает..."
                }
              },
              en = {
                {
                  "I could talk to the spirits today!"
                },
                {
                  "But the temple is not working today..."
                }
              }
            }
          }
        }
      },
      stinky = {
        name = {
          ru = "Стинки",
          en = "Stinky"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "А правда, что привидения существуют?",
                },
                {
                  "Я вчера видел одного!",
                  "Он съел все мои конфеты!"
                },
                {
                  "Ну не я же их съел!"
                }
              },
              en = {
                {
                  "Is it true that ghosts exist?",
                },
                {
                  "I saw one yesterday!",
                  "He ate all my candies!"
                },
                {
                  "Well, I didn't eat them!"
                }
              }
            }
          }
        }
      },
      jack = {
        name = {
          ru = "Джек",
          en = "Jack"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Я во что-то влип!",
                },
                {
                  "Что это? Жвачка?!"
                }
              },
              en = {
                {
                  "I'm stuck in something!",
                },
                {
                  "What is it? Gum?!"
                }
              }
            }
          }
        }
      },
      doris = {
        name = {
          ru = "Дорис",
          en = "Doris"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Мой костюм сшила мама!",
                  "Но самый красивый костюм все равно у Стеллы..."
                },
                {
                  "Она всегда красивая...",
                  "Даже когда она - лошадь..."
                }
              },
              en = {
                {
                  "Mom made my costume!",
                  "But Stella still has the most beautiful costume..."
                },
                {
                  "She is always beautiful...",
                  "Even when she's a horse..."
                }
              }
            }
          }
        }
      },
      patrick = { -- @todo говорит зайти в супермаркет - это не запутает?
        name = {
          ru = "Патрик",
          en = "Patrick"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Хэй, детка!"
                },
                {
                  "А зайди-ка в супермаркет, купи мне кое-что.."
                },
                {
                  "Нет, не конфеты..."
                },
                {
                  "Нет, и не это..."
                },
                {
                  "Так... Иди отсюда."
                }
              },
              en = {
                {
                  "Hay, Baby!"
                },
                {
                  "Go to the supermarket and buy me something..."
                },
                {
                  "No, not candies..."
                },
                {
                  "No, and not that..."
                },
                {
                  "Ehm... Get out of here."
                }
              }
            }
          }
        }
      },
      strangekid = {
        name = {
          ru = "Странный ребенок",
          en = "Strange Kid"
        },
        visits = 0,
        state = function (data, char)
          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Я занят..."
                }
              },
              en = {
                {
                  "I'm busy..."
                }
              }
            }
          }
        }
      },
      book = {
        name = {
          ru = "Большая Энциклопедия Магии",
          en = "The Grand Magic Encyclopedia"
        },
        visits = 0,
        state = function (data, char)
          if (data.inventory.candies and
            data.inventory.lantern and
            data.inventory.costume and
            data.inventory.clock and
            data.inventory.lambswool) then
            return "theEnd"
          end

          return "greet"
        end,
        states = {
          greet = {
            messages = {
              ru = {
                {
                  "Магический Ритуал Времени."
                },
                {
                  "Используется, чтобы наладить отношения со Временем.",
                  "Для ритуала понадобятся 5 предметов, знаменующих сей день:"
                },
                {
                  "Это ты съешь вовремя."
                },
                {
                  "Это ты выпьешь вовремя."
                },
                {
                  "Этим ты скроешь себя от Времени."
                },
                {
                  "Этим ты осветишь себе путь во Времени."
                },
                {
                  "А это и есть Само Время."
                },
                {
                  "Ритуал может сработать только в Храме."
                }
              },
              en = {
                {
                  "Magic Ritual of Time."
                },
                {
                  "Used to mend relations in Time.",
                  "Ritual require five objects that signifying this day:"
                },
                {
                  "It you will eat in Time."
                },
                {
                  "It you will drink in Time."
                },
                {
                  "With it you will hide yourself from Time."
                },
                {
                  "With it you will shine your path in Time."
                },
                {
                  "It the time itself."
                },
                {
                  "The ritual can only work in the Temple."
                }
              }
            }
          },
          theEnd = {
            messages = {
              ru = {
                {
                  "Ну, наконец, все идет как следует!",
                  "Кто-то сыграл со временем плохую шутку."
                }
              },
              en = {
                {
                  "Well, finally, everything goes right!",
                  "Someone played a bad joke with time."
                }
              }
            },
            leave = function (data, char, game)
              game:gotoMap("churchEndScene")
              --game.dialogManager:next("mollyEnd")
            end
          }
        }
      },
      mollyEnd = {
        name = {
          ru = "Молли",
          en = "Molly"
        },
        visits = 0,
        state = function (data, char)
          return "theEnd"
        end,
        states = {
          theEnd = {
            messages = {
              ru = {
                {
                  "Я только хотела, чтобы праздник был подольше...",
                  "Я не хотела ничего плохого!"
                },
                {
                  "Я просто очень люблю Хэллоуин!",
                  "Но я смогу подождать еще год!"
                },
                {
                  "В конце концов...",
                },
                {
                  "Есть еще другие праздники!..."
                }
              },
              en = {
                {
                  "I only wanted a longer Helloween...",
                  "I did't expect something bad!"
                },
                {
                  "I love Helloween so much!",
                  "But I can wait a year!"
                },
                {
                  "All in all...",
                },
                {
                  "There are another holydays!..."
                }
              }
            },
            leave = function (data, char, game)
              game:gotoState("Endgame")
            end
          }
        }
      },
      day = {
        name = {
          ru = "",
          en = ""
        },
        visits = 0,
        state = function (data, char, game)
          if game.loops == 1 then
            return "loop1"
          end

          if game.loops == 2 then
            return "loop2"
          end

          return "loopN"
        end,
        states = {
          loop1 = {
            messages = {
              ru = {
                {
                  "Сегодня Хэллоуин!",
                  "Самый мистический праздник!"
                },
                {
                  "Нужно обязательно прогуляться по городу!"
                }
              },
              en = {
                {
                  "It's weird... Seems like new day didn't come.",
                  "Today is Helloween again!"
                }
              }
            }
          },
          loop2 = {
            messages = {
              ru = {
                {
                  "Странно... Кажется, новый день не наступил.",
                  "Сегодня все еще Хэллоуин!"
                },
                {
                  "Как это возможно?"
                }
              },
              en = {
                {
                  "It's weird... Seems like new day didn't come.",
                  "Today is Helloween again!"
                },
                {
                  "How it's possible?"
                }
              }
            }
          },
          loopN = {
            messages = {
              ru = {
                {
                  "Снова Хэллоуин?",
                  "Нужно это исправить!"
                }
              },
              en = {
                {
                  "Helloween again!",
                  "It should be fixed!"
                }
              }
            }
          }
        }
      },
      church = { -- @todo
        name = {
          ru = "Храм",
          en = "Church"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.key then
            return "open"
          end

          return "close"
        end,
        states = {
          close = {
            messages = {
              ru = {
                {
                  "Двери закрыты"
                }
              },
              en = {
                {
                  "The doors are closed"
                }
              }
            }
          },
          open = {
            messages = {
              ru = {
                {
                  "Ключ со скрипом проворачивается в замочной скважине..."
                },
                {
                  "Двери медленно открываются..."
                }
              },
              en = {
                {
                  "With squeaks key turned inside keyhole..."
                },
                {
                  "The doors open slowly..."
                }
              }
            },
            leave = function (data, char, game)
              game:gotoMap("church")
            end
          }
        }
      },
      churchOut = { -- @todo
        name = {
          ru = "Выход из храма",
          en = "Church doors"
        },
        visits = 0,
        state = function (data, char)
          return "open"
        end,
        states = {
          open = {
            messages = {
              ru = {
                {
                  "Дверь со скрипом открывается..."
                },
                {
                  "Свет заливает лицо..."
                }
              },
              en = {
                {
                  "The door opens with a creak ..."
                },
                {
                  "Light floods face..."
                }
              }
            },
            leave = function (data, char, game)
              game:gotoMap("cityOutChurch")
            end
          }
        }
      },
      home = { -- @todo
        name = {
          ru = "Дом",
          en = "Home"
        },
        visits = 0,
        state = function (data, char)
          return "open"
        end,
        states = {
          open = {
            messages = {
              ru = {
                {
                  "Дверь легко поддается"
                }
              },
              en = {
                {
                  "The door open easily"
                }
              }
            },
            leave = function (data, char, game)
              game:gotoMap("home")
            end
          }
        }
      },
      homeOut = { -- @todo
        name = {
          ru = "Выход из дома",
          en = "Home doors"
        },
        visits = 0,
        state = function (data, char)
          if not data.inventory.clock then
            return "closed"
          end

          return "open"
        end,
        states = {
          open = {
            messages = {
              ru = {
                {
                  "Слышен веселый уличный шум"
                },
                {
                  "Дверь открывается..."
                },
              },
              en = {
                {
                  "Cheerful street noise is heard"
                },
                {
                  "The door opens..."
                },
              }
            },
            leave = function (data, char, game)
              game:gotoMap("city")
            end
          },
          closed = {
            messages = {
              ru = {
                {
                  "Не забудь взять свои часы."
                }
              },
              en = {
                {
                  "Don't forget to take your watch."
                }
              }
            }
          }
        }
      },
      getClock = { -- @todo
        name = {
          ru = "Часы",
          en = "Сlock"
        },
        visits = 0,
        state = function (data, char)
          if data.inventory.clock then
            return "gettedClock"
          end

          return "getClock"
        end,
        states = {
          getClock = {
            messages = {
              ru = {
                {
                  "Теперь я могу следить за временем."
                }
              },
              en = {
                {
                  "Now I can keep track of the time."
                }
              }
            },
            leave = function (data)
              data.inventory.clock = true
            end
          },
          gettedClock = {
            messages = {
              ru = {
                {
                  "Здесь обычно лежат мои часы."
                },
                {
                  "Они обязательно должны лежать в этом углу!"
                }
              },
              en = {
                {
                  "Here place where i keep my watch."
                },
                {
                  "They necessarily must be in this corner!"
                }
              }
            }
          }
        }
      }
    }
  }
end

return Store

-- Формат текстов
--
-- ru = {
--   {
--     "строка 1",
--     "строка 2",
--     "последняя строка (без запятой после кавычки)"
--   },
--   {
--     "Следующая страница диалога, может быть из одной строки"
--   },
--   {
--     "Не длиннее 60 символов - иначе вылезет за бабл"
--   }
-- },
-- en = {
--   {
--     "Same rules, but in English"
--   }
-- }
