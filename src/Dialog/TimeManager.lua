local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")
local Interval = require("Core.Interval")

local TimeManager = class("TimeManager")

local CLOCK_XPOS = love.graphics.getWidth() - 99
local CLOCK_YPOS = 130

local TIMEUP_START_TIME = 15
local TIMEUP_INTERVAL_TIME = 2

function TimeManager:initialize(game)
  self.game = game
  self.store = game.store

  self.clockSprite = Sprite:new({image = "images/gui/gui_clock.png"})
  self.clockSprite.offset = vec2d(-self.clockSprite.image:getWidth()/2, -self.clockSprite.image:getHeight()/2)

  self.arrowSprite = Sprite:new({image = "images/gui/gui_clock_arrow.png"})
  self.arrowSprite.offset = vec2d(-self.clockSprite.image:getWidth()/2, -self.clockSprite.image:getHeight()/2)

  self.arrowAngle = 0

  --self.get_Tween_pos_y = Tween:new(0, 1, 1.2, "easeOutElastic", false, true)
  self.get_Tween_pos_y = Tween:new(-40, 0, 0.6, "easeOutQuint", false, true)
  self.get_Tween_cos = Tween:new(2, 0, 2.0, "easeOutElastic", false, true)

  self.isGetProcess = false
  self.get_Tween_cos.onEndSignal:add( function() self.isGetProcess = false end )

  self.lost_Tween_scale = Tween:new(1.0, 1.5, 0.5, "easeInBack", false, true)
  self.lost_Tween_alpha = Tween:new(1.0, 0, 0.5, "easeOutBounce", false, true)

  self.lost_Tween_alpha.onEndSignal:add( function() self.isLost = false end )

  self.isGetted = false
  self.isLost = false

  self.isTimeUpStarted = false

  self.timeUp_Tween_scale1 = Tween:new(1.0, 1.2, 0.25, "easeOutSine", false, true)
  self.timeUp_Tween_scale2 = Tween:new(1.2, 1.0, 0.5, "easeOutBounce", false, true)
  self.timeUp_Tween_scale2.current = 1.0
  self.timeUp_Tween_scale1.onEndSignal:add(
    function()
      self.timeUp_Tween_scale1.time = 0.0
      self.timeUp_Tween_scale1.current = 1.0
      self.timeUp_Tween_scale2:start()
    end )

  self.timeUp_timer = Interval:new(TIMEUP_INTERVAL_TIME, true)
  self.timeUp_timer.onEndSignal:add(
    function()
      self:startTimeUpEffect()
    end )
end

function TimeManager:update(dt)
  if self.store.data.inventory.clock and not self.isGetted then
    self.isGetted = true
    self.isGetProcess = true
    self.get_Tween_pos_y:start()
    self.get_Tween_cos:start()
  end

  if not self.store.data.inventory.clock and self.isGetted then
    self.isGetted = false
    self.isLost = true
    self.lost_Tween_scale:start()
    self.lost_Tween_alpha:start()
  end

  if self.game.timer then
    self.arrowAngle = (1-(self.game.timer.time/self.game.timer.length))*math.pi*2
  end

  self.get_Tween_pos_y:update(dt)
  self.get_Tween_cos:update(dt)

  self.lost_Tween_scale:update(dt)
  self.lost_Tween_alpha:update(dt)

  self.timeUp_Tween_scale1:update(dt)
  self.timeUp_Tween_scale2:update(dt)

  self.timeUp_timer:update(dt)

  if math.ceil(self.game.timer.time) == TIMEUP_START_TIME and not self.isTimeUpStarted then
    self.isTimeUpStarted = true
    self.timeUp_timer:start()
  end

  if math.ceil(self.game.timer.time) == 1 and self.isTimeUpStarted then
    self:stopTimeUpEffect()
  end
end

function TimeManager:draw()
  if not self.store.data.inventory.clock and not self.isGetted and self.isLost then
    self.clockSprite:draw(vec2d(CLOCK_XPOS, CLOCK_YPOS), 0, 0, 0, self.lost_Tween_scale.current, self.lost_Tween_scale.current, 0, 0, self.lost_Tween_alpha.current)
    self.arrowSprite:draw(vec2d(CLOCK_XPOS, CLOCK_YPOS), 0, 0, 0, self.lost_Tween_scale.current, self.lost_Tween_scale.current, 0, 0, self.lost_Tween_alpha.current)
  end

  if self.store.data.inventory.clock and self.isGetted and not self.isGetProcess then
    self.clockSprite:draw(vec2d(CLOCK_XPOS, CLOCK_YPOS), 0, 0, 0, (self.timeUp_Tween_scale1.current)*self.timeUp_Tween_scale2.current, (self.timeUp_Tween_scale1.current)*self.timeUp_Tween_scale2.current)
    self.arrowSprite:draw(vec2d(CLOCK_XPOS, CLOCK_YPOS), 0, 0, self.arrowAngle, (self.timeUp_Tween_scale1.current)*self.timeUp_Tween_scale2.current, (self.timeUp_Tween_scale1.current)*self.timeUp_Tween_scale2.current)
  end

  if self.store.data.inventory.clock and self.isGetted and self.isGetProcess then
    self.clockSprite:draw(vec2d(CLOCK_XPOS + math.cos(self.get_Tween_cos.current + math.pi/2)*100, CLOCK_YPOS + math.sin(self.get_Tween_cos.current + math.pi/2)*100 - 100 + self.get_Tween_pos_y.current), 0, 0, math.cos(-self.get_Tween_cos.current + math.pi/2))
    self.arrowSprite:draw(vec2d(CLOCK_XPOS + math.cos(self.get_Tween_cos.current + math.pi/2)*100, CLOCK_YPOS + math.sin(self.get_Tween_cos.current + math.pi/2)*100 - 100 + self.get_Tween_pos_y.current), 0, 0, self.arrowAngle + math.cos(-self.get_Tween_cos.current + math.pi/2))
  end
end

function TimeManager:startTimeUpEffect()
  self.game.effectManager:startShaderEffect_Game("circleRipple")
  self.timeUp_Tween_scale1:start()
end

function TimeManager:stopTimeUpEffect()
  self.timeUp_timer:stop()
  self.isTimeUpStarted = false
end

return TimeManager
