local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")
local Interval = require("Core.Interval")
local Signal = require("Core.Event.Signal")

local EffectFade = require("Dialog.Effects.EffectFade")
local EffectFadeCircle = require("Dialog.Effects.EffectFadeCircle")

local ShaderEffectScreenWave = require("Dialog.Effects.ShaderEffectScreenWave")
local ShaderEffectCircleRipple = require("Dialog.Effects.ShaderEffectCircleRipple")

local EffectManager = class("EffectManager")

local TIME_IS_UP_LENGHT = 6.0

function EffectManager:initialize(game)
  self.game = game

  --self.shader_ripple = love.graphics.newShader("shaders/ripple.fs")

  self.effects =
  {
    blackFadeOut = EffectFade:new(game, self, {0,0,0}, 2.0, "easeOutCubic", 0.0, 1.0),
    blackFadeIn = EffectFade:new(game, self, {0,0,0}, 2.0, "easeOutCubic", 1.0, 0.0),
    whiteFadeOut = EffectFade:new(game, self, {1,1,1}, 2.0, "easeOutCubic", 0.0, 1.0),
    whiteFadeIn = EffectFade:new(game, self, {1,1,1}, 2.0, "easeInCubic", 1.0, 0.0),
    fadeCircle = EffectFadeCircle:new(game, self, 6.0, "easeInOutCubic")
  }

  self.shaderEffects =
  {
    screenWave = ShaderEffectScreenWave:new(game, self),
    circleRipple = ShaderEffectCircleRipple:new(game, self)
  }

  self.shader_ripple = love.graphics.newShader("shaders/screen_wave.fs")

  self.currentShader_Game = nil
  self.timerShader_Game = Interval:new(1.0)
  self.timerShader_Game.onEndSignal:add( function() self.currentShader_Game = nil end )

  self.currentShader_All = nil
  self.timerShader_All = Interval:new(6.0)
  self.timerShader_All.onEndSignal:add( function() self.currentShader_All = nil end )
end

function EffectManager:update(dt)
  for k, effect in pairs(self.effects) do
    if effect.isStarted then
      effect:update(dt)
    end
  end

  if self.currentShader_Game then
    self.currentShader_Game:update(dt)
    self.timerShader_Game:update(dt)
  end

  if self.currentShader_All then
    self.currentShader_All:update(dt)
    self.timerShader_All:update(dt)
  end
end

function EffectManager:setShader_Game()
  if self.currentShader_Game then
    self.currentShader_Game:sendShaderData()
    love.graphics.setShader(self.currentShader_Game.shader)
  end
end

function EffectManager:setShader_All()
  if self.currentShader_All then
    self.currentShader_All:sendShaderData()
    love.graphics.setShader(self.currentShader_All.shader)
  end
end

function EffectManager:endShader()
  love.graphics.setShader()
end

function EffectManager:startEffect(effectName, def)
  self.effects[effectName]:start(def)
end

function EffectManager:startShaderEffect_Game(effectName, def)
  if def then
    self.timerShader_Game:setLength(def.duration or 6.0)
  end
  self.currentShader_Game = self.shaderEffects[effectName]
  self.currentShader_Game:start(def)
  self.timerShader_Game:start()
end

function EffectManager:startShaderEffect_All(effectName, def)
  if def then
    self.currentShader_All:setLength(def.length or 6.0)
  end
  self.currentShader_All = self.shaderEffects[effectName]
  self.currentShader_All:start(def)
  self.timerShader_All:start()
end

function EffectManager:draw()
  for k, effect in pairs(self.effects) do
    if effect.isStarted then
      effect:draw()
    end
  end

end

return EffectManager
