local DialogManager = class("DialogManager")

function DialogManager:initialize(game)
  self.game = game
  self.controls = game.controls
  self.store = game.store

  self.isStarted = false
  self.tick = 1
  self.currentMessage = 1
  self.charName = nil

  self.next = nil
end

function DialogManager:start(charName)
  if self.isStarted then
    return
  end

  self.game.player:gotoState("Dialog")
  self.isStarted = true
  self.tick = 1
  self.currentMessage = 1
  self.charName = charName

  local char = self:getChar()
  char.visits = char.visits + 1

  self.spacebarSign = true
  self.spacebarSign_interval = 40
  self.spacebarSign_time = 0
end

function DialogManager:next(charName)
  self.nextCharName = charName
end

function DialogManager:leave()
  if not self.isStarted then
    return
  end

  local char = self:getChar()
  local state = self:getState()

  if state.leave then
    state.leave(self.store.data, char, self.game)
  end

  self.game.player:gotoState("Stand")

  self.isStarted = false
  self.currentMessage = -1
  self.charName = nil

  if self.nextCharName then
    self:start(self.nextCharName)
    self.nextCharName = nil
  end
end

function DialogManager:getChar()
  return self.store.data.chars[self.charName]
end

function DialogManager:getState()
  local char = self:getChar()
  local state = char.state(self.store.data, char, self.game)
  return char.states[state]
end

function DialogManager:getMessages()
  local state = self:getState()
  return state.messages[LANG]
end

function DialogManager:getName()
  return self:getChar().name[LANG]
end

function DialogManager:update(dt)
  if not self.isStarted then
    return
  end

  if self.tick > 1 and self.controls:isKeyboardDown('space', 0) then
    self.currentMessage = self.currentMessage + 1
  end

  self.tick = self.tick + 1

  local messages = self:getMessages()
  if self.currentMessage > #messages then
    self:leave()
  end

  self.spacebarSign_time = self.spacebarSign_time + 1
  if self.spacebarSign_time >= self.spacebarSign_interval then
    self.spacebarSign_time = 0
    if self.spacebarSign == false then
      self.spacebarSign = true
    else
      self.spacebarSign = false
    end
  end
end

function DialogManager:draw(dt)
  if not self.isStarted then
    return
  end

  pushColor()

  local boxGap = 200
  local winW = love.graphics.getWidth()
  local w = winW - boxGap * 2
  local x = boxGap
  local y = 24
  local h = 120

  -- box
  love.graphics.setColor(0.2, 0.2, 0.2, 0.7)
  love.graphics.rectangle("fill", x, y, w, h, 8, 8)
  love.graphics.setColor(0.5, 0.5, 0.5)
  love.graphics.rectangle("line", x, y, w, h, 8, 8)

  -- text
  love.graphics.setColor(1, 1, 1)
  love.graphics.setFont(FONT_DIALOG)

  local txtGapX = 24
  local txtGapY = 8
  local lineGap = 8

  local char = self:getChar()
  local messages = self:getMessages()
  local message = messages[self.currentMessage]
  local lineCount = #message
  local startY = 4 + (h - (txtGapY * 2) - ((lineCount * 24) + (lineCount - 1) * lineGap)) / 2

  -- NAME
  love.graphics.setColor(0.9, 0.7, 0.7)
  love.graphics.print(
    self:getName(),
    x + txtGapX,
    y + 2
  )

  -- MESSAGE
  love.graphics.setColor(1, 1, 1)
  for i, line in ipairs(message) do
    love.graphics.print(
      line,
      x + txtGapX,
      y + txtGapY + startY + (i - 1) * (24 + lineGap)
    )
  end

  if self.spacebarSign == true then
    love.graphics.print("spacebar",950,110)
  end

  popColor()
end

return DialogManager
