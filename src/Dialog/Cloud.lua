local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")
local Interval = require("Core.Interval")
local Signal = require("Core.Event.Signal")

local Cloud = class("Cloud")

function Cloud:initialize(game)
  self.game = game

  self.spriteCloud = Sprite:new({
    image = "images/gui/cloud.png",
    offset = vec2d(-4,-36)
  })

  self.currentScale = 0.0
  self.currentRotate = 1.0

  self.scaleTween = Tween:new(0, 1.0, 1.0, "easeOutElastic", false, true)
  self.scaleRotate = Tween:new(1.0, 0, 0.4, "easeOutSine", false, true)

  self.show = false
  self.scaleTween.onEndSignal:add(
    function()
      if not self.isStarted then
        self.show = false
      end
    end )

  self.isStarted = false

  self.isOpened = false

  self.stopInterval = Interval:new(0.1)
  self.stopInterval.onEndSignal:add(
    function()
      self.scaleTween:stop()
      self.scaleRotate:stop()
      self.scaleTween.tween = "easeInQuad"
      self.scaleTween.from = self.currentScale
      self.scaleTween.current = self.currentScale
      self.scaleTween.to = 0.0
      self.scaleTween.duration = 0.2
      self.scaleTween:start()

      self.scaleRotate.duration = 1.0
      self.scaleRotate.from = self.currentRotate
      self.scaleRotate.current = self.currentRotate
      self.scaleRotate.to = 1.0
      self.scaleRotate:start()

      self.isStarted = false
    end )
end

function Cloud:open()
  self.show = true
  self.stopInterval:stop()
  if not self.isStarted then
    --print("open")
    self.scaleTween:stop()
    self.scaleRotate:stop()
    self.isStarted = true

    if self.currentScale >= 0.3 then
      self.scaleTween.tween = "easeOutSine"
      self.scaleTween.duration = 0.2

      self.scaleRotate.duration = 0.2
    else
      self.scaleTween.tween = "easeOutElastic"
      self.scaleTween.duration = 1.0

      self.scaleRotate.duration = 0.2
    end

    self.scaleTween.from = self.currentScale
    self.scaleTween.current = self.currentScale
    self.scaleTween.to = 1.0
    self.scaleTween:start()

    self.scaleRotate.from = self.currentRotate
    self.scaleRotate.current = self.currentRotate
    self.scaleRotate.to = 0.0
    self.scaleRotate:start()
  end

end

function Cloud:close()
  if self.isStarted and self.currentScale ~= 0 then
    --print("close")
    self.stopInterval:start()
  end
end

function Cloud:update(dt)
  self.stopInterval:update(dt)
  self.scaleTween:update(dt)
  self.scaleRotate:update(dt)
  self.currentScale = self.scaleTween.current

  self.currentRotate = self.scaleRotate.current
end

function Cloud:draw(x, y)
  if (self.currentScale > 0.1) and self.show then
    self.spriteCloud:draw(vec2d(x,y), nil, nil, self.currentRotate, self.currentScale, self.currentScale)
  end
end

return Cloud
