local DialogManager = class("DialogManager")
local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")
local Interval = require("Core.Interval")

function DialogManager:initialize(game)
  self.game = game
  self.controls = game.controls
  self.store = game.store

  self.isStarted = false
  self.tick = 1
  self.currentMessage = 1
  self.charName = charName

  self.next = nil

  self.messageCharsCount = 0
  self.currentChar = 0

  self.state = "closed"

  self.startPosition = vec2d(640, -160)
  self.screenPosition = vec2d(640, 80)
  self.dialogSprite = Sprite:new({image = "images/gui/dialog_window.png", offset = vec2d(-459, -79)})

  self.startPos_tween = Tween:new(self.startPosition.y, self.screenPosition.y, 1.0, "easeOutElastic_small", false, true)
  self.startScaleX_tween = Tween:new(0.5, 1.0, 1.0, "easeOutCubic", false, true)
  self.startScaleY_tween = Tween:new(1.5, 1.0, 1.0, "easeOutCubic", false, true)
  self.startPos_tween.onEndSignal:add( function()
    self.state = "opened"
    self:nextData()
    --self.character_timer:start()
   end )

  self.spacebarSign = true
  self.spacebarSign_timer = Interval:new(0.3, true)
  self.spacebarSign_timer.onEndSignal:add(
    function()
      if self.spacebarSign then
        self.spacebarSign = false
      else
        self.spacebarSign = true
      end
      --self.spacebarSign = self.spacebarSign and false or true
    end )
  self.spacebarSign_timer:start()

  self.endPos_tween = Tween:new(self.screenPosition.y, self.startPosition.y, 0.5, "easeInBack", false, true)
  self.endScaleX_tween = Tween:new(1.0, 0.5, 0.7, "easeInCubic", false, true)
  self.endScaleY_tween = Tween:new(1.0, 1.5, 0.7, "easeInCubic", false, true)
  self.endPos_tween.onEndSignal:add(
    function()
      self.state = "closed"
      self.isStarted = false
      self.character_timer:stop()
    end )


  self.character_timer = Interval:new(0.015, true)
  self.character_timer.onEndSignal:add(
    function()
      self.currentChar = self.currentChar + 1
      if self.currentChar >= self.messageCharsCount then
        self.currentChar = self.messageCharsCount
        self.character_timer:stop()
      end
    end )
end

function DialogManager:start(charName)
  self.state = "start_process"

  self.startPos_tween:start()
  self.startScaleX_tween:start()
  self.startScaleY_tween:start()

  self.game.player:gotoState("Dialog")
  self.isStarted = true
  self.tick = 1
  self.currentMessage = 1
  self.charName = charName

  local char = self:getChar()
  char.visits = char.visits + 1
end

function DialogManager:next(charName)
  self.nextCharName = charName
end

function DialogManager:leave()
  self.state = "end_process"


  self.endPos_tween:start()
  self.endScaleX_tween:start()
  self.endScaleY_tween:start()


  local char = self:getChar()
  local state = self:getState()

  if state.leave then
    state.leave(self.store.data, char, self.game)
  end

  self.game.player:gotoState("Stand")



  self.currentMessage = -1
  --self.charName = nil

  if self.nextCharName then
    self:start(self.nextCharName)
    self.nextCharName = nil
  end

end

function DialogManager:getChar()
  return self.store.data.chars[self.charName]
end

function DialogManager:getState()
  local char = self:getChar()
  local state = char.state(self.store.data, char, self.game)
  return char.states[state]
end

function DialogManager:getMessages()
  local state = self:getState()
  return state.messages[LANG]
end

function DialogManager:getName()
  return self:getChar().name[LANG]
end

function DialogManager:update(dt)
  --if not self.isStarted then
  --  return
  --end

  if self.tick > 1 and self.controls:isKeyboardDown('space', 0) and not self.character_timer.isStarted and self.state ~= "start_process" then
    self.currentMessage = self.currentMessage + 1
    self:nextData()
  elseif self.controls:isKeyboardDown('space', 0) and self.character_timer.isStarted and self.state ~= "start_process" then
    self.currentChar = self.messageCharsCount
    self.character_timer:stop()
  end

  self.tick = self.tick + 1

  local messages = self:getMessages()
  if self.currentMessage > #messages and self.state ~= "end_process" then
    self:leave()
  end

  self.startPos_tween:update(dt)
  self.startScaleX_tween:update(dt)
  self.startScaleY_tween:update(dt)

  self.endPos_tween:update(dt)
  self.endScaleX_tween:update(dt)
  self.endScaleY_tween:update(dt)

  self.spacebarSign_timer:update(dt)

  self.character_timer:update(dt)
end

function DialogManager:nextData()
  --print("nextData")
  self.boxGap = 200
  self.winW = love.graphics.getWidth()
  self.w = self.winW - self.boxGap * 2
  self.x = self.boxGap
  self.y = 24
  self.h = 120

  self.txtGapX = 24
  self.txtGapY = 8
  self.lineGap = 8

  self.char = self:getChar()
  local messages = self:getMessages()
  --print(inspect(messages))
  local message = messages[self.currentMessage] or {}

  self.lineCount = #message
  self.startY = 4 + (self.h - (self.txtGapY * 2) - ((self.lineCount * 24) + (self.lineCount - 1) * self.lineGap)) / 2


  if (self.lineCount > 0) then
    self.printMessage = ""
    for i, line in ipairs(message) do
      self.printMessage = self.printMessage .. line .. "\n"
    end
  end

  self.messageCharsCount = string.len(self.printMessage)
  self.currentChar = 0
  --self.character_timer:stop()
  self.character_timer:start()
end

function DialogManager:draw(dt)
  pushColor()

  love.graphics.setFont(FONT_DIALOG)

  if self.state == "start_process" then

    self.dialogSprite:draw(vec2d(self.screenPosition.x, self.startPos_tween.current), nil, nil, nil, self.startScaleX_tween.current, self.startScaleY_tween.current)

  elseif self.state == "opened" then

    self.dialogSprite:draw(vec2d(self.screenPosition.x, self.screenPosition.y))

    love.graphics.setColor(0.9, 0.7, 0.7)
    love.graphics.print(self:getName(), 210, 30)

    love.graphics.setColor(1, 1, 1)
    love.graphics.print(self.printMessage:sub( 0, self.currentChar ), self.x + self.txtGapX ,self.y + self.txtGapY + self.startY)

    if self.spacebarSign and not self.character_timer.isStarted then
      love.graphics.setColor(1, 1, 1, 0.7)
      love.graphics.print("spacebar",950,110)
      love.graphics.setColor(1, 1, 1, 1)
    end

  elseif self.state == "end_process" then
    self.dialogSprite:draw(vec2d(self.screenPosition.x, self.endPos_tween.current), nil, nil, nil, self.endScaleX_tween.current, self.endScaleY_tween.current)

  end
  popColor()
end

return DialogManager
