local Sprite = require("Core.Sprite")
local Tween = require("Core.Tween")

local InventoryManager = class("InventoryManager")

local SLOTS_DISTANSE = 90
local SLOTS_YPOS = 60
local SLOTS_XPOS = 820

function InventoryManager:initialize(game)
  self.game = game
  self.store = game.store

  self.slotSprite = Sprite:new({image = "images/items/item_slot.png"})

  self.sprites = {
    pumpkin = Sprite:new({image = "images/items/item_pumpkin.png"}),
    pie = Sprite:new({image = "images/items/item_pie.png"}),
    lantern = Sprite:new({image = "images/items/item_pumpkin_lighter.png"}),
    lambswool = Sprite:new({image = "images/items/item_cup.png"}),
    key = Sprite:new({image = "images/items/item_key.png"}),
    clock = Sprite:new({image = "images/items/item_clock.png"}),
    costume = Sprite:new({image = "images/items/item_suit.png"}),
    candle = Sprite:new({image = "images/items/item_candle.png"}),
    candies = Sprite:new({image = "images/items/item_candies.png"})
  }

  self.sprites.pumpkin.invPos = 0
  self.sprites.pie.invPos = 1
  self.sprites.lantern.invPos = 2
  self.sprites.lambswool.invPos = 3
  self.sprites.key.invPos = 4
  self.sprites.clock.invPos = 5
  self.sprites.costume.invPos = 6
  self.sprites.candle.invPos = 7
  self.sprites.candies.invPos = 8

  for k, sprite in pairs(self.sprites) do
    sprite.getted = false
    sprite.get_Tween_scale_x = Tween:new(0, 1, 1.2, "easeOutElastic", false, true)
    sprite.get_Tween_scale_y = Tween:new(0, 1, 0.8, "easeOutElastic", false, true)

    sprite.lost_Tween_scale_x = Tween:new(1, 1.3, 0.4, "easeOutQuint", false, true)
    sprite.lost_Tween_scale_y = Tween:new(1, 1.3, 0.3, "easeOutQuint", false, true)
    sprite.lost_Tween_scale_x.current = 0
    sprite.lost_Tween_scale_y.current = 0
    sprite.lost_Tween_alpha = Tween:new(1, 0, 0.4, "linear", false, true)
  end

  self.particleImage, self.particleImageData = Sprite.static.getCachedImageData("images/effects/star.png")
  self.particlesEffect = love.graphics.newParticleSystem(self.particleImage, 32)
	self.particlesEffect:setColors(1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0.5, 1, 1, 1, 0)
	self.particlesEffect:setDirection(-1.5707963705063)
	self.particlesEffect:setEmissionArea("none", 0, 0, 0, false)
	self.particlesEffect:setEmissionRate(20)
	self.particlesEffect:setEmitterLifetime(0.95825099945068)
	self.particlesEffect:setInsertMode("top")
	self.particlesEffect:setLinearAcceleration(0.43671935796738, -0.048524372279644, -0.048524372279644, 0.43671935796738)
	self.particlesEffect:setLinearDamping(2.1399250030518, 6.6429867744446)
	self.particlesEffect:setOffset(50, 50)
	self.particlesEffect:setParticleLifetime(0.45279064774513, 1.5839908123016)
	self.particlesEffect:setRadialAcceleration(0, 0)
	self.particlesEffect:setRelativeRotation(false)
	self.particlesEffect:setRotation(0, 0)
	self.particlesEffect:setSizes(0.0025149639695883, 0.31547710299492)
	self.particlesEffect:setSizeVariation(0.97507786750793)
	self.particlesEffect:setSpeed(92.409820556641, 442.56170654297)
	self.particlesEffect:setSpin(3.3605935573578, 0.00048782021622173)
	self.particlesEffect:setSpinVariation(0)
	self.particlesEffect:setSpread(6.2831854820251)
	self.particlesEffect:setTangentialAcceleration(0.097048744559288, 0.097048744559288)
  self.particlesEffect:stop()

  self.particlesEffectPos = self:getSlotPosition(0)

  self.get_sfx = love.audio.newSource("sound/get_item.wav", "static")
  self.get_sfx:setVolume(0.5)
end

function InventoryManager:getSlotPosition(slot_num)
  return vec2d(love.graphics.getWidth() - SLOTS_XPOS + slot_num*SLOTS_DISTANSE, love.graphics.getHeight() - SLOTS_YPOS)
end

function InventoryManager:update(dt)

  for k, sprite in pairs(self.sprites) do
    if self.store.data.inventory[k] and not sprite.getted then
      sprite.getted = true
      sprite.get_Tween_scale_x:start()
      sprite.get_Tween_scale_y:start()
      self.particlesEffectPos = self:getSlotPosition(sprite.invPos)
      self.particlesEffect:start()
      self.get_sfx:stop()
      self.get_sfx:setPitch( love.math.random(90, 115)/100)
      self.get_sfx:play()
    end

    if not self.store.data.inventory[k] and sprite.getted then
      sprite.getted = false
      sprite.lost_Tween_scale_x:start()
      sprite.lost_Tween_scale_y:start()
      sprite.lost_Tween_alpha:start()
    end

    sprite.get_Tween_scale_x:update(dt)
    sprite.get_Tween_scale_y:update(dt)
    sprite.lost_Tween_scale_x:update(dt)
    sprite.lost_Tween_scale_y:update(dt)
    sprite.lost_Tween_alpha:update(dt)
  end

  self.particlesEffect:update(dt)
end

function InventoryManager:draw(dt)
  for i = 0,8,1
  do
    self.slotSprite:draw(self:getSlotPosition(i), 60, 60, nil, nil, nil, nil, nil, 0.75)
  end

  love.graphics.draw(self.particlesEffect, self.particlesEffectPos.x, self.particlesEffectPos.y)

  for k, sprite in pairs(self.sprites) do
    if self.store.data.inventory[k] and sprite.getted then
      sprite:draw(self:getSlotPosition(sprite.invPos), 60, 60, 0, sprite.get_Tween_scale_x.current, sprite.get_Tween_scale_y.current)
    elseif not self.store.data.inventory[k] and not sprite.getted then
      sprite:draw(self:getSlotPosition(sprite.invPos), 60, 60, 0, sprite.lost_Tween_scale_x.current, sprite.lost_Tween_scale_y.current,0,0,sprite.lost_Tween_alpha.current)
    end
  end

end

return InventoryManager
