local Tween = require("Core.Tween")

local ShaderEffectScreenWave = class('ShaderEffectScreenWave')

function ShaderEffectScreenWave:initialize(game, effectManager)
  self.game = game
  self.effectManager = effectManager
  self.shader = love.graphics.newShader("shaders/screen_wave.fs")

  self.duration = duration

  --self.tweenAlpha = Tween:new(from or 0, to or 1.0, duration, easing, false, true)
  self.tweenAmplitude = Tween:new(0, 1.0, 6.0, "easeInOutSine", false, true)
  self.tweenLenght = Tween:new(0, 1.0, 6.0, "easeInOutSine", false, true)
  self.tweenShift = Tween:new(0, 20.0, 6.0, "linear", false, true)
end

function ShaderEffectScreenWave:start(def)
  if def then
    self.tweenAmplitude.duration = def.duration
    self.tweenShift.duration = def.duration
    self.tweenLenght.duration = def.duration
  else
    self.tweenAmplitude.duration = 6.0
    self.tweenShift.duration = 6.0
    self.tweenLenght.duration = 6.0
  end

  self.tweenAmplitude:start()
  self.tweenShift:start()
  self.tweenLenght:start()
end

function ShaderEffectScreenWave:update(dt)
  self.tweenAmplitude:update(dt)
  self.tweenShift:update(dt)
  self.tweenLenght:update(dt)
end

function ShaderEffectScreenWave:sendShaderData()
  self.shader:send("wave_amplitude", self.tweenAmplitude.current)
  self.shader:send("wave_lenght", self.tweenLenght.current)
  self.shader:send("wave_shift", self.tweenShift.current)
end

return ShaderEffectScreenWave
