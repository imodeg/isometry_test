local Tween = require("Core.Tween")

local ShaderEffectCircleRipple = class('ShaderEffectCircleRipple')

function ShaderEffectCircleRipple:initialize(game, effectManager)
  self.game = game
  self.effectManager = effectManager
  self.shader = love.graphics.newShader("shaders/ripple.fs")

  self.duration = duration

  self.tweenLenght = Tween:new(0, 25.0, 1.0, "easeOutQuad", false, true)
  self.tweenShift = Tween:new(0, 15.0, 1.0, "easeOutQuad", false, true)
  self.tweenIntencity = Tween:new(2.0, 0, 1.0, "linear", false, true)
end

function ShaderEffectCircleRipple:start(def)
  if def then
    self.tweenShift.duration = def.duration
    self.tweenLenght.duration = def.duration
    self.tweenIntencity.duration = def.duration
  else
    self.tweenShift.duration = 1.0
    self.tweenLenght.duration = 1.0
    self.tweenIntencity.duration = 1.0
  end

  self.tweenShift:start()
  self.tweenLenght:start()
  self.tweenIntencity:start()
end

function ShaderEffectCircleRipple:update(dt)
  self.tweenShift:update(dt)
  self.tweenLenght:update(dt)
  self.tweenIntencity:update(dt)
end

function ShaderEffectCircleRipple:sendShaderData()
  local mouse_x, mouse_y = love.mouse.getPosition()
  --self.shader:send("start_point", {love.graphics.getWidth() - 99, 130})
  --self.shader:send("wave_lenght", mouse_y)
  --self.shader:send("wave_shift", mouse_x/10)

  self.shader:send("start_point", {love.graphics.getWidth() - 99, 130})
  self.shader:send("wave_lenght", self.tweenLenght.current)
  self.shader:send("wave_shift", self.tweenShift.current)
  self.shader:send("intencity", self.tweenIntencity.current)
  --print("wave_lenght: " .. mouse_y/10 .. " wave_shift: " .. mouse_x/10)
  --self.shader:send("wave_amplitude", self.tweenAmplitude.current)
  --self.shader:send("wave_lenght", self.tweenLenght.current)
  --self.shader:send("wave_shift", self.tweenShift.current)
end

return ShaderEffectCircleRipple
