local Tween = require("Core.Tween")

local EffectFade = class('EffectFade')

function EffectFade:initialize(game, effectManager, color, duration, easing, from, to)
  self.game = game
  self.effectManager = effectManager
  self.color = color
  self.duration = duration
  self.tweenAlpha = Tween:new(from or 0, to or 1.0, duration, easing, false, true)

  self.isStarted = false

  self.tweenAlpha.onEndSignal:add( function() self.isStarted = false end )
end

function EffectFade:start(def)
  self.isStarted = true

  if def then
    self.tweenAlpha.duration = def.duration
  else
    self.tweenAlpha.duration = self.duration
  end
  self.tweenAlpha:start()
end

function EffectFade:update(dt)
  self.tweenAlpha:update(dt)
end

function EffectFade:draw()
  pushColor()
  love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.tweenAlpha.current)
  love.graphics.rectangle( "fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight() )
  popColor()
end

return EffectFade
