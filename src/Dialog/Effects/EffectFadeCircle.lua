local Tween = require("Core.Tween")
local Sprite = require("Core.Sprite")

local EffectFadeCircle = class('EffectFadeCircle')

function EffectFadeCircle:initialize(game, effectManager, duration, easing)
  self.game = game
  self.effectManager = effectManager
  self.duration = duration
  self.tweenAlpha = Tween:new(0, 1.0, duration-(duration/5), easing, false, true)
  self.tweenScale = Tween:new(4, 1.0, duration, easing, false, true)

  self.isStarted = false

  self.tweenScale.onEndSignal:add( function() self.isStarted = false end )
  self.blackScreenSprite = Sprite:new({image = "images/effects/black_screen_glow.png"})
  self.blackScreenSprite.offset = vec2d(-self.blackScreenSprite.image:getWidth()/2, -self.blackScreenSprite.image:getHeight()/2)
end

function EffectFadeCircle:start(def)
  self.isStarted = true

  if def then
    self.tweenAlpha.duration = def.duration-(def.duration/5)
    self.tweenScale.duration = def.duration
  else
    self.tweenAlpha.duration = self.duration-(self.duration/5)
    self.tweenScale.duration = self.duration
  end
  self.tweenAlpha:start()
  self.tweenScale:start()
end

function EffectFadeCircle:update(dt)
  self.tweenAlpha:update(dt)
  self.tweenScale:update(dt)
end

function EffectFadeCircle:draw()
  local playerOrtCoordinates = self.game.player:getOrtPosition()
  playerOrtCoordinates.x = playerOrtCoordinates.x - self.game.camera.x
  playerOrtCoordinates.y = playerOrtCoordinates.y - self.game.camera.y - 76
  self.blackScreenSprite:draw(playerOrtCoordinates, 0,0,0, self.tweenScale.current, self.tweenScale.current, 0, 0, self.tweenAlpha.current)
end

return EffectFadeCircle
