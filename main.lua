--package.path = "./vendor/?.lua;./src/?.lua;" .. package.path
love.filesystem.setRequirePath("/vendor/?.lua;/src/?.lua;/levels/?.lua")
class = require("middleclass")
inspect = require("inspect")
require("Utils.index")

love.window.setMode(1280, 720, { msaa  = 16})
love.window.setTitle( "Neverending Partyween" )

FONT_DEBUG = love.graphics.newFont("fonts/dialog.ttf", 16)
FONT_DIALOG = love.graphics.newFont("fonts/dialog.ttf", 24)
FONT_CLOCK = love.graphics.newFont("fonts/dialog.ttf", 42)
FONT_JPN_BIG = love.graphics.newFont("fonts/keifont.ttf", 40)

DEBUG_ENABLED = false

LANG = "en"

local MAX_DT = 1 / 60

local Game = require("Game")
local game = Game:new()

function love.load()
  love.graphics.setDefaultFilter("nearest")
  --game:gotoState("Level")
  game:gotoState("Menu")

  music = love.audio.newSource("music/witchmusic.mp3", "stream")
  music:setLooping(true)
  love.audio.play( music )
end

function love.update(dt)
  game:update(math.min(dt, MAX_DT))
end

function love.draw()
  game:draw()
end

function love.keypressed(key)
  game:keyboardPressed(key)
end

function love.keyreleased(key)
  game:keyboardReleased(key)
end

function love.mousepressed( x, y, button, istouch, presses )
  game:mousePressed( x, y, button, istouch, presses )
end

function love.mousereleased( x, y, button, istouch, presses )
  game:mouseReleased( x, y, button, istouch, presses )
end
